# unify.host

Host process for Unify

## Standalone Installation

`Unify` can be configured to run in-process in any Javascript environment with a supported data store 
([IndexedDB](https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API) and any of the SQL databases supported by 
[`knex`](https://knexjs.org/)). Use the following steps to install for a browser in a React application using IndexedDB.

### Install Dependencies

```
yarn add unify.host unify.react
```

### Configure React Bindings

Modify `src/index.js` to wrap your `App` component in a `Provider`:

```
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'unify.react'
import App from './App'
import host from 'unify.host'

ReactDOM.render(
  <Provider host={host()}><App /></Provider>,
  document.getElementById('root')
)
```

## Aspects

The `Unify` host provides the following aspects:

### Property Aspects

#### `uuid`

Sets the property to a 
[random universally unique identifier](https://en.wikipedia.org/wiki/Universally_unique_identifier#Version_4_(random)).

### Call Aspects

#### `open`

Allows external access to the vocabulary with any scope.

#### `scoped(...scopeProperties)`

Allows external access to the vocabulary with the specified scope properties set. Pass no parameters to enforce 
access to the global scope.

#### `strictApi`

Used internally by the `strictApi` configuration option to ensure the vocabulary has been marked as "public".

## License

**The MIT License (MIT)**

Copyright © 2022 Dale Anderson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
