module.exports = (source, expressions, storage, constraints, machines, log) => {
  log = log.child({ source: 'unify.host.bus' })

  return {
    observable: expressions.proxy(source),
    publish: async message => {
      // it is questionable that constraints / machine validation are always validated in strict order
      // in theory, they should, because:
      //   - validation is synchronous in both constraints and machines
      //   - the asynchronous part comes from initialization and loading of expressions
      //   - since subsequent calls are effectively queued by the promise mechanism,
      //     and subscriptions to expressions are shared,
      //   - all validation should effectively be queued......
      const constraintValidationResult = await constraints.validate(message)
      if(!constraintValidationResult.valid) {
        throw new Error(constraintValidationResult.messages.join('. '))
      }
      
      const machineValidationResult = await machines.validatePublish(message)
      if(!machineValidationResult.valid) {
        throw new Error(machineValidationResult.messages.join('. '))
      }

      const envelope = await storage.messages.store({ message, serverTime: Date.now() })
      await machines.publish(message)
      source.publish(envelope)
      log.debug('Stored and published', { envelope })

      return envelope
    }
  }
}