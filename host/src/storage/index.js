const xstore = require('@x/store')
const expressions = require('./expressions')
const machines = require('./machines')
const messages = require('./messages')

module.exports = (storageConfiguration, scopes, hasher, log) => {
  log = log.child({ source: 'unify.host.storage' })
  
  const db = xstore(storageConfiguration)
  const initialised = initialise()

  return {
    expressions: expressions(initialised, hasher, log),
    machines: machines(initialised, log),
    messages: messages(initialised, log),
    close: () => initialised.then(({ close }) => close())
  }

  function initialise() {
    log.debug(`Initialising database using ${storageConfiguration.client} client`)
    
    const entities = [
      {
        name: 'expressions',
        keyPath: 'key',
        indexes: [['definition', 'scope']]
      },
      {
        name: 'machines',
        keyPath: 'key'
      },
      {
        name: 'messages',
        keyPath: 'timestamp',
        autoIncrement: true,
        indexes: [...scopes.map(scope => [`message.${scope}`, 'timestamp']), 'timestamp']
      }
    ]

    return db.open(entities).then(provider => ({
      expressions: provider.entity('expressions'),
      machines: provider.entity('machines'),
      messages: provider.entity('messages'),
      close: provider.close
    }))
  } 
}