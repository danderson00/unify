const { sortObject } = require('../utilities')

module.exports = (initialised, hasher, log) => {
  return {
    retrieve: (definition, scope = {}) => {
      const hash = hasher(definition)
      return initialised
        .then(({ expressions }) => (
          expressions.retrieve([
            { p: 'definition', v: hash },
            { p: 'scope', v: JSON.stringify(sortObject(scope)) }
          ])
        ))
        .then(results => {
          if(results.length > 0) {
            log.debug('Retrieved cached expression', { definition: hash, scope, timestamp: results[0].timestamp })
            return results[0]
          } else {
            log.debug('No cached expression existed', { definition: hash, scope })
            return undefined
          }
        })
    },
    store: (definition, scope, state, error, timestamp) => {
      const hash = hasher(definition)
      const scopeValue = JSON.stringify(sortObject(scope))
      const key = hash + hasher(scopeValue)

      return initialised.then(({ expressions }) => (
        expressions.store({
          key,
          definition: hash,
          scope: scopeValue,
          state,
          error,
          timestamp
        }).then(result => {
          log.debug('Stored cached expression', { definition: hash, scope, timestamp })
          return result
        })
      ))
    },
    clear: () => initialised.then(({ expressions }) => expressions.clear())
  }
}