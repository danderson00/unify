module.exports = (initialised, log) => {
  return {
    retrieve: async () => {
      const { machines } = await initialised
      const retrieved = machines.retrieve()
      log.debug(retrieved.length ? `Retrieved ${retrieved.length} machines` : `No machines configured`)
      return retrieved
    },
    store: async machine => {
      const { machines } = await initialised
      await machines.store({ key: `${machine.id}-${JSON.stringify(machine.scope)}`, ...machine })
      log.debug(`Stored machine state for ${machine.id} with scope ${JSON.stringify(machine.scope)}`)
    }
  }
}