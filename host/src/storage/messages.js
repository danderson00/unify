module.exports = (initialised, log) => {
  return {
    retrieve: (scope = {}, since = 0) => (
      initialised
        .then(({ messages }) => (
          messages.retrieve(
            Object.keys(scope)
              .map(key => ({ p: `message.${key}`, v: scope[key] }))
              .concat({ p: 'timestamp', o: '>', v: since })
          )
        ))
        .then(results => {
          log.debug('Retrieved messages', { count: results.length, since, scope })
          return results
        })
    ),
    store: envelope => (
      initialised
        .then(({ messages }) => (
          messages.store(envelope)
        ))
        .then(storedEnvelope => {
          log.debug('Stored envelope', { envelope: storedEnvelope })
          return storedEnvelope
        })
    )
  }
}