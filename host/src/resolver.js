const { scopePredicate, dedupe, debounceAsync } = require('./utilities')
const createDisconnectTarget = require('./operators/disconnectTarget')

// this needs a serious refactor
module.exports = (expressions, storage, inputObservable, hasher, log) => (
  async (definition, scope, options = {}) => {
    log = log.child({ source: 'unify.host.resolver' })

    const proxy = expressions.proxy(inputObservable)
    const scopeObservable = proxy.where(scopePredicate(scope))
    const cache = cacheMessages(scopeObservable)
    const source = expressions.subject()
    const disconnectTarget = createDisconnectTarget(source)

    const disconnect = () => {
      proxy.disconnect()
      disconnectTarget.disconnect()
    }

    // reconstruct observable from stored state and provided definition
    const storedState = !options.transient && (await storage.expressions.retrieve(definition, scope)) || {}
    const observable = expressions.zip(disconnectTarget, definition, storedState.state, storedState.error)

    if(storedState.error) {
      // no need to replay / hook anything up if the expression is in error state - just flush the message cache
      cache()
      log.debug('Resolved expression', { definition: hasher(definition), scope, cached: !!storedState.state, timestamp: storedState.timestamp, error: storedState.error })

    } else {
      observable.errorObservable.subscribe(error => {
        log.error(error)
        disconnect()
      })

      // replay any messages that have been published since the expression was updated
      let lastPublishedTimestamp
      const publishMessage = envelope => {
        if(!observable.errorObservable()) {
          lastPublishedTimestamp = envelope.timestamp
          source.publish(envelope.message)
        }
      }

      const subsequentMessages = await storage.messages.retrieve(scope, storedState.timestamp || 0)
      const cachedMessages = cache()
      const deduped = dedupe(subsequentMessages, cachedMessages)
      deduped.forEach(publishMessage)

      // connect the source subject to the scope observable
      scopeObservable.subscribe(publishMessage)

      // update the stored state every time the value changes
      const updateState = debounceAsync(() => {
        const lastScopeTimestamp = lastPublishedTimestamp || storedState.timestamp || 0

        log.debug('Updating state', { definition: hasher(definition), scope, timestamp: lastScopeTimestamp })

        return storage.expressions.store(
          definition,
          scope,
          expressions.unzip(observable).state,
          observable.errorObservable(),
          lastScopeTimestamp
        )
      })

      if (!options.transient) {
        observable.subscribe(updateState)
        if (subsequentMessages.length > 0 || cachedMessages.length > 0) {
          // not sure if the await is required here - could improve response times
          await updateState()
        }
      }

      log.debug('Resolved expression', { definition: hasher(definition), scope, cached: !!storedState.state, timestamp: storedState.timestamp, stored: subsequentMessages.length, interim: cachedMessages.length, deduped: subsequentMessages.length + cachedMessages.length - deduped.length })
    }

    return {
      observable,
      disconnect
    }
  }
)

const cacheMessages = observable => {
  const messages = []
  const subscription = observable.subscribe(envelope => messages.push(envelope))
  return () => {
    subscription.unsubscribe()
    return messages
  }
}