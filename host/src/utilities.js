module.exports = {
  sortObject: source => (
    Object
      .keys(source)
      .sort()
      .reduce((target, key) => {
        target[key] = source[key]
        return target
      }, {})
  ),
  scopePredicate: scope => (
    envelope => Object
      .keys(scope)
      .every(key => envelope.message[key] === scope[key])
  ),
  dedupe: (stored, subsequent) => ([
    ...stored,
    ...subsequent.filter(x => stored.length === 0 || x.timestamp > stored[stored.length - 1].timestamp)
  ]),
  debounceAsync: sourceFunction => {
    // assumes no arguments and the caller doesn't consume the result
    let currentExecution
    let queuedRequests = []

    const executeSourceFunction = () => {
      return sourceFunction()
        .then(() => {
          if(queuedRequests.length > 0) {
            const currentQueuedRequests = [...queuedRequests]
            queuedRequests = []

            currentExecution = executeSourceFunction()
            currentExecution
              .then(() => currentQueuedRequests.forEach(x => x.resolve()))
              .catch(error => currentQueuedRequests.forEach(x => x.reject(error)))
          } else {
            currentExecution = undefined
          }
        })
    }

    return () => new Promise((resolve, reject) => {
      if(currentExecution) {
        queuedRequests = [...queuedRequests, { resolve, reject }]
      } else {
        currentExecution = executeSourceFunction()
          .then(resolve)
          .catch(reject)
      }
    })
  }
}