const { proxy } = require('@x/expressions/src/observable')

// allow child components to register observables to be disconnected when the target is disconnected
module.exports = parent => {
  let observables = []
  const target = proxy(parent, { source: true })
  const disconnectProxy = target.disconnect

  target.registerDisconnectObservable = observable => { observables = [...observables, observable] }
  target.releaseDisconnectObservable = observable => { observables = observables.filter(x => x !== observable) }

  target.disconnect = () => {
    disconnectProxy()
    observables.forEach(x => x.disconnect())
  }

  return target
}

module.exports.registerDisconnectObservable = (inputObservable, observable = inputObservable) =>
  findDisconnectTarget(inputObservable).registerDisconnectObservable(observable)

module.exports.releaseDisconnectObservable = (inputObservable, observable = inputObservable) =>
  findDisconnectTarget(inputObservable).releaseDisconnectObservable(observable)

const findDisconnectTarget = inputObservable => {
  if(!inputObservable) {
    throw new Error("Unable to find disconnect target")
  }

  return inputObservable.registerDisconnectObservable
    ? inputObservable
    : findDisconnectTarget(inputObservable.parent)
}