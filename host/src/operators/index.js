const join = require('./join')

module.exports = {
  apply: (expressions, subscriptions) => expressions.addOperator(join(expressions, subscriptions))
}