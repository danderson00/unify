const { registerDisconnectObservable, releaseDisconnectObservable } = require('./disconnectTarget')

module.exports = (expressions, subscriptions) => {
  return {
    type: 'stream',
    identifier: 'join',
    component: inputObservable => {
      return {
        create: (scopeProperty, expression = o => o) => {
          return create(scopeProperty, expression, expressions.unwrap(expression(expressions.subject())))
        },
        unpack: ({ definition, state }) => {
          return create(
            definition.scopeProperty,
            definition.expression,
            state && state.currentValue || expressions.unwrap(definition.expression(expressions.subject()))
          )
        }
      }

      function create(scopeProperty, expression, initialValue) {
        let currentSubscribeRequest, currentObservable, currentJoinValue

        const definition = expressions.extractDefinition(expression)
        const returns = definition.length > 0
          ? definition[definition.length - 1].find(x => x.key === 'returns').value
          : 'stream'

        const resultObservable = expressions.serializable.subject({
          initialValue,
          getDefinition: () => ({
            type: 'stream',
            identifier: 'join',
            returns,
            scopeProperty,
            expression
          }),
          getState: state => ({
            ...state,
            currentJoinValue
          })
        })

        const disconnectObservable = observable => {
          observable.disconnect()
          releaseDisconnectObservable(inputObservable, observable)
        }

        const updateJoin = value => {
          const nextJoinValue = value && value[scopeProperty]

          if (currentJoinValue !== nextJoinValue) {
            currentJoinValue = nextJoinValue

            if (currentObservable) {
              disconnectObservable(currentObservable)
            }

            if (currentSubscribeRequest) {
              currentSubscribeRequest.then(disconnectObservable)
            }

            if (nextJoinValue) {
              currentSubscribeRequest = subscriptions.subscribe(definition, { [scopeProperty]: currentJoinValue })
              currentSubscribeRequest.then(observable => {
                currentObservable = observable
                currentSubscribeRequest = undefined
                registerDisconnectObservable(inputObservable, currentObservable)
                currentObservable.subscribe(value => {
                  resultObservable.publish(value)
                })
                resultObservable.publish(currentObservable())
              })
              //.catch() need observable error stream!
            } else {
              currentObservable = undefined
            }
          }
        }

        inputObservable.subscribe(value => {
          updateJoin(value)
        })
        updateJoin(inputObservable())

        return resultObservable
      }
    }
  }
}
