module.exports = (expressionsModule, resolver, hasher, log) => {
  log = log.child({ source: 'unify.host.subscriptions' })
  const expressions = {}
  const expressionKey = (definition, scope) => JSON.stringify({ definition: hasher(definition), scope })

  const resolveExpression = (definition, scope, options, debug) => {
    const key = expressionKey(definition, scope)
    const expression = expressions[key] = expressions[key] || {
      promise: resolver(definition, scope, options),
      subscriberCount: 0
    }
    expression.subscriberCount++
    log.debug('Incremented subscriber count', { definition: hasher(definition), count: expression.subscriberCount, scope, ...debug })
    return expression
  }

  const unsubscribe = (definition, scope, expression, disconnect, debug) => {
    expression.subscriberCount--

    if(expression.subscriberCount < 0) {
      log.warn('Subscriber count below 0', { [hasher(definition)]: expression.subscriberCount, scope, ...debug })
    } else {
      log.debug(
        expression.subscriberCount === 0 ? 'Destroyed subscription' : 'Decremented subscriber count',
        { definition: hasher(definition), count: expression.subscriberCount, scope, ...debug }
      )
    }

    if(expression.subscriberCount <= 0) {
        disconnect()
        delete expressions[expressionKey(definition, scope)]
    }
  }

  return {
    subscribe: async (definition, scope = {}, options = {}, debug) => {
      const expression = resolveExpression(definition, scope, options, debug)
      const resolverResult = await expression.promise

      // a little nasty, monkey patch disconnect to reduce the subscriber count
      const proxy = expressionsModule.serializable.proxy(resolverResult.observable)
      const originalDisconnect = proxy.disconnect
      proxy.disconnect = () => {
        originalDisconnect()
        unsubscribe(definition, scope, expression, resolverResult.disconnect, debug)
        proxy.disconnect = originalDisconnect
      }
      // hack, hack... pass on the errorObservable
      proxy.errorObservable = resolverResult.observable.errorObservable

      return proxy
    }
  }
}