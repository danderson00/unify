const { construct } = require('@x/expressions')
const storageModule = require('./storage')
const busModule = require('./bus')
const resolverModule = require('./resolver')
const subscriptionsModule = require('./subscriptions')
const constraintsModule = require('./constraints')
const machinesModule = require('./machines')
const validationModule = require('./validation')
const operators = require('./operators')
const hasher = require('./hasher')
const logger = require('./logger')

const defaultOptions = {
  expressions: construct(),
  hasher,
  scopes: [],
  aspects: {},
  storage: { client: 'sqlite3', connection: { filename: ':memory:' } },
  log: { level: 'info' }
}

module.exports = userOptions => {
  const options = { ...defaultOptions, ...userOptions }

  const log = options.logger || logger(options.log.level)
  const inputObservable = options.expressions.subject()
  const storage = storageModule(options.storage, options.scopes, options.hasher, log)
  const resolver = resolverModule(options.expressions, storage, inputObservable, options.hasher, log)
  const subscriptions = subscriptionsModule(options.expressions, resolver, options.hasher, log)
  const constraints = constraintsModule(options.expressions, subscriptions, options.constraints)
  const machines = machinesModule(options.expressions, options.machines, storage, log)
  const bus = busModule(inputObservable, options.expressions, storage, constraints, machines, log)
  const validate = validationModule(options.expressions, options, log)

  operators.apply(options.expressions, subscriptions)

  return {
    subscribe: validate.subscribe(subscriptions.subscribe),
    subscribeMachine: async (id, scope = {}) => machines.subscribe(id, scope),
    publish: validate.publish(bus.publish),
    purgeExpressionCache: storage.expressions.clear,
    close: storage.close
  }
}
