module.exports = expressions => request => {
  switch(request.type) {
    case 'raw': return createFromDefinition(request.definition)
    case 'vocabulary': return createFromVocabulary(request.name, request.parameters)
  }

  function createFromDefinition(definition) {
    return definition
  }

  function createFromVocabulary(name, parameters = []) {
    return expressions.extractDefinition(o => o.construct.apply(null, [name, ...parameters]))
  }
}