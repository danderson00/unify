module.exports = {
  callAspects: require('./vocabulary'),
  ...require('./types')
}