module.exports = ({
  scoped: (...properties) => ({
    validate: ({ parameters: [{}, scope] }) => {
      const keys = Object.keys(scope || {})
      return arrayEqual(keys, properties) ||
        `Scope must ${properties.length === 0 ? `be empty` :
          `contain properties ${properties.join(', ')}${
            (keys.length > 0) ? ` (passed ${keys.join(', ')})` : ''
          }`
        }`
    },
    context: () => ({ public: true })
  }),
  open: ({
    context: () => ({ public: true })
  }),
  strictApi: ({
    validate: ({ context }) => context.public || `No vocabulary ${context.name}`
  })
})

const arrayEqual = (a, b) => a.length === b.length && a.every(x => b.includes(x))