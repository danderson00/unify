const { v4: uuid } = require('uuid')

module.exports = {
  propertyAspects: {
    uuid: () => ({ newValue: uuid() })
  }
}
