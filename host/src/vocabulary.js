module.exports = (expressions, types, definitions = {}) => {
  const mapVocabulary = (name, definition) => ({
    name,
    definition,
    expression: definition[definition.length - 1]
  })

  const buildDefinitions = source => typeof source === 'function'
    ? types.buildCalls(source)
    : source

  const vocabulary = [].concat(
    ...forceArray(definitions)
      .map(buildDefinitions)
      .map(x => Object.keys(x).map(name =>
        mapVocabulary(name, forceArray(x[name]))
      ))
  )

  vocabulary.forEach(({ name, expression }) => expressions.addVocabulary({ name, expression }))

  return {
    find: name => vocabulary.find(x => x.name === name),
    count: () => vocabulary.length
  }
}

const forceArray = source => source instanceof Array ? source : [source]
