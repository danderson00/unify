const machines = require('@x/unify.fsm/src/machines')

module.exports = (expressions, definitions, storage, log) => {
  const initialise = storage.machines.retrieve().then(
    initialStates => machines(expressions, definitions, initialStates, log)
  )

  return {
    publish: message => initialise.then(api => api.publish(message)),
    validatePublish: message => initialise.then(api => api.validatePublish(message)),
    subscribe: (id, scope) => initialise.then(api => api.subscribe(id, scope))
  }
}