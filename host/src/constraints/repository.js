module.exports = (expressions, subscriptions) => {
  const constraints = []

  const relatedConstraints = message => constraints.filter(constraint =>
    constraint.scopeProps.every(prop => Object.keys(message).includes(prop)) &&
    (constraint.topics.length === 0 || constraint.topics.some(topic => message.topic === topic)) &&
    (!constraint.filter || constraint.filter(message))
  )

  return {
    add: constraint => {
      constraints.push({
        ...constraint,
        definition: expressions.extractDefinition(constraint.expression),
        scopeProps: (constraint.scopeProps || []).sort(),
        topics: (constraint.topics || []),
        activeScopes: {},
        failureMessage: constraint.failureMessage || 'A constraint was violated',
        messageScope(message) {
          return this.scopeProps.reduce(
            (scope, property) => ({ ...scope, [property]: message[property]}),
            {}
          )
        },
        subscriptionFor(message) {
          const scope = this.messageScope(message)
          const key = JSON.stringify(scope)
          // subscriptions are held indefinitely. this will be acceptable for small systems
          // but cache management strategies are required here
          return this.activeScopes[key] = this.activeScopes[key] ||
            subscriptions.subscribe(this.definition, scope)
        }
      })
    },
    getRelatedExpressions: message => (
      Promise.all(relatedConstraints(message).map(constraint => (
        constraint.subscriptionFor(message)
          .then(o => ({
            o,
            failureMessage: typeof constraint.failureMessage === 'function'
              ? constraint.failureMessage(message)
              : constraint.failureMessage
          }))
      )))
    )
  }
}
