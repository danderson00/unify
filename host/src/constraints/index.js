const repository = require('./repository')

module.exports = (expressionsModule, subscriptions, constraints = []) => {
  const container = repository(expressionsModule, subscriptions)

  constraints.forEach(container.add)

  return {
    add: container.add,
    validate: async message => {
      const expressions = await container.getRelatedExpressions(message)

      const validateMessage = (o, message) => {
        const newSource = expressionsModule.subject()
        const cloned = expressionsModule.clone(newSource, o)
        newSource.publish(message)
        return expressionsModule.unwrap(cloned)
      }

      return expressions.reduce(
        (result, { o, failureMessage }) =>
          validateMessage(o, message)
            ? result
            : { valid: false, messages: [...result.messages, failureMessage] },
        { valid: true, messages: [] }
      )
    }
  }
}