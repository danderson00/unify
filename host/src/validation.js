const aspects = require('./aspects')
const vocabularyModule = require('./vocabulary')
const typesModule = require('@x/types')
const resolveDefinitionModule = require('./resolveDefinition')

module.exports = (expressions, configuration, log) => {
  log = log.child({ source: 'unify.host.validation' })

  const types = typesModule({
    throwOnFailure: true,
    types: configuration.types,
    strict: configuration.strictTypes,
    propertyAspects: { ...aspects.propertyAspects, ...configuration.aspects.propertyAspects },
    typeAspects: configuration.aspects.typeAspects,
    callAspects: { ...aspects.callAspects, ...configuration.aspects.callAspects },
    globalTypeAspects: [{ topic: String }],
    globalCallAspects: configuration.strictApi ? [aspects.callAspects.strictApi] : []
  })

  const vocabulary = vocabularyModule(expressions, types, configuration.vocabulary)
  const resolveDefinition = resolveDefinitionModule(expressions)

  log.info(`Loaded ${types.typeCount()} topic types`)
  log.info(`Loaded ${vocabulary.count()} vocabulary entries`)

  return {
    publish: publish => async (message, context) => publish(types.apply(message.topic, message, context), context),
    subscribe: subscribe => async (request, scope = {}, options = {}) => {
      if(request.type === 'vocabulary') {
        const definition = vocabulary.find(request.name)
        if(!definition) {
          throw new Error(`No vocabulary ${request.name}`)
        }

        const subscribeAspect = {
          target: () => (request, scope, options) => subscribe(resolveDefinition(request), scope, options, request.type === 'vocabulary' && { vocabulary: request.name })
        }

        return types.call(
          [subscribeAspect, ...definition.definition],
          [request, scope, options],
          { ...options.context, name: request.name, types }
        )

      } else if (request.type === 'raw') {
        if(configuration.allowRawQueries === false || configuration.strictApi) {
          throw new Error('Raw queries are not allowed on this host')
        }
        return subscribe(resolveDefinition(request), scope, options)

      } else {
        throw new Error(`Unknown request type: ${request.type}`)
      }
    }
  }
}
