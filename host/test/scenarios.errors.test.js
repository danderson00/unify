const hostModule = require('..')
const expressions = require('@x/expressions')
const log = require('@x/log')

const hostConfig = { log: { level: 'fatal' } }

test("expression errors are exposed on the errorObservable", async () => {
  const host = hostModule(hostConfig)
  const definition = expressions.extractDefinition(o => o.map(() => { throw new Error('test') }))

  const o = await host.subscribe({ type: 'raw', definition })
  await host.publish({})

  expect(o.errorObservable()).toEqual({ frames: [{ operator: 'map', position: 1 }], error: new Error('test') })
})

test("expression errors survive serialization", async () => {
  const host = hostModule(hostConfig)
  const definition = expressions.extractDefinition(o => o.map(() => { throw new Error('test') }))

  let o = await host.subscribe({ type: 'raw', definition })
  await host.publish({})
  o.disconnect()

  o = await host.subscribe({ type: 'raw', definition })
  expect(o.errorObservable()).toEqual({ frames: [{ operator: 'map', position: 1 }], error: new Error('test') })
})

test("expression errors are logged as they occur", async () => {
  const spy = jest.fn()
  const host = hostModule({ logger: log({ level: 'error', writers: [() => spy] }) })
  const definition = expressions.extractDefinition(o => o.map(() => { throw new Error('test') }))

  await host.subscribe({ type: 'raw', definition })
  expect(spy.mock.calls.length).toBe(0)
  await host.publish({})
  await host.subscribe({ type: 'raw', definition })
  await host.publish({})
  expect(spy.mock.calls.length).toBe(1)
})

test("expression errors are logged during rehydration", async () => {
  const spy = jest.fn()
  const host = hostModule({ logger: log({ level: 'error', writers: [() => spy] }) })
  const definition = expressions.extractDefinition(o => o.map(() => { throw new Error('test') }))

  await host.publish({})
  expect(spy.mock.calls.length).toBe(0)
  await host.subscribe({ type: 'raw', definition })
  await host.publish({})
  await host.subscribe({ type: 'raw', definition })
  expect(spy.mock.calls.length).toBe(1)
})

test("constructed vocabulary containing assign", async () => {
  const host = hostModule({ ...hostConfig, vocabulary: {
    contractor: o => o.assign({
      contractorId: o => o.topic('contractor').select('contractorId'),
      '...details': o => o.topic('contractor').accumulate('contractor'),
      averageRating: o => o.map(x => asd)
    })
  }})

  let o = await host.subscribe({ type: 'vocabulary', name: 'contractor' })
  await host.publish({})
  o.disconnect()

  o = await host.subscribe({ type: 'vocabulary', name: 'contractor' })
  expect(o.errorObservable()).toEqual({
    "error": new ReferenceError('asd is not defined'),
    "frames": [
      {
        "operator": "contractor",
        "position": 1
      },
      {
        "key": "averageRating",
        "operator": "assign",
        "position": 1
      },
      {
        "operator": "map",
        "position": 1
      }
    ]
  })
})