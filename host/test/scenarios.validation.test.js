const hostModule = require('..')
const { construct } = require('@x/expressions')

const hostConfig = { log: { level: 'fatal' } }
const uuidRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i

test("simple types are validated", async () => {
  const host = hostModule({
    ...hostConfig,
    types: ({ required }) => ({
      simpleType: {
        id: [Number, required],
        description: String
      }
    })
  })

  await host.publish({ topic: 'unknownType', someOtherProperty: 'test' })
  await host.publish({ topic: 'simpleType', id: 1, description: 'test' })
  await expect(host.publish({ topic: 'simpleType', description: 'test' })).rejects.toMatchObject({ message: 'id is required' })
  await expect(host.publish({ topic: 'simpleType', id: 'test' })).rejects.toMatchObject({ message: 'id must be a Number' })
  await expect(host.publish({ topic: 'simpleType', description: 1 })).rejects.toMatchObject({ message: 'id is required. description must be a String' })
})

test("strictTypes option requires types to be defined and throws if extra properties exist", async () => {
  const host = hostModule({
    ...hostConfig,
    strictTypes: true,
    types: ({ required }) => ({
      simpleType: {
        id: [Number, required],
        description: String
      }
    })
  })

  await host.publish({ topic: 'simpleType', id: 1, description: 'test' })
  await expect(host.publish({ topic: 'unknownType', someOtherProperty: 'test' })).rejects.toMatchObject({ message: 'No type with id unknownType registered' })
  await expect(host.publish({ topic: 'simpleType', someOtherProperty: 'test' })).rejects.toMatchObject({ message: 'id is required. someOtherProperty is an invalid property name' })
})

test("simple vocabulary validation", async () => {
  const expressionsInstance = construct()
  const host = hostModule({
    ...hostConfig,
    strictTypes: true,
    expressions: expressionsInstance,
    vocabulary: {
      test: [
        { validate: ({ context }) => context.authenticated || 'You must be authenticated' },
        o => o
      ]
    }
  })
  await expect(host.subscribe({ type: 'vocabulary', name: 'test' })).rejects.toMatchObject({ message: 'You must be authenticated' })
  await host.subscribe({ type: 'vocabulary', name: 'test' }, {}, { context: { authenticated: true } })
})

test("scoped vocabulary", async () => {
  const expressionsInstance = construct()
  const host = hostModule({
    ...hostConfig,
    scopes: ['p1', 'p2'],
    strictTypes: true,
    expressions: expressionsInstance,
    vocabulary: ({ scoped }) => ({
      scoped: [
        scoped('p1', 'p2'),
        o => o
      ]
    })
  })

  expect(host.subscribe({ type: 'vocabulary', name: 'scoped' })).rejects.toMatchObject({ message: 'Scope must contain properties p1, p2' })
  expect(host.subscribe({ type: 'vocabulary', name: 'scoped' }, { p1: 1 })).rejects.toMatchObject({ message: 'Scope must contain properties p1, p2 (passed p1)' })
  expect(host.subscribe({ type: 'vocabulary', name: 'scoped' }, { p1: 1, p2: 2, p3: 3 })).rejects.toMatchObject({ message: 'Scope must contain properties p1, p2 (passed p1, p2, p3)' })
  await host.subscribe({ type: 'vocabulary', name: 'scoped' }, { p1: 1, p2: 2 })
})

test("empty scoped vocabulary", async () => {
  const expressionsInstance = construct()
  const host = hostModule({
    ...hostConfig,
    strictTypes: true,
    expressions: expressionsInstance,
    vocabulary: ({ scoped }) => ({
      scoped: [
        scoped(),
        o => o
      ]
    })
  })

  expect(host.subscribe({ type: 'vocabulary', name: 'scoped' }, { p1: 1 })).rejects.toMatchObject({ message: 'Scope must be empty' })
  expect(host.subscribe({ type: 'vocabulary', name: 'scoped' }, { p1: 1, p2: 2, p3: 3 })).rejects.toMatchObject({ message: 'Scope must be empty' })
  await host.subscribe({ type: 'vocabulary', name: 'scoped' })
  await host.subscribe({ type: 'vocabulary', name: 'scoped' }, {})
})

test("strictApi option requires vocabulary to be marked as open or scoped", async () => {
  const expressionsInstance = construct()
  const host = hostModule({
    ...hostConfig,
    strictApi: true,
    expressions: expressionsInstance,
    vocabulary: ({ open, scoped }) => ({
      open: [open, o => o],
      scoped: [scoped(), o => o],
      private: o => o
    })
  })

  await expect(host.subscribe({ type: 'vocabulary', name: 'private' })).rejects.toMatchObject({ message: 'No vocabulary private' })
  await host.subscribe({ type: 'vocabulary', name: 'open' })
  await host.subscribe({ type: 'vocabulary', name: 'scoped' })
})

test("uuid property aspect assigns new uuid each publish", async () => {
  const host = hostModule({
    ...hostConfig,
    types: ({ uuid }) => ({
      simpleType: {
        id: [uuid]
      }
    })
  })

  let { message } = await host.publish({ topic: 'simpleType' })
  expect(message.id.match(uuidRegex)).toBeTruthy()
  message = (await host.publish({ topic: 'simpleType' })).message
  expect(message.id.match(uuidRegex)).toBeTruthy()
})
