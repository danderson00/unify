const hostModule = require('..')
const { construct, extractDefinition, unwrap } = require('@x/expressions')

const hostConfig = { scopes: ['relatedId', 'orderId'], log: { level: 'fatal' } }
const delay = (ms = 50) => new Promise(r => setTimeout(r, ms))

test("simple join operation", async () => {
  const expressions = construct()
  const host = hostModule({ ...hostConfig, expressions })
  const definition = expressions.extractDefinition(o => o.topic('test').join('relatedId'))
  let o = await host.subscribe({ type: 'raw', definition })

  await host.publish({ topic: 'test', relatedId: 1 })
  await host.publish({ relatedId: 1, value: 2 })

  await delay()
  expect(o()).toEqual({ relatedId: 1, value: 2 })

  await host.publish({ relatedId: 1, value: 3 })
  await delay()
  expect(o()).toEqual({ relatedId: 1, value: 3 })
})

test("join with expression", async () => {
  const expressions = construct()
  const host = hostModule({ ...hostConfig, expressions })
  const definition = expressions.extractDefinition(o => o.topic('test').join('relatedId', o => o.select('value')))
  let o = await host.subscribe({ type: 'raw', definition })

  await host.publish({ topic: 'test', relatedId: 1 })

  await host.publish({ topic: 'test2', relatedId: 1, value: 2 })
  await delay()
  expect(o()).toEqual(2)

  await host.publish({ topic: 'test2', relatedId: 1, value: 3 })
  await delay()
  expect(o()).toEqual(3)
})

test("composed join", async () => {
  const expressions = construct()
  const host = hostModule({ expressions, scopes: ['productId', 'orderId'], log: { level: 'fatal' } })
  const definition = expressions.extractDefinition(
    o => o.groupBy('productId',
      o => o.compose(
        o => o.topic('productAdded').accumulate('detail'),
        o => o.join('productId', o => o.topic('product').accumulate('detail')),
        (addedDetails, productDetails) => ({
          ...addedDetails,
          ...productDetails,
          total: addedDetails.quantity * productDetails.price
        })
      )
    )
  )
  let o = await host.subscribe({ type: 'raw', definition }, { orderId: 1 })

  await host.publish({ topic: 'product', productId: 1, detail: { name: 'widget', price: 1 } })
  await host.publish({ topic: 'product', productId: 2, detail: { name: 'component', price: 3 } })
  await host.publish({ topic: 'productAdded', orderId: 1, productId: 1, detail: { quantity: 2 } })
  await host.publish({ topic: 'productAdded', orderId: 1, productId: 2, detail: { quantity: 1 } })
  await delay()

  expect(unwrap(o)).toEqual([
    { name: 'widget', price: 1, quantity: 2, total: 2 },
    { name: 'component', price: 3, quantity: 1, total: 3 }
  ])
})

test("join as vocabulary", async () => {
  const expressions = construct()
  const host = hostModule({ expressions, scopes: ['productId', 'orderId'], log: { level: 'fatal' }, vocabulary: {
    joinToProduct: o => o.join('productId', o => o.topic('product').accumulate('detail'))
  } })

  const definition = expressions.extractDefinition(o => o.joinToProduct())
  // let o = await host.subscribe({ type: 'raw', definition }, { orderId: 1 })
  let o = await host.subscribe({ type: 'vocabulary', name: 'joinToProduct' }, { orderId: 1 })

  await host.publish({ topic: 'product', productId: 1, detail: { name: 'widget', price: 1 } })
  await host.publish({ topic: 'productAdded', orderId: 1, productId: 1 })
  await delay()

  expect(unwrap(o)).toEqual({ name: 'widget', price: 1 })
}, 1000000)

test("join as composed vocabulary", async () => {
  const expressions = construct()
  expressions.addVocabulary({ joinToProduct: o => o.join('productId', o => o.topic('product').accumulate('detail')) })
  const host = hostModule({ expressions, scopes: ['productId', 'orderId'], log: { level: 'fatal' } })

  const definition = expressions.extractDefinition(o => o.assign({ join: o => o.joinToProduct() }))
  let o = await host.subscribe({ type: 'raw', definition }, { orderId: 1 })

  await host.publish({ topic: 'product', productId: 1, detail: { name: 'widget', price: 1 } })
  await host.publish({ topic: 'productAdded', orderId: 1, productId: 1 })
  await delay()

  expect(unwrap(o)).toEqual({ join: { name: 'widget', price: 1 } })
}, 1000000)

test("nested grouped join", async () => {
  const expressions = construct()
  const host = hostModule({ expressions, scopes: ['productId', 'orderId'], log: { level: 'fatal' } })
  const definition = expressions.extractDefinition(
    o => o.compose(
      o => o.topic('productAdded').groupBy('productId',
        o => o.join('productId', o => o.topic('product').accumulate()),
      ),
      items => ({
        items,
        total: items.reduce((total, item) => total + item.price, 0)
      })
    )
  )
  let o = await host.subscribe({ type: 'raw', definition }, { orderId: 1 })

  await host.publish({ topic: 'product', productId: 1, name: 'widget', price: 1 })
  await host.publish({ topic: 'product', productId: 2, name: 'component', price: 3 })
  await host.publish({ topic: 'productAdded', orderId: 1, productId: 1 })
  await host.publish({ topic: 'productAdded', orderId: 1, productId: 2 })
  await delay()

  expect(unwrap(o)).toEqual({
    "items": [{"name": "widget","price": 1,"productId": 1,"topic": "product"},{"name": "component","price": 3,"productId": 2,"topic": "product"}],
    "total": 4
  })
}, 1000000)

test("nested grouped join as requested vocabulary", async () => {
  const expressions = construct()
  const host = hostModule({ expressions, scopes: ['productId', 'orderId'], log: { level: 'fatal' }, vocabulary: {
    order: o => o.compose(
      o => o.topic('productAdded').groupBy('productId',
        o => o.join('productId', o => o.topic('product').accumulate()),
      ),
      items => ({
        items,
        total: items.reduce((total, item) => total + item.price, 0)
      })
    )
  } })
  let o = await host.subscribe({ type: 'vocabulary', name: 'order' }, { orderId: 1 })

  await host.publish({ topic: 'product', productId: 1, name: 'widget', price: 1 })
  await host.publish({ topic: 'product', productId: 2, name: 'component', price: 3 })
  await host.publish({ topic: 'productAdded', orderId: 1, productId: 1 })
  await host.publish({ topic: 'productAdded', orderId: 1, productId: 2 })
  await delay()

  expect(unwrap(o)).toEqual({
    "items": [{"name": "widget","price": 1,"productId": 1,"topic": "product"},{"name": "component","price": 3,"productId": 2,"topic": "product"}],
    "total": 4
  })
})