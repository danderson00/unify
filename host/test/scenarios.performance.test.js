const hostModule = require('..')
const expressions = require('@x/expressions')

const hostConfig = { scopes: ['p1'], log: { level: 'fatal' } }

test("simple subscriptions only pulse when observable pulses", async () => {
  const host = hostModule(hostConfig)
  const definition = expressions.extractDefinition(o => o.where(x => x % 2 === 0))
  let o = await host.subscribe({ type: 'raw', definition })

  const spy = jest.fn()
  o.subscribe(spy)

  await host.publish(2)
  expect(o()).toBe(2)
  expect(spy.mock.calls.length).toBe(1)

  await host.publish(3)
  expect(o()).toBe(2)
  expect(spy.mock.calls.length).toBe(1)

  await host.publish(4)
  expect(o()).toBe(4)
  expect(spy.mock.calls.length).toBe(2)
})

test("scoped subscriptions only pulse when scope is satisfied", async () => {
  const host = hostModule(hostConfig)
  const definition = expressions.extractDefinition(o => o.count())
  let o = await host.subscribe({ type: 'raw', definition }, { p1: 1 })

  const spy = jest.fn()
  o.subscribe(spy)

  await host.publish({ p1: 1 })
  expect(o()).toBe(1)
  expect(spy.mock.calls.length).toBe(1)

  await host.publish({ p1: 2 })
  expect(o()).toBe(1)
  expect(spy.mock.calls.length).toBe(1)

  await host.publish({ p1: 1 })
  expect(o()).toBe(2)
  expect(spy.mock.calls.length).toBe(2)
})
