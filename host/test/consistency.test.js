const hostModule = require('..')
const expressions = require('@x/expressions')

const delay = delay => new Promise(r => setTimeout(r, 20))

test("general asynchrony", async () => {
  const host = hostModule({ log: { level: 'fatal' } })
  const definition = expressions.extractDefinition(o => o.count())
  let o = await host.subscribe({ type: 'raw', definition })
  host.publish(1)
  host.publish(1)
  o.disconnect()

  host.publish(1)

  o = await host.subscribe({ type: 'raw', definition })
  host.publish(1)
  await host.publish(1)

  await delay(20)
  expect(o()).toBe(5)
  o.disconnect()

  o = await host.subscribe({ type: 'raw', definition })

  await delay(20)
  expect(o()).toBe(5)

  host.publish(1)
  host.publish(1)

  await delay(50)
  expect(o()).toBe(7)
  o.disconnect()

  host.publish(1)
  const promise = host.subscribe({ type: 'raw', definition })
  host.publish(1)
  o = await promise
  await delay(20)
  expect(o()).toBe(9)
})
