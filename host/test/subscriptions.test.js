const subscriptions = require('../src/subscriptions')
const hasher = require('../src/hasher')
const logger = require('../src/logger')
const expressions = require('@x/expressions')

const log = logger('none')

test("resolver function is called to obtain observable", async () => {
  const resolver = jest.fn(() => ({ observable: expressions.subject() }))
  const subs = subscriptions(expressions, resolver, hasher, log)
  const o = await subs.subscribe('q', 's')
  expect(resolver.mock.calls.length).toBe(1)
  expect(resolver.mock.calls[0]).toEqual(['q', 's', {}])
  expect(o.disconnect).toBeInstanceOf(Function)
})

test("resolver results are cached per query and scope", async () => {
  const resolver = jest.fn(() => ({ observable: expressions.subject() }))
  const subs = subscriptions(expressions, resolver, hasher, log)
  const o1 = await subs.subscribe('q', 's')
  const o2 = await subs.subscribe('q', 's')
  expect(resolver.mock.calls.length).toBe(1)
})

test("cached observables are released when all connections are released", async () => {
  const resolver = jest.fn(() => ({ observable: expressions.subject(), disconnect: () => {} }))
  const subs = subscriptions(expressions, resolver, hasher, log)
  const o1 = await subs.subscribe('q', 's')
  const o2 = await subs.subscribe('q', 's')
  expect(resolver.mock.calls.length).toBe(1)
  o1.disconnect()
  const o3 = await subs.subscribe('q', 's')
  expect(resolver.mock.calls.length).toBe(1)
  o2.disconnect()
  o3.disconnect()
  const o4 = await subs.subscribe('q', 's')
  expect(resolver.mock.calls.length).toBe(2)
})

test("immediately subsequent subscriptions do not cause multiple resolver requests", () => {
  const resolver = jest.fn(() => ({ observable: expressions.subject() }))
  const subs = subscriptions(expressions, resolver, hasher, log)
  subs.subscribe('q', 's1')
  subs.subscribe('q', 's1')
  subs.subscribe('q', 's2')
  expect(resolver.mock.calls.length).toBe(2)
})