const hostModule = require('..')

const hostConfig = { scopes: ['id'], log: { level: 'fatal' } }

test("simple machine", async () => {
  const host = hostModule({ ...hostConfig, machines: [{
      id: 'machine',
      startTopic: 'start',
      scopeProps: ['id'],
      machine: {
        initial: '1',
        states: {
          '1': { on: { 'two': '2' } },
          '2': { on: { 'one': '1' } }
        }
      }
    }] })

  await host.publish({ topic: 'start', id: 1 })
  const value = await host.subscribeMachine('machine', { id: 1 })
  expect(value()).toBe('1')
  await host.publish({ topic: 'two', id: 1 })
  expect(value()).toBe('2')
})

test("publishing different scope", async () => {
  const host = hostModule({ ...hostConfig, machines: [{
      id: 'machine',
      startTopic: 'start',
      scopeProps: ['id'],
      machine: {
        initial: '1',
        states: {
          '1': { on: { 'two': '2' } },
          '2': { on: { 'one': '1' } }
        }
      }
    }] })

  await host.publish({ topic: 'start', id: 1 })
  const value = await host.subscribeMachine('machine', { id: 1 })
  expect(value()).toBe('1')
  await host.publish({ topic: 'two', id: 2 })
  expect(value()).toBe('1')
})
