const resolver = require('../src/resolver')
const storage = require('../src/storage')
const hasher = require('../src/hasher')
const logger = require('../src/logger')
const expressions = require('@x/expressions')

const log = logger('none')
const createStorage = () => (
  storage(
    { client: 'sqlite3', connection: { filename: ':memory:' } },
    ['id'],
    hasher,
    log
  )
)

test("resolve returns a scoped observable", async () => {
  const source = expressions.subject()
  const resolve = resolver(expressions, createStorage(), source, hasher, log)
  const o = (await resolve(expressions.unzip(source).definition, { id: 1 })).observable

  expect(o()).toBeUndefined()
  source.publish({ message: { id: 1, v: 1 } })
  expect(o().v).toBe(1)
  source.publish({ message: { id: 2, v: 2 } })
  expect(o().v).toBe(1)
  source.publish({ message: { id: 1, v: 3 } })
  expect(o().v).toBe(3)
})

test("previously published messages are replayed", async () => {
  const source = expressions.subject()
  const storage = createStorage()
  const resolve = resolver(expressions, storage, source, hasher, log)

  await storage.messages.store({ message: { v: 1 } })
  const o = (await resolve(expressions.unzip(source).definition, {})).observable
  expect(o().v).toBe(1)
})

test("expression state is cached", async () => {
  const source = expressions.subject()
  const storage = createStorage()
  const resolve = resolver(expressions, storage, source, hasher, log)
  const definition = expressions.unzip(source).definition

  await storage.messages.store({ message: { v: 1 } })
  await resolve(definition, {})

  const storedExpression = await storage.expressions.retrieve(definition, {})
  expect(storedExpression.state).not.toBeUndefined()
})

test("messages are replayed through transient subscriptions", async () => {
  const source = expressions.subject()
  const storage = createStorage()
  const resolve = resolver(expressions, storage, source, hasher, log)
  const definition = expressions.unzip(source).definition

  await storage.messages.store({ message: { v: 1 } })
  const o = (await resolve(definition, {}, { transient: true })).observable
  expect(o().v).toBe(1)
})

test("expression state is not cached for transient subscriptions", async () => {
  const source = expressions.subject()
  const storage = createStorage()
  const resolve = resolver(expressions, storage, source, hasher, log)
  const definition = expressions.unzip(source).definition

  await storage.messages.store({ message: { v: 1 } })
  await resolve(definition, {}, { transient: true })
  const storedExpression = await storage.expressions.retrieve(definition, {})
  expect(storedExpression).toBeUndefined()
})

test("currently publishing messages are replayed", async () => {
  const source = expressions.subject()
  const storage = createStorage()
  const resolve = resolver(expressions, storage, source, hasher, log)

  storage.messages.store({ message: { v: 1 } }) // no await here
  const o = (await resolve(expressions.unzip(source).definition, {})).observable
  expect(o().v).toBe(1)
})

test("immediately published messages are not duplicated", async () => {
  const source = expressions.subject().count()
  const storage = createStorage()
  const resolve = resolver(expressions, storage, source, hasher, log)

  const [o, envelope] = await Promise.all([
    resolve(expressions.unzip(source).definition, {}),
    storage.messages.store({ message: { v: 1 } })
  ])
  expect(o.observable()).toBe(1)
})