const hostModule = require('..')
const { construct } = require('@x/expressions')

test("subscriptions can be taken out by vocabulary name", async () => {
  const expressions = construct()
  const host = hostModule({ log: { level: 'fatal' }, expressions, vocabulary: {
    sumOfEvens: o => o.where(x => x % 2 === 0).sum()
  } })
  const o = await host.subscribe({ type: 'vocabulary', name: 'sumOfEvens' })
  expect(o()).toBe(0)
  await host.publish(2)
  await host.publish(3)
  expect(o()).toBe(2)
})

test("vocabulary subscriptions can be parameterized", async () => {
  const expressions = construct()
  const host = hostModule({ log: { level: 'fatal' }, expressions, vocabulary: {
    sumOfMultiples: (o, multiple) => o.where(x => x % multiple === 0).sum()
  } })
  const o = await host.subscribe({ type: 'vocabulary', name: 'sumOfMultiples', parameters: [3] })
  expect(o()).toBe(0)
  await host.publish(2)
  await host.publish(3)
  expect(o()).toBe(3)
})

test("parameterised vocabulary subscriptions are unique for different parameters", async () => {
  const expressions = construct()
  const host = hostModule({ log: { level: 'fatal' }, expressions, vocabulary: {
    sumOfMultiples: (o, multiple) => o.where(x => x % multiple === 0).sum()
  } })
  await host.subscribe({ type: 'vocabulary', name: 'sumOfMultiples', parameters: [3] })
  await host.publish(3)
  const o = await host.subscribe({ type: 'vocabulary', name: 'sumOfMultiples', parameters: [2] })
  expect(o()).toBe(0)
})

test("vocabulary can be specified as an array", async () => {
  const expressions = construct()
  const host = hostModule({ log: { level: 'fatal' }, expressions, vocabulary: [
    { isMultiple: (o, multiple) => o.where(x => x % multiple === 0) },
    { sumOfMultiples: (o, multiple) => o.isMultiple(multiple).sum() }
  ] })
  const o = await host.subscribe({ type: 'vocabulary', name: 'sumOfMultiples', parameters: [3] })
  await host.publish(3)
  expect(o()).toBe(3)
})

test("vocabulary can executed as a raw expression", async () => {
  const expressions = construct()
  const host = hostModule({ log: { level: 'fatal' }, expressions, vocabulary: {
    innerCount: o => o.count(),
    getCount: o => o.innerCount()
  } })
  // const o = await host.subscribe({
  //   type: 'raw',
  //   definition: expressions.extractDefinition(o => o.getCount())
  // })
  const o = await host.subscribe({
    type: 'vocabulary',
    name: 'getCount'
  })
  expect(o()).toBe(0)
  await host.publish({})
  expect(o()).toBe(1)
})
