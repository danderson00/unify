const { sortObject, dedupe, debounceAsync } = require('../src/utilities')

test("sortObject returns object with keys sorted alphabetically", () => {
  expect(Object.keys(sortObject({ a: 1, c: 3, b: 2 }))).toEqual(['a', 'b', 'c'])
})

test("dedupe removes already replayed messages from subsequent array", () => {
  expect(dedupe([
    { timestamp: 1 }
  ], [
    { timestamp: 1 },
    { timestamp: 2 }
  ])).toEqual([
    { timestamp: 1 },
    { timestamp: 2 }
  ])
})

test("dedupe handles empty arrays", () => {
  expect(dedupe([], [{ timestamp: 1 }])).toEqual([{ timestamp: 1 }])
  expect(dedupe([{ timestamp: 1 }], [])).toEqual([{ timestamp: 1 }])
})

test("debounceAsync combines subsequent requests and pauses until first completes", async () => {
  const spy = jest.fn()
  let callNumber = 0

  const target = () => new Promise(resolve => {
    setTimeout(() => {
      spy(++callNumber)
      resolve()
    }, 10)
  })
  const debounced = debounceAsync(target)

  const promise1 = debounced()
  const promise2 = debounced()
  const promise3 = debounced()

  expect(spy.mock.calls).toEqual([])
  await promise1
  expect(spy.mock.calls).toEqual([[1]])
  await promise2
  expect(spy.mock.calls).toEqual([[1], [2]])
  await promise3
  expect(spy.mock.calls).toEqual([[1], [2]])
})

test("debounceAsync handles ongoing executions", async () => {
  const spy = jest.fn()
  let callNumber = 0

  const target = () => new Promise(resolve => {
    setTimeout(() => {
      spy(++callNumber)
      resolve()
    }, 10)
  })
  const debounced = debounceAsync(target)

  // first execution will trigger immediately
  const promise1 = debounced()
  // second will wait until first completes
  const promise2 = debounced()
  await promise1
  await new Promise(resolve => setTimeout(resolve, 5))
  // second execution is still running, wait until it completes
  const promise3 = debounced()
  // combined with third request
  const promise4 = debounced()

  expect(spy.mock.calls).toEqual([[1]])
  await promise2
  expect(spy.mock.calls).toEqual([[1], [2]])
  await promise3
  expect(spy.mock.calls).toEqual([[1], [2], [3]])
  await promise4
  expect(spy.mock.calls).toEqual([[1], [2], [3]])
})
