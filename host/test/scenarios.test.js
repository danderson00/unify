const hostModule = require('..')
const expressions = require('@x/expressions')

const hostConfig = { log: { level: 'fatal' } }

test("retrieval of expression twice", async () => {
  const host = hostModule(hostConfig)
  const definition = expressions.extractDefinition(o => o.count())

  await host.publish(1)
  await host.publish(1)
  let o = await host.subscribe({ type: 'raw', definition })
  o.disconnect()

  o = await host.subscribe({ type: 'raw', definition })
  expect(o()).toEqual(2)
})

test("retrieval of vocabulary", async () => {
  const instance = expressions.construct()
  const host = hostModule({
    expressions: instance,
    vocabulary: {
      rating: o => o
        .topic('rating')
        .groupBy('category',
          o => o.groupBy('userId').average('value')
        )
        .average()
    },
    log: { level: 'fatal' }
  })
  const definition = instance.extractDefinition(o => o.rating())

  await host.publish({ topic: 'rating', category: 1, userId: 1, value: 1 })
  let o = await host.subscribe({ type: 'raw', definition })
  expect(o()).toBe(1)
  o.disconnect()
  o = await host.subscribe({ type: 'raw', definition })
  expect(o()).toBe(1)
})

test("retrieval of vocabulary by name", async () => {
  const host = hostModule({
    vocabulary: { 
      rating: o => o
        .topic('rating')
        .groupBy('category', 
          o => o.groupBy('userId').average('value')
        )
        .average()
    },
    log: { level: 'fatal' } 
  })

  await host.publish({ topic: 'rating', category: 1, userId: 1, value: 1 })
  let o = await host.subscribe({ type: 'vocabulary', name: 'rating' })
  expect(o()).toBe(1)
  o.disconnect()
  o = await host.subscribe({ type: 'vocabulary', name: 'rating' })
  expect(o()).toBe(1)
})

test("subscriptions are updated before publish promise resolves", async () => {
  const host = hostModule(hostConfig)
  const definition = expressions.extractDefinition(o => o.count())

  const spy = jest.fn()
  const o = await host.subscribe({ type: 'raw', definition })
  o.subscribe(spy)

  await host.publish({}).then(spy)
  expect(spy.mock.calls.length).toBe(2)
  expect(spy.mock.calls[0][0]).toBe(1)
})
