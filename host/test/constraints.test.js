const constraints = require('../src/constraints')
const expressionsModule = require('@x/expressions')

const setup = (expressions, scopeProps, topics, filter, failureMessage) => {
  const sources = []
  const subscribe = jest.fn((definition, scope) => {
    const key = JSON.stringify({ definition, scope })
    const source = expressionsModule.subject()
    let sourceProps = sources.find(x => x.key === key)
    if(!sourceProps) {
      sourceProps = { key, definition, scope, source, o: expressionsModule.zip(source, definition) }
      sources.push(sourceProps)
    }
    return Promise.resolve(sourceProps.o)
  })
  const container = constraints(expressionsModule, { subscribe });
  [].concat(...[expressions]).forEach(expression => {
    container.add({ expression, scopeProps, topics, filter, failureMessage: failureMessage || 'fail' })
  })
  return { container, sources }
}

test("simple constraint with no scope", async () => {
  const { container, sources } = setup(o => o.map(x => x % 2))
  expect(sources.length).toBe(0)
  expect((await container.validate(1)).valid).toBe(true)
  expect((await container.validate(2))).toEqual({ valid: false, messages: ['fail'] })
  expect(sources.length).toBe(1)
})

test("contextual constraint with no scope", async () => {
  const { container, sources } = setup(o => o.count().map(x => x < 2))
  expect((await container.validate(1)).valid).toBe(true)
  sources[0].source.publish(1)
  expect((await container.validate(1))).toEqual({ valid: false, messages: ['fail'] })
  expect(sources.length).toBe(1)
})

test("contextual constraint with scope", async () => {
  const { container, sources } = setup(o => o.count().map(x => x < 2), ['category'])
  expect((await container.validate({ category: 1 })).valid).toBe(true)
  expect((await container.validate({ category: 2 })).valid).toBe(true)
  expect(sources.length).toBe(2)

  sources[0].source.publish({ category: 1 })
  expect((await container.validate({ category: 1 })).valid).toBe(false)
  expect((await container.validate({ category: 2 })).valid).toBe(true)
  expect(sources.length).toBe(2)

  sources[1].source.publish({ category: 2 })
  expect((await container.validate({ category: 1 })).valid).toBe(false)
  expect((await container.validate({ category: 2 })).valid).toBe(false)
  expect(sources.length).toBe(2)
})

test("multiple contextual constraints", async () => {
  const { container, sources } = setup([
    o => o.count().map(x => x < 3),
    o => o.sum('value').map(x => x >= 0)
  ], ['id'])

  expect((await container.validate({ id: 1, value: 1 })).valid).toBe(true)
  expect((await container.validate({ id: 1, value: -1 })).valid).toBe(false)
  expect(sources.length).toBe(2)
  expect((await container.validate({ id: 2, value: 1 })).valid).toBe(true)
  expect((await container.validate({ id: 2, value: -1 })).valid).toBe(false)
  expect(sources.length).toBe(4)

  sources[0].source.publish({ value: 1 })
  expect((await container.validate({ id: 1, value: 1 })).valid).toBe(true)

  sources[0].source.publish({ value: 1 })
  sources[3].source.publish({ value: 1 })

  expect((await container.validate({ id: 1, value: 1 })).valid).toBe(false)
  expect((await container.validate({ id: 2, value: -1 })).valid).toBe(true)
  expect((await container.validate({ id: 2, value: -2 })).valid).toBe(false)
  expect(sources.length).toBe(4)
})

test("simple constraint with topics", async () => {
  const { container } = setup(o => o.map(x => x.value !== 2), [], ['validated'])
  expect((await container.validate({ topic: 'not validated', value: 2 })).valid).toBe(true)
  expect((await container.validate({ topic: 'validated', value: 1 })).valid).toBe(true)
  expect((await container.validate({ topic: 'validated', value: 2 })).valid).toBe(false)
})

test("simple constraint with filter", async () => {
  const { container } = setup(o => o.map(x => x.value !== 2), undefined, undefined, x => x.value % 2 === 0)
  expect((await container.validate({ value: 1 })).valid).toBe(true)
  expect((await container.validate({ value: 2 })).valid).toBe(false)
  expect((await container.validate({ value: 4 })).valid).toBe(true)
})

test("simple constraint with failureMessage function", async () => {
  const { container } = setup(o => o.map(x => false), undefined, undefined, undefined, message => `fail ${message.text}`)
  expect((await container.validate({ text: 'test' }))).toEqual({ valid: false, messages: ['fail test'] })
})