const hostModule = require('..')

const hostConfig = { scopes: ['id1', 'id2'], log: { level: 'fatal' } }

test("simple scoped constraint", async () => {
  const host = hostModule({ ...hostConfig, constraints: [{
      scopeProps: ['id1'],
      expression: o => o.sum('value').map(x => x >= 0),
      failureMessage: 'fail'
    }] })

  await host.publish({ topic: 'transaction', id1: 1, value: 2 })
  await expect(host.publish({ topic: 'transaction', id1: 1, value: -3 })).rejects.toMatchObject({ message: 'fail' })

  await expect(host.publish({ topic: 'transaction', id1: 2, value: -1 })).rejects.toMatchObject({ message: 'fail' })
  await host.publish({ topic: 'transaction', id1: 1, value: -1 })
})

test("topic filter", async () => {
  const host = hostModule({ ...hostConfig, constraints: [{
      expression: o => o.map(x => false),
      topics: ['topic1', 'topic2'],
      failureMessage: 'fail'
    }] })

  await expect(host.publish({ topic: 'topic1' })).rejects.toMatchObject({ message: 'fail' })
  await expect(host.publish({ topic: 'topic2' })).rejects.toMatchObject({ message: 'fail' })
  await host.publish({ topic: 'topic3' })
})