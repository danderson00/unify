const join = require('../../src/operators/join')
const disconnectTarget = require('../../src/operators/disconnectTarget')
const expressions = require('@x/expressions')
const { stringify, parse } = require('@x/expressions/test/utils')

const delay = () => new Promise(resolve => setTimeout(resolve))

const setup = ({ action, expression, initialSourceValue }) => {
  const outerScopeSource = expressions.subject({ initialValue: { value: 'test' } })
  outerScopeSource.disconnect = jest.fn()
  const outerScopeSource2 = expressions.subject({ initialValue: { value: 'secondRequest' } })
  outerScopeSource2.disconnect = jest.fn()
  const subscriptions = { subscribe: jest.fn()
    .mockReturnValueOnce(Promise.resolve(outerScopeSource))
    .mockReturnValueOnce(Promise.resolve(outerScopeSource2))
  }
  const component = join(expressions, subscriptions).component

  const source = expressions.subject({ initialValue: initialSourceValue })
  const sourceDisconnectTarget = disconnectTarget(source)
  const joined = component(sourceDisconnectTarget)
    .create('outerScopeId', expression)

  return action({ subscriptions, source, joined, outerScopeSource, outerScopeSource2, sourceDisconnectTarget, component })
}

test("outer scope is requested when property value is set", () => {
  setup({ action: ({ subscriptions, source }) => {
    expect(subscriptions.subscribe.mock.calls.length).toBe(0)
    source.publish({ outerScopeId: 1 })
    expect(subscriptions.subscribe.mock.calls.length).toBe(1)
    expect(subscriptions.subscribe.mock.calls[0][1]).toEqual({ outerScopeId: 1 })
  } })
})

test("initial value from outer scope is set", async () => {
  await setup({ action: async ({ source, joined }) => {
    source.publish({ outerScopeId: 1 })
    await delay()
    expect(joined()).toEqual({ value: 'test' })
  } })
})

test("outer scope is requested from initial source value", async () => {
  await setup({
    initialSourceValue: { outerScopeId: 1 },
    action: async ({ source, joined }) => {
      await delay()
      expect(joined()).toEqual({ value: 'test' })
    }
  })
})

test("result observable is updated when scope updates", async () => {
  await setup({
    initialSourceValue: { outerScopeId: 1 },
    action: async ({ outerScopeSource, joined }) => {
      await delay()
      outerScopeSource.publish({ value: 'test2' })
      expect(joined()).toEqual({ value: 'test2' })
    }
  })
})

test("new outer scope is requested when outer scope changes", async () => {
  await setup({
    initialSourceValue: { outerScopeId: 1 },
    action: async ({ source, subscriptions }) => {
      await delay()
      source.publish({ outerScopeId: 2 })
      expect(subscriptions.subscribe.mock.calls.length).toBe(2)
    }
  })
})

test("old outer scope is disconnected when scope changes", async () => {
  await setup({
    initialSourceValue: { outerScopeId: 1 },
    action: async ({ source, outerScopeSource }) => {
      await delay()
      source.publish({ outerScopeId: 2 })
      expect(outerScopeSource.disconnect.mock.calls.length).toBe(1)
    }
  })
})

test("outer scope is disconnected when source is disconnected", async () => {
  await setup({
    initialSourceValue: { outerScopeId: 1 },
    action: async ({ sourceDisconnectTarget, outerScopeSource }) => {
      await delay()
      sourceDisconnectTarget.disconnect()
      expect(outerScopeSource.disconnect.mock.calls.length).toBe(1)
    }
  })
})

test("old outer scope is disconnected when new scope is requested immediately after it", async () => {
  await setup({
    initialSourceValue: { outerScopeId: 1 },
    action: async ({ source, outerScopeSource }) => {
      source.publish({ outerScopeId: 2 })
      await delay()
      expect(outerScopeSource.disconnect.mock.calls.length).toBe(1)
    }
  })
})

test("join can be serialized", async () => {
  await setup({
    action: async ({ source, outerScopeSource, outerScopeSource2, sourceDisconnectTarget, component, joined }) => {
      source.publish({ outerScopeId: 1 })
      outerScopeSource.publish({ value: 'test2' })

      await delay()

      const serializationInfo = joined.getSerializationInfo()
      const deserialized = component(sourceDisconnectTarget).unpack(parse(stringify(serializationInfo)))

      // this is the deserialized value
      expect(deserialized()).toEqual({ value: 'test2' })

      await delay()

      // initial value from observable defined in setup
      expect(deserialized()).toEqual({ value: 'secondRequest' })

      outerScopeSource2.publish({ value: 'test3' })
      expect(deserialized()).toEqual({ value: 'test3' })
    }
  })
})

test("join can be created from definition", async () => {
  await setup({
    action: async ({ source, sourceDisconnectTarget, joined, component, outerScopeSource2 }) => {
      const definition = joined.getSerializationInfo().definition
      const deserialized = component(sourceDisconnectTarget).unpack({ definition })
      source.publish({ outerScopeId: 1 })
      await delay()
      outerScopeSource2.publish({ value: 'test2' })
      expect(deserialized()).toEqual({ value: 'test2' })
    }
  })
})