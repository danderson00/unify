const disconnectTarget = require('../../src/operators/disconnectTarget')
const { subject, proxy } = require('@x/expressions')

test("disconnectTarget proxies parent until disconnected", () => {
  const source = subject()
  const target = disconnectTarget(source)
  const observable = proxy(target)
  const fn = jest.fn()
  observable.subscribe(fn)

  source.publish(1)
  expect(fn.mock.calls.length).toBe(1)

  target.disconnect()
  source.publish(1)
  expect(fn.mock.calls.length).toBe(1)
})

test("disconnectTarget calls disconnect on registered observables", () => {
  const source = subject()
  const target = disconnectTarget(source)
  const observable = proxy(target)
  observable.parent = target
  observable.disconnect = jest.fn()

  disconnectTarget.registerDisconnectObservable(observable)
  target.disconnect()

  expect(observable.disconnect.mock.calls.length).toBe(1)
})

test("disconnectTarget does not call disconnect on released observables", () => {
  const source = subject()
  const target = disconnectTarget(source)
  const observable = proxy(target)
  observable.parent = target
  observable.disconnect = jest.fn()

  disconnectTarget.registerDisconnectObservable(observable)
  disconnectTarget.releaseDisconnectObservable(observable)
  target.disconnect()

  expect(observable.disconnect.mock.calls.length).toBe(0)
})