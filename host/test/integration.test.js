const hostModule = require('..')
const expressions = require('@x/expressions')

test("messages are relayed", async () => {
  const host = hostModule({ log: { level: 'fatal' } })
  const definition = expressions.extractDefinition(o => o)
  const o1 = await host.subscribe({ type: 'raw', definition })
  const o2 = await host.subscribe({ type: 'raw', definition })
  await host.publish(1)
  expect(o1()).toBe(1)
  expect(o2()).toBe(1)
})

test("previously published messages are replayed", async () => {
  const host = hostModule({ log: { level: 'fatal' } })
  const definition = expressions.extractDefinition(o => o)
  await host.publish(1)
  const o = await host.subscribe({ type: 'raw', definition })
  expect(o()).toBe(1)
})

test("previously published scope messages are replayed", async () => {
  const host = hostModule({ scopes: ['id'], log: { level: 'fatal' } })
  const definition = expressions.extractDefinition(o => o)
  await host.publish({ id: 1, value: 1 })
  const o = await host.subscribe({ type: 'raw', definition }, { id: 1 })
  expect(o().value).toBe(1)
})

test("subsequently published scope messages are replayed", async () => {
  const host = hostModule({ scopes: ['id'], log: { level: 'fatal' } })
  const definition = expressions.extractDefinition(o => o)
  await host.publish({ id: 1, value: 1 })
  const o1 = await host.subscribe({ type: 'raw', definition }, { id: 1 })
  o1.disconnect()
  await host.publish({ id: 1, value: 2 })
  const o2 = await host.subscribe({ type: 'raw', definition }, { id: 1 })
  expect(o2().value).toBe(2)
})

test("simple expressions are evaluated", async () => {
  const host = hostModule({ log: { level: 'fatal' } })
  const definition = expressions.extractDefinition(o => o.map(x => x * 2))
  const o = await host.subscribe({ type: 'raw', definition })
  await host.publish(1)
  expect(o()).toBe(2)
})

test("complex expressions are evaluated", async () => {
  const host = hostModule({ log: { level: 'fatal' } })
  const definition = expressions.extractDefinition(o => o.groupBy('category').average('value'))
  const o = await host.subscribe({ type: 'raw', definition })
  await host.publish({ category: 1, value: 1 })
  await host.publish({ category: 2, value: 2 })
  await host.publish({ category: 3, value: 3 })
  expect(o()).toEqual(2)
  await host.publish({ category: 1, value: 4 })
  expect(o()).toEqual(3)
})

test("observables do not update after disconnecting", async () => {
  const host = hostModule({ log: { level: 'fatal' } })
  const definition = expressions.extractDefinition(o => o)
  const o1 = await host.subscribe({ type: 'raw', definition })
  const o2 = await host.subscribe({ type: 'raw', definition })
  await host.publish(1)
  await o1.disconnect() // not actually async, but will be over transport...
  await host.publish(2)
  expect(o1()).toBe(1)
  expect(o2()).toBe(2)
})

test("subscribe rejects when raw query is executed if allowRawQueries is false", async () => {
  const host = hostModule({ allowRawQueries: false, log: { level: 'fatal' } })
  const definition = expressions.extractDefinition(o => o.select('value'))
  expect(host.subscribe({ type: 'raw', definition })).rejects.toMatchObject({ message: 'Raw queries are not allowed on this host' })
})