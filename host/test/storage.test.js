const storage = require('../src/storage')
const hasher = require('../src/hasher')
const logger = require('../src/logger')

const configuration = { client: 'sqlite3', connection: { filename: ':memory:' } }
const log = logger('none')

test("messages attach autoIncrement timestamp", async () => {
  const { messages } = storage(configuration, [], hasher, log)
  await messages.store({ message: { id: 1 } })
  const stored = await messages.retrieve({})
  expect(stored).toEqual([
    { message: { id: 1 }, timestamp: 1 }
  ])
})

test("messages can be stored and retrieved by scope", async () => {
  const { messages } = storage(configuration, ['id'], hasher, log)
  await messages.store({ message: { id: 1, p: 1 } })
  await messages.store({ message: { id: 1, p: 2 } })
  await messages.store({ message: { id: 2, p: 3 } })
  const stored = await messages.retrieve({ id: 1 })
  expect(stored).toEqual([
    { message: { id: 1, p: 1 }, timestamp: 1 },
    { message: { id: 1, p: 2 }, timestamp: 2 }
  ])
})

test("messages can be retrieved by timestamp", async () => {
  const { messages } = storage(configuration, ['id'], hasher, log)
  await messages.store({ message: { id: 1 } })
  await messages.store({ message: { id: 2 } })
  await messages.store({ message: { id: 3 } })
  const stored = await messages.retrieve({}, 2)
  expect(stored).toEqual([
    { message: { id: 3 }, timestamp: 3 }
  ])
})

test("expressions can be stored and retrieved", async () => {
  const { expressions } = storage(configuration, ['id'], hasher, log)
  await expressions.store('definition', { id: 1 }, { p1: 1 })
  await expressions.store('definition', { id: 2 }, { p1: 3 })
  const stored = await expressions.retrieve('definition', { id: 1 })
  expect(stored).toMatchObject({
    definition: hasher('definition'),
    scope: `{"id":1}`,
    state: { p1: 1 }
  })
})