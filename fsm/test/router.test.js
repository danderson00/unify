const { Machine, router } = require('..')
const { subject } = require('@x/expressions')

const machine = Machine({
  initial: 'shop/Browse',
  on: {
    'home': 'shop/Browse',
    'checkout': 'checkout',
    'admin': [
      {
        target: 'admin',
        cond: ({ isAdmin }) => isAdmin === true
      },
      {
        target: 'adminFromMessage',
        cond: ({}, { message }) => message.isAdmin === true
      },
      { target: 'notAllowed' }
    ]
  },
  states: {
    'shop/Browse': {
      on: { 'productSelected': 'shop/Product' },
    },
    'shop/Product': {
      meta: { scope: ['productId'], props: ['p1', 'p2'] }
    },
    'checkout': {
      initial: 'checkout/ReviewItems',
        states: {
        'checkout/ReviewItems': {
          on: { 'itemsConfirmed': 'checkout/Shipping' }
        },
        'checkout/Shipping': {
          meta: { scope: ['orderId'] }
        }
      }
    },
    'admin': { },
    'adminFromMessage': { },
    'notAllowed': { },
  }
})

const setup = async (context = {}) => {
  const bus = subject()
  const onNavigate = jest.fn()
  router(bus).attach(machine, x => x, onNavigate, context)
  return { bus, onNavigate }
}

const verify = async (calls, count, lastComponent, lastScope = {}, additionalProps) => {
  await new Promise(r => setTimeout(r))
  expect(calls.length).toBe(count)
  expect(calls[calls.length - 1]).toEqual([{ component: lastComponent, props: { scope: lastScope, ...additionalProps } }])
}

test("attach navigates for initial state", async () => {
  const { onNavigate } = await setup()
  await verify(onNavigate.mock.calls, 1, ['shop/Browse'])
})

test("paths are followed", async () => {
  const { bus, onNavigate } = await setup()
  bus.publish({ topic: 'productSelected' })
  await verify(onNavigate.mock.calls, 2, ['shop/Product'])
})

test("scope is extracted from message if scope meta is set", async () => {
  const { bus, onNavigate } = await setup()
  bus.publish({ topic: 'productSelected', productId: 1 })
  await verify(onNavigate.mock.calls, 2, ['shop/Product'], { productId: 1 })
})

test("scope meta can handle unset scope properties", async () => {
  const { bus, onNavigate } = await setup()
  bus.publish({ topic: 'productSelected' })
  await verify(onNavigate.mock.calls, 2, ['shop/Product'], {})
})

test("props are extracted from message if props meta is set", async () => {
  const { bus, onNavigate } = await setup()
  bus.publish({ topic: 'productSelected', productId: 1, p1: 1, p2: 'test' })
  await verify(onNavigate.mock.calls, 2, ['shop/Product'], { productId: 1 }, { p1: 1, p2: 'test' })
})

test("global paths are followed", async () => {
  const { bus, onNavigate } = await setup()
  bus.publish({ topic: 'productSelected' })
  bus.publish({ topic: 'home' })
  await verify(onNavigate.mock.calls, 3, ['shop/Browse'])
})

test("initial state of child machine is set", async () => {
  const { bus, onNavigate } = await setup()
  bus.publish({ topic: 'checkout' })
  await verify(onNavigate.mock.calls, 2, ['checkout', 'checkout/ReviewItems'])
})

test("child machine paths are followed", async () => {
  const { bus, onNavigate } = await setup()
  bus.publish({ topic: 'checkout' })
  bus.publish({ topic: 'itemsConfirmed' })
  await verify(onNavigate.mock.calls, 3, ['checkout', 'checkout/Shipping'])
})

test("scope is set in child machines", async () => {
  const { bus, onNavigate } = await setup()
  bus.publish({ topic: 'checkout' })
  bus.publish({ topic: 'itemsConfirmed', orderId: 1 })
  await verify(onNavigate.mock.calls, 3, ['checkout', 'checkout/Shipping'], { orderId: 1 })
})

test("conditional navigation failure", async () => {
  const { bus, onNavigate } = await setup()
  bus.publish({ topic: 'admin' })
  await verify(onNavigate.mock.calls, 2, ['notAllowed'])
})

test("conditional navigation with context", async () => {
  const { bus, onNavigate } = await setup({ isAdmin: true })
  bus.publish({ topic: 'admin' })
  await verify(onNavigate.mock.calls, 2, ['admin'])
})

test("conditional navigation from message", async () => {
  const { bus, onNavigate } = await setup()
  bus.publish({ topic: 'admin', isAdmin: true })
  await verify(onNavigate.mock.calls, 2, ['adminFromMessage'])
})
