const machines = require('../../src/machines')
const expressions = require('@x/expressions')

const definitions = [
  {
    id: 'one',
    scopeProps: ['id'],
    machine: {
      initial: '1',
      states: {
        '1': { on: { 'to2': '2' } },
        '2': { on: { 'to1': '1' } }
      }
    }
  },
  {
    id: 'two',
    scopeProps: ['id', 'id2'],
    startTopic: 'start',
    contextProps: [''],
    machine: {
      initial: '1',
      strict: true,
      states: {
        '1': { on: { 'to2': '2' } },
        '2': { on: { 'to1': '1' } }
      }
    }
  }
]

test("publish returns changed or started services", () => {
  const api = machines(expressions, definitions)
  expect(api.publish({ topic: 'unknown' }).length).toBe(0)
  expect(api.publish({ id: 1 }).length).toBe(1)
  expect(api.publish({ id: 1, id2: 2 }).length).toBe(0)
  expect(api.publish({ id: 1, id2: 2, topic: 'start' }).length).toBe(1)
  expect(api.publish({ id: 1, id2: 2, topic: 'to2' }).length).toBe(2)
})

test("subscribe starts service for definitions without startTopic", () => {
  const api = machines(expressions, definitions)
  const value = api.subscribe('one', { id: 1 })
  expect(value()).toBe('1')
})

test("subscribe throws for unstarted definitions with startTopic", () => {
  const api = machines(expressions, definitions)
  expect(() => api.subscribe('two', { id: 1, id2: 2 })).toThrow()
})

test("subscribe throws for unknown definitions", () => {
  const api = machines(expressions, definitions)
  expect(() => api.subscribe('three', { id: 1, id2: 2 })).toThrow()
})

test("subscribe throws when scope doesn't match scopeProps", () => {
  const api = machines(expressions, definitions)
  expect(() => api.subscribe('one', {})).toThrow()
  expect(() => api.subscribe('two', { id: 1 })).toThrow()
  expect(() => api.subscribe('two', { id: 1, id2: 2, id3: 3 })).toThrow()
})

test("validatePublish returns empty array for valid messages", () => {
  const api = machines(expressions, definitions)
  api.publish({ id: 1, id2: 2, topic: 'start' })
  expect(api.validatePublish({ id: 1, id2: 2, topic: 'to2' })).toEqual({ valid: true, messages: [] })
})

test("validatePublish returns errors for messages that cause machines to throw", () => {
  const api = machines(expressions, definitions)
  api.publish({ id: 1, id2: 2, topic: 'start' })
  expect(api.validatePublish({ id: 1, id2: 2, topic: 'to3' }).valid).toBe(false)
  expect(api.validatePublish({ id: 1, id2: 2, topic: 'to3' }).messages.length).toBe(1)
})

test("validatePublish does not alter machine state", () => {
  const api = machines(expressions, definitions)
  api.publish({ id: 1, id2: 2, topic: 'start' })
  const value = api.subscribe('two', { id: 1, id2: 2 })
  expect(value()).toBe('1')
  api.validatePublish({ id: 1, id2: 2, topic: 'to2' })
  expect(value()).toBe('1')
})

test("validatePublish does not start new machines", () => {
  const api = machines(expressions, definitions)
  api.validatePublish({ id: 1, id2: 2, topic: 'start' })
  expect(() => api.subscribe('two', { id: 1, id2: 2 })).toThrow()
})
