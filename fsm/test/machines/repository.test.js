const repository = require('../../src/machines/repository')
const expressions = require('@x/expressions')

const definitions = [
  {
    id: 'one',
    scopeProps: ['id'],
    machine: {
      initial: '1',
      states: {
        '1': { on: { 'to2': '2' } },
        '2': { on: { 'to1': '1' } }
      }
    }
  },
  {
    id: 'two',
    scopeProps: ['id', 'id2'],
    startTopic: 'start',
    contextProps: [''],
    machine: {
      initial: '1',
      states: {
        '1': { on: { 'to2': '2' } },
        '2': { on: { 'to1': '1' } }
      }
    }
  }
]

test("relatedDefinitions returns appropriate definitions", () => {
  const container = repository(expressions, definitions)
  expect(container.relatedDefinitions({ id: 1 })).toMatchObject([{ id: 'one' }])
  expect(container.relatedDefinitions({ id: 1, id2: 2 })).toMatchObject([{ id: 'one' }, { id: 'two' }])
})

test("createService returns started service", () => {
  const container = repository(expressions, definitions)
  const service = container.createService(container.definitionFor('one'), { id: 1 })
  service.send({ type: 'to2' })
  expect(service.state.value).toBe('2')
})

test("getService returns previously created service", () => {
  const container = repository(expressions, definitions)
  const service = container.createService(container.definitionFor('one'), { id: 1 })
  expect(container.getService('one', { id: 1 })).toBe(service)
})

test("services are started for each initial state", () => {
  const container1 = repository(expressions, definitions)
  const service = container1.createService(container1.definitionFor('two'), { id: 1 })
  service.send({ type: 'to2' })

  const container2 = repository(expressions, definitions, [
    { id: 'two', scope: { id: 1 }, state: service.state, context: {} }
  ])
  const service2 = container2.getService('two', { id: 1 })
  expect(service2.state.value).toBe('2')
  service2.send({ type: 'to1' })
  expect(service2.state.value).toBe('1')
})