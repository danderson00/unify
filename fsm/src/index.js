const { Machine } = require('xstate')
const router = require('./router')

module.exports = { Machine, router }
