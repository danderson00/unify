const { interpret } = require('xstate')

module.exports = (localBus) => {
  return {
    attach: (machine, resolveComponent, onNavigate, context) => {
      const machineWithContext = machine.withContext(context)
      machineWithContext.id = machineWithContext.id === '(machine)' ? 'navigation' : machineWithContext.id
      const service = interpret(machineWithContext, { devTools: process.env.NODE_ENV === 'development' }).start()

      localBus.subscribe(message => service.send({ message, type: message.topic }))

      service.onTransition(machineState => {
        if (machineState.changed !== false && !machineState.done) {
          const path = getPath(machineState)
          const props = extractMeta('props', machineState)
          const scope = extractMeta('scope', machineState)

          Promise.resolve(resolveComponent(path))
            .then(component => {

              onNavigate({
                component,
                props: { scope, ...props }
              })
            })
        }
      })
    }
  }
}

const getPath = machineState => typeof machineState.value === 'object'
  // this obviously won't work for nesting beyond 2...
  ? [Object.keys(machineState.value)[0], Object.values(machineState.value)[0]]
  : [machineState.value]

const extractMeta = (property, machineState) => (
  Object.values(machineState.meta || {}).reduce(
    (allValues, nextValue) => ({
      ...allValues,
      ...getPropertiesFromMessage(nextValue[property], machineState.event.message)
    }),
    {}
  )
)

const getPropertiesFromMessage = (props, event) => (props || []).reduce(
  (values, prop) => ({
    ...values,
    ...(event[prop] !== undefined && { [prop]: event[prop] })
  }),
  {}
)
