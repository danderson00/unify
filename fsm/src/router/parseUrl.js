module.exports = machine => url => url.split('/').reduce(
  (state, pathSegment) => {
    const currentNode = state[state.length - 1]

    if(currentNode.expects.length === 0) {
      const fullConfigPath = [...currentNode.previousPathSegments, pathSegment].join('/')
      if (state.config.states[fullConfigPath]) {
        // woot, we found a node - find the required scope properties and "expect" them
      } else {
        // otherwise keep looking for the required node
        return [
          ...state.slice(0, state.length - 2),
          { ...currentNode, previousPathSegments: [...currentNode.previousPathSegments, pathSegment] }
        ]
      }
    } else {
      
    }
  },
  [{ config: machine.config, previousPathSegments: [], expects: [] }]
)