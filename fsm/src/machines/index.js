const repository = require('./repository')
const { deepEqual } = require('@x/expressions/src/utilities')

// const definition = {
//   // required
//   id: '',
//   machine: {},
//
//   // optional
//   scopeProps: [],
//   contextProps: [], // requires startTopic
//   startTopic: ''
//
//   // private
//   compiled: Machine()
// }


module.exports = (expressions, definitions = [], initialStates = [], log) => {
  const container = repository(expressions, definitions, initialStates, log)

  const extractProps = (props, message) => (props || []).reduce(
    (result, prop) => ({ ...result, [prop]: message[prop] }),
    {}
  )

  const send = (service, message) => {
    if(!message.topic) {
      return false
    }
    const previousValue = service.state.value
    service.send({ ...message, type: message.topic })
    return !deepEqual(service.state.value, previousValue)
  }

  return {
    publish: message => container.relatedDefinitions(message).reduce(
      (changed, definition) => {
        const scope = extractProps(definition.scopeProps, message)
        const service = container.getService(definition.id, scope)
        const serialize = ({ state, machine }) => ({ id: definition.id, scope, state, context: machine.context })

        if(service) {
          if(send(service, message)) {
            return [...changed, serialize(service)]
          } else {
            return changed
          }
        } else {
          if(!definition.startTopic) {
            return [...changed, serialize(container.createService(definition, scope))]
          } else if(message.topic === definition.startTopic) {
            return [...changed, serialize(container.createService(definition, scope, extractProps(definition.contextProps, message)))]
          } else {
            return changed
          }
        }
      },
      []
    ),
    validatePublish: message => {
      const errors = container.relatedDefinitions(message).reduce(
        (errors, definition) => {
          try {
            const scope = extractProps(definition.scopeProps, message)
            const service = container.getService(definition.id, scope)

            if(service) {
              if(message.topic) {
                service.nextState({ ...message, type: message.topic })
              }
            } else {
              if(!definition.startTopic) {
                container.createService(definition, scope, undefined, undefined, true)
              } else if(message.topic === definition.startTopic) {
                container.createService(definition, scope, extractProps(definition.contextProps, message), undefined, true)
              }
            }
          } catch(error) {
            return [...errors, error]
          }
          return errors
        },
        []
      )
      return {
        valid: errors.length === 0,
        messages: errors
      }
    },
    subscribe: (id, scope) => {
      const service = container.getService(id, scope)
      if(service) {
        return expressions.proxy(service.observable)

      } else {
        const definition = container.definitionFor(id)
        if(!definition) {
          throw new Error(`No machine definition exists for id ${id}`)
        }
        if(!deepEqual(definition.scopeProps, Object.keys(scope))) {
          throw new Error(`Machine scopeProps ${definition.scopeProps} do not match scope ${JSON.stringify(scope)}`)
        }
        if(definition.startTopic) {
          throw new Error(`No machine id ${id} started for scope ${JSON.stringify(scope)}`)
        } else {

          // we can safely start this machine without persisting the state - it will simply restart in the same state again
          return expressions.proxy(container.createService(definition, scope).observable)
        }
      }
    },
  }
}