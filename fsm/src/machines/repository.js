const { Machine, interpret } = require('xstate')

module.exports = (expressions, definitions, initialStates = [], log) => {
  definitions = definitions.map(definition => ({
    scopeProps: [],
    contextProps: [],
    ...definition,
    compiled: Machine(definition.machine)
  }))

  const scopeKey = scope => JSON.stringify(scope) // TODO: sort object

  const definitionFor = id => definitions.find(x => x.id === id)

  const createService = (definition, context, state) => {
    const service = interpret(definition.compiled.withContext(context)).start(state)
    service.observable = expressions.subject({ initialValue: service.state.value })
    service.onTransition(state => service.observable.publish(state.value))
    return service
  }


  const machines = initialStates.reduce(
    (ids, state) => {
      const definition = definitionFor(state.id)
      if(definition) {
        ids[state.id] = ids[state.id] || {}
        ids[state.id][scopeKey(state.scope)] = createService(definition, state.context, state.state)
      } else {
        log.warn(`Machine state found for ${state.id} but no matching definition registered`)
      }
      return ids
    },
    {}
  )

  return {
    relatedDefinitions: message => definitions.filter(definition =>
      definition.scopeProps.every(prop => Object.keys(message).includes(prop))
    ),
    createService: (definition, scope, context, state, temporary) => {
      const service = createService(definition, { ...context, ...scope }, state)
      if(temporary) {
        return service
      } else {
        machines[definition.id] = machines[definition.id] || {}
        return machines[definition.id][scopeKey(scope)] = service
      }
    },
    getService: (id, scope) => machines[id] && machines[id][scopeKey(scope)],
    definitionFor
  }
}