module.exports = {
  toXstate: config => {

  }
}

const expressions = {
  initial: 'shop/Browse',
  on: {
    'home': 'shop/Browse',
    'checkout': 'checkout',
    'admin': {
      'admin': { if: ({ isAdmin }) => isAdmin },
      'adminFromMessage': { if: ({}, { isAdmin }) => isAdmin },
      'notAllowed': {}
    }
  },
  states: {
    'shop/Browse': {
      on: { 'productSelected': 'shop/Product' },
    },
    'shop/Product': {
      scope: ['productId'], // or just 'productId'
      props: ['p1', 'p2']
    },
    'checkout': {
      initial: 'checkout/ReviewItems',
      states: {
        'checkout/ReviewItems': {
          on: { 'itemsConfirmed': 'checkout/Shipping' }
        },
        'checkout/Shipping': {
          scope: ['orderId']
        }
      }
    },
    'admin': { },
    'adminFromMessage': { },
    'notAllowed': { },
  }
}

const xstate = Machine({
  initial: 'shop/Browse',
  on: {
    'home': 'shop/Browse',
    'checkout': 'checkout',
    'admin': [
      {
        target: 'admin',
        cond: ({ isAdmin }) => isAdmin === true
      },
      {
        target: 'adminFromMessage',
        cond: ({}, { message }) => message.isAdmin === true
      },
      { target: 'notAllowed' }
    ]
  },
  states: {
    'shop/Browse': {
      on: { 'productSelected': 'shop/Product' },
    },
    'shop/Product': {
      meta: { scope: ['productId'], props: ['p1', 'p2'] }
    },
    'checkout': {
      initial: 'checkout/ReviewItems',
      states: {
        'checkout/ReviewItems': {
          on: { 'itemsConfirmed': 'checkout/Shipping' }
        },
        'checkout/Shipping': {
          meta: { scope: ['orderId'] }
        }
      }
    },
    'admin': { },
    'adminFromMessage': { },
    'notAllowed': { },
  }
})