const React = require('react')
const enzyme = require('enzyme')
const { Provider, connect } = require('../src')
const hostModule = require('@x/unify.host')
const { construct } = require('@x/expressions')

test("simple message count", async () => {
  const host = hostModule({ log: { level: 'error' } })
  const Child = ({ votes }) => <div>{votes}</div>
  const Connected = connect({ votes: o => o.topic('vote').count() })(Child)
  await host.publish({ topic: 'vote' })
  const wrapper = enzyme.mount(<Provider host={host}><Connected /></Provider>)
  await host.publish({ topic: 'vote' })
  await new Promise(r => setTimeout(r, 20))
  expect(wrapper.find('div').text()).toBe('2')
})

test("multiple model properties", async () => {
  const host = hostModule({ log: { level: 'error' } })
  const Child = ({ details, rating }) => <div>{`${details.id}${details.name}${rating}`}</div>
  const Connected = connect({ 
    details: o => o.topic('details').accumulate(),
    rating: o => o.topic('rating').average('value')
  })(Child)
  await host.publish({ topic: 'details', id: 1, name: 'test' })
  await host.publish({ topic: 'rating', value: 2 })
  await host.publish({ topic: 'rating', value: 4 })
  const wrapper = enzyme.mount(<Provider host={host}><Connected /></Provider>)

  await new Promise(r => setTimeout(r, 20))
  expect(wrapper.find('div').text()).toBe('1test3')

  await host.publish({ topic: 'details', name: 'test2' })
  await host.publish({ topic: 'rating', value: 6 })

  await new Promise(r => setTimeout(r, 20))
  expect(wrapper.find('div').text()).toBe('1test24')
})

test("multiple vocabulary properties", async () => {
  const expressions = construct()

  const vocabulary = {
    total: o => o.sum(),
    avg: o => o.average()
  }
  const host = hostModule({ log: { level: 'error' }, expressions, vocabulary })
  const Child = ({ total, avg }) => <div>{`${total}-${avg}`}</div>
  const Connected = connect('total', 'avg')(Child)
  await host.publish(1)
  await host.publish(3)
  const wrapper = enzyme.mount(<Provider host={host} expressions={expressions}><Connected /></Provider>)

  await new Promise(r => setTimeout(r, 50))
  expect(wrapper.find('div').text()).toBe('4-2')

  await host.publish(5)

  await new Promise(r => setTimeout(r, 20))
  expect(wrapper.find('div').text()).toBe('9-3')
})

test("vocabulary with parameters", async () => {
  const expressions = construct()

  const vocabulary = {
    concat: (o, s1, s2) => o.map(x => s1 + s2 + x)
  }
  const host = hostModule({ log: { level: 'error' }, expressions, vocabulary })
  const Child = ({ concat }) => <div>{`${concat}`}</div>
  const Connected = connect({ concat: ['ab', 'cd']})(Child)
  await host.publish('ef')
  const wrapper = enzyme.mount(<Provider host={host} expressions={expressions}><Connected /></Provider>)

  await new Promise(r => setTimeout(r, 50))
  expect(wrapper.find('div').text()).toBe('abcdef')

  await host.publish('gh')

  await new Promise(r => setTimeout(r, 20))
  expect(wrapper.find('div').text()).toBe('abcdgh')
})

test("simple groupBy", async () => {
  const host = hostModule({ log: { level: 'error' } })
  const Child = ({ ratings }) => (
    <ul>
      {ratings.map((x, i) => <li key={i}>{x}</li>)}
    </ul>
  )
  const Connected = connect({ 
    ratings: o => o.groupBy('userId', o => o.select('value'))
  })(Child)

  await host.publish({ userId: 1, value: 1 })
  await host.publish({ userId: 2, value: 3 })
  await host.publish({ userId: 3, value: 2 })

  const wrapper = enzyme.mount(<Provider host={host}><Connected /></Provider>)

  await new Promise(r => setTimeout(r, 20))
  expect(wrapper.find(Child).find('ul').text()).toBe('132')

  await host.publish({ userId: 2, value: 4 })

  await new Promise(r => setTimeout(r, 20))
  expect(wrapper.find(Child).find('ul').text()).toBe('142')
})

// issue with child expression not being executed...
test.skip("join", async () => {
  const expressions = construct()
  const host = hostModule({ log: { level: 'error' }, scopes: ['id1', 'id2'], expressions })
  const Child = ({ sumChildren }) => (
    <span>{sumChildren}</span>
  )
  const Connected = connect({
    sumChildren: o => o.topic('parent').join('id2', o => o.topic('child').sum('value'))
  })(Child)

  await host.publish({ topic: 'parent', id1: 1, id2: 1 })
  await host.publish({ topic: 'child', id2: 1, value: 1 })
  await host.publish({ topic: 'child', id2: 1, value: 2 })

  const wrapper = enzyme.mount(<Provider host={host} expressions={expressions}><Connected /></Provider>)

  await new Promise(r => setTimeout(r, 50))
  expect(wrapper.find(Child).find('span').text()).toBe('3')

  await host.publish({ topic: 'child', id2: 1, value: 3 })

  await new Promise(r => setTimeout(r, 50))
  expect(wrapper.find(Child).find('span').text()).toBe('6')
})

test("changing scope updates component", async () => {
  const host = hostModule({ log: { level: 'error' }, scopes: ['id1'] })
  const Child = ({ sum }) => (
    <span>{sum}</span>
  )
  const Connected = connect({
    sum: o => o.sum('value')
  })(Child)

  await host.publish({ id1: 1, value: 1 })
  await host.publish({ id1: 1, value: 2 })
  await host.publish({ id1: 2, value: 4 })

  const wrapper = enzyme.mount(<Provider host={host}><Connected scope={{ id1: 1 }} /></Provider>)

  await new Promise(r => setTimeout(r, 50))
  expect(wrapper.find(Child).find('span').text()).toBe('3')

  wrapper.setProps({ children: <Connected scope={{ id1: 2 }} /> })

  await new Promise(r => setTimeout(r, 50))
  expect(wrapper.find(Child).find('span').text()).toBe('4')
})

test("expression errors are logged on the consumer", async () => {

})