const React = require('react')
const enzyme = require('enzyme')
const { act } = require('react-dom/test-utils')
const { Provider, Navigator, Machine, publisher } = require('../src')
const hostModule = require('@x/unify.host')

const machine = Machine({
  initial: 'Browse',
  on: {
    'click': [
      {
        target: 'Clicked',
        cond: ({ modelObservable }) => modelObservable().clickCount > 1
      }
    ]
  },
  states: {
    'Browse': {
      on: { 'productSelected': 'Product' },
    },
    'Product': {
      meta: { scope: ['productId'] }
    },
    'Clicked': {}
  }
})

const components = {
  Browse: publisher(({ publish }) =>
    <div>
      Browse
      <button onClick={() => publish('productSelected', { productId: 2 })} />
      <a onClick={() => publish('click')}></a>
    </div>
  ),
  Product: ({ scope }) => <div>Product {scope.productId}</div>,
  Clicked: () => 'Clicked'
}
const resolveComponent = path => components[path]

const setup = async connect => {
  const host = hostModule({ log: { level: 'error' } })
  let wrapper

  const execute = async action => {
    await act(async () => {
      action()
      await new Promise(r => setTimeout(r, 50))
      wrapper.update()
    })
  }

  await execute(() =>
    wrapper = enzyme.mount(
      <Provider host={host}>
        <Navigator machine={machine} resolveComponent={resolveComponent} connect={connect} />
      </Provider>
    )
  )

  const verify = text => expect(wrapper.find(Navigator).text()).toBe(text)
  const publish = message => execute(() => host.publish(message))

  return { publish: publish, verify, wrapper, execute }
}

test("simple navigation", async () => {
  const { wrapper, execute, verify } = await setup()
  await execute(() => wrapper.find('button').simulate('click'))
  verify('Product 2')
})

test("expressions can be connected to context", async () => {
  const { wrapper, execute, verify } = await setup({ clickCount: o => o.topic('click').count() })
  await execute(() => wrapper.find('a').simulate('click'))
  verify('Browse')
  await execute(() => wrapper.find('a').simulate('click'))
  verify('Clicked')
})