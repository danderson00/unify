const { userModel, defaultModel } = require('../src/mapModel')
const expressions = require('@x/expressions')

const expression = o => o

describe("userModel", () => {
  test("handles single vocabulary names", () => {
    expect(userModel(['v'])).toEqual({
      v: { type: 'vocabulary', name: 'v', parameters: undefined }
    })
  })

  test("handles single expressions", () => {
    expect(userModel([{ e: expression }])).toEqual({
      e: { type: 'raw', expression }
    })
  })

  test("handles single vocabulary names with parameters", () => {
    expect(userModel([{ v: ['1', 2] }])).toEqual({
      v: { type: 'vocabulary', name: 'v', parameters: ['1', 2] }
    })
  })

  test("remaps vocabulary names", () => {
    expect(userModel([{ v: 'v2' }])).toEqual({
      v: { type: 'vocabulary', name: 'v2', parameters: undefined }
    })
  })

  test("handles multiple parameters", () => {
    expect(userModel([
      'v1',
      { v2: expression, v3: 'v32' },
      { v4: ['1', 2], v5: expression }
    ])).toEqual({
      v1: { type: 'vocabulary', name: 'v1', parameters: undefined },
      v2: { type: 'raw', expression },
      v3: { type: 'vocabulary', name: 'v32', parameters: undefined },
      v4: { type: 'vocabulary', name: 'v4', parameters: ['1', 2] },
      v5: { type: 'raw', expression }
    })
  })
})

describe("defaultModel", () => {
  test("expressions use default value of expression", () => {
    expect(
      defaultModel({
        sum: { type: 'raw', expression: o => o.sum() },
        groups: { type: 'raw', expression: o => o.groupBy('category') }
      }, expressions)
    ).toEqual({
      sum: 0,
      groups: []
    })
  })

  test("vocabulary uses default value of call to each component", () => {
    expect(
      defaultModel({
        sum: { type: 'vocabulary', name: 'sum' },
        groupBy: { type: 'vocabulary', name: 'groupBy' }
      }, expressions)
    ).toEqual({
      sum: 0,
      groupBy: []
    })
  })
})
