const enzyme = require('enzyme')
const Adapter = require('@wojtekmaj/enzyme-adapter-react-17')
require('babel-polyfill')

enzyme.configure({ adapter: new Adapter() })

