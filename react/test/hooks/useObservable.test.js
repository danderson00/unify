const React = require('react')
const { act } = require('react-dom/test-utils')
const { mount } = require('enzyme')
const { subject } = require('@x/expressions')
const { useObservable } = require('../../src')

const setup = values => {
  const observable = values
    ? values.map(initialValue => subject({ initialValue }))
    : subject({ initialValue: 'test' })
  return {
    observable,
    Consumer: () => {
      const value = useObservable(observable)
      return <div>{value}</div>
    }
  }
}

test("useObservable renders initial value", () => {
  const { Consumer } = setup()
  const wrapper = mount(<Consumer />)
  expect(wrapper.find(Consumer).text()).toBe('test')
})

test("useObservable rerenders on observable pulse", () => {
  const { observable, Consumer } = setup()
  const wrapper = mount(<Consumer />)
  act(() => observable.publish('changed'))
  expect(wrapper.find(Consumer).text()).toBe('changed')
})

test("useObservable unsubscribes from observable on unmount", () => {
  const { observable, Consumer } = setup()
  const wrapper = mount(<Consumer />)
  const instance = wrapper.find(Consumer)
  wrapper.unmount()
  act(() => observable.publish('changed'))
  expect(instance.text()).toBe('test')
  // it's difficult to test if the subscription has actually been triggered
})

test("useObservable renders multiple initial values", () => {
  const { Consumer } = setup(['test1', 'test2'])
  const wrapper = mount(<Consumer />)
  expect(wrapper.find(Consumer).text()).toBe('test1test2')
})

test("useObservable rerenders multiple observables on pulse", () => {
  const { observable, Consumer } = setup(['test1', 'test2'])
  const wrapper = mount(<Consumer />)
  act(() => observable[0].publish('changed1'))
  expect(wrapper.find(Consumer).text()).toBe('changed1test2')
  act(() => observable[1].publish('changed2'))
  expect(wrapper.find(Consumer).text()).toBe('changed1changed2')
})

test("useObservable unsubscribes from observable on unmount", () => {
  const { observable, Consumer } = setup(['test1', 'test2'])
  const wrapper = mount(<Consumer />)
  const instance = wrapper.find(Consumer)
  wrapper.unmount()
  act(() => observable[0].publish('changed1'))
  act(() => observable[1].publish('changed2'))
  expect(instance.text()).toBe('test1test2')
})
