import { useState } from 'react'

const React = require('react')
const { act } = require('react-dom/test-utils')
const { mount } = require('enzyme')
const { Provider, useEvaluate } = require('../../src')
const hostModule = require('@x/unify.host')
const { construct } = require('@x/expressions')

const delay = ms => act(() => new Promise(r => setTimeout(r, ms)))

test("useEvaluate returns evaluated expression", async () => {
  const host = hostModule({ log: { level: 'error' } })

  const Evaluator = () => {
    const [sum] = useEvaluate(o => o.where(x => x.categoryId === categoryId).sum('value'), {}, [{ categoryId: 1 }])
    return <div>{sum}</div>
  }
  const wrapper = mount(<Provider host={host}><Evaluator /></Provider>).find('div')
  const publish = message => act(() => host.publish(message))

  expect(wrapper.text()).toBe('0')
  await publish({ categoryId: 1, value: 2 })
  expect(wrapper.text()).toBe('2')
  await publish({ categoryId: 1, value: 3 })
  expect(wrapper.text()).toBe('5')
  await publish({ categoryId: 2, value: 1 })
  expect(wrapper.text()).toBe('5')
  await publish({ categoryId: 1, value: 3 })
  expect(wrapper.text()).toBe('8')
})

test("evaluate returns an observable for the specified vocabulary name and parameters", async () => {
  const expressions = construct()
  const vocabulary = {
    categoryValue: (o, categoryId) => o.where(x => x.categoryId === categoryId).sum('value')
  }
  const host = hostModule({ log: { level: 'error' }, expressions, vocabulary })

  const Evaluator = () => {
    const [sum] = useEvaluate('categoryValue', {}, [1])
    return <div>{sum}</div>
  }

  const wrapper = mount(<Provider host={host} expressions={expressions}><Evaluator /></Provider>).find('div')
  const publish = message => act(() => host.publish(message))

  expect(wrapper.text()).toBe('0')
  await publish({ categoryId: 1, value: 2 })
  expect(wrapper.text()).toBe('2')
  await publish({ categoryId: 1, value: 3 })
  expect(wrapper.text()).toBe('5')
  await publish({ categoryId: 2, value: 1 })
  expect(wrapper.text()).toBe('5')
  await publish({ categoryId: 1, value: 3 })
  expect(wrapper.text()).toBe('8')
})

test("evaluate arguments can be updated using returned function", async () => {
  const host = hostModule({ log: { level: 'error' } })

  const Evaluator = () => {
    const [sum, setCategoryId] = useEvaluate(o => o.where(x => x.categoryId === categoryId).sum('value'), {}, [{ categoryId: 1 }])
    return <div onClick={() => setCategoryId({ categoryId: 2 })}>{sum}</div>
  }
  const wrapper = mount(<Provider host={host}><Evaluator /></Provider>).find('div')
  const publish = message => act(() => host.publish(message))

  await publish({ categoryId: 1, value: 2 })
  await publish({ categoryId: 1, value: 3 })
  await publish({ categoryId: 2, value: 1 })
  await publish({ categoryId: 1, value: 3 })
  expect(wrapper.text()).toBe('8')

  wrapper.simulate('click')
  await delay(20)
  expect(wrapper.text()).toBe('1')
})

test("previous evaluations are disconnected", async () => {
  const host = hostModule({ log: { level: 'error' } })

  let currentPromise = undefined
  let currentCategoryId = 0
  const Evaluator = () => {
    const [sum, setCategoryId] = useEvaluate(o => o.where(x => x.categoryId === categoryId).sum('value'))
    return <div onClick={() => currentPromise = setCategoryId({ categoryId: ++currentCategoryId })}>{sum}</div>
  }
  const wrapper = mount(<Provider host={host}><Evaluator /></Provider>).find('div')
  const publish = message => act(() => host.publish(message))

  await publish({ categoryId: 1, value: 2 })
  await publish({ categoryId: 1, value: 3 })
  await publish({ categoryId: 2, value: 1 })

  wrapper.simulate('click')
  await delay(20)
  const o1 = await currentPromise
  expect(o1()).toBe(5)

  wrapper.simulate('click')
  await delay(20)
  const o2 = await currentPromise
  expect(o2()).toBe(1)

  await publish({ categoryId: 1, value: 3 })
  await delay(20)
  expect(o1()).toBe(5)
})

test("evaluating causes a single rerender", async () => {
  let renderCount = 0
  const host = hostModule({ log: { level: 'error' } })

  const Evaluator = () => {
    renderCount++
    const [sum] = useEvaluate(o => o.where(x => x.categoryId === categoryId).sum('value'), {}, [{ categoryId: 1 }])
    return <div>{sum}</div>
  }
  mount(<Provider host={host}><Evaluator /></Provider>).find('div')
  const publish = message => act(() => host.publish(message))

  await delay(50)

  expect(renderCount).toBe(3)
  await publish({ categoryId: 1, value: 2 })
  expect(renderCount).toBe(4)
})

test("initialArgs can be set dynamically", async () => {
  const host = hostModule({ log: { level: 'error' } })

  const Evaluator = () => {
    const [categoryId, setCategoryId] = useState(1)
    const [sum] = useEvaluate(o => o.where(x => x.categoryId === categoryId).sum('value'), {}, [{ categoryId }])
    return <>
      <div>{sum}</div>
      <button onClick={() => setCategoryId(2)}></button>
    </>
  }
  const wrapper = mount(<Provider host={host}><Evaluator categoryId={1} /></Provider>)
  const text = wrapper.find('div')
  const publish = message => act(() => host.publish(message))

  expect(text.text()).toBe('0')
  await publish({ categoryId: 1, value: 2 })
  expect(text.text()).toBe('2')

  act(() => {
    wrapper.find('button').simulate('click')
    wrapper.update()
  })

  await delay(50)

  expect(text.text()).toBe('0')
  await publish({ categoryId: 2, value: 3 })
  expect(text.text()).toBe('3')
})

// not implemented, but not sure this should be the default behavior anyway
// test("evaluate merges ambient scope", async () => {})