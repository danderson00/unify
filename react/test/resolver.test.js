const resolver = require('../src/resolver').default
const expressions = require('@x/expressions')
const { subject, unwrap } = expressions

let source, host

beforeEach(() => {
  source = subject()
  host = { subscribe: jest.fn(() => Promise.resolve(source)) }
})

test("resolver returns a default value for models", () => {
  const o = resolver(expressions, host)({}, [{
    sum: o => o.sum(),
    groups: o => o.groupBy()
  }])
  expect(unwrap(o)).toEqual({
    sum: 0,
    groups: []
  })
})

test("resolver returns a default value for vocabulary", () => {
  const o = resolver(expressions, host)({}, ['sum', 'groupBy'])
  expect(unwrap(o)).toEqual({
    sum: 0,
    groupBy: []
  })
})

test("resolver requests raw query for models", () => {
  resolver(expressions, host)({}, [{ sum: o => o.sum() }])
  expect(host.subscribe.mock.calls.length).toBe(1)
  expect(host.subscribe.mock.calls[0][0].type).toBe('raw')
})

test("resolver requests vocabulary query for vocabulary", () => {
  resolver(expressions, host)({}, ['sum'])
  expect(host.subscribe.mock.calls).toEqual([[{ type: 'vocabulary', name: 'sum' }, {}]])
})

test("resolver makes multiple requests for multiple vocabulary names", () => {
  resolver(expressions, host)({}, ['sum', 'count'])
  expect(host.subscribe.mock.calls).toEqual([
    [{ type: 'vocabulary', name: 'sum' }, {}],
    [{ type: 'vocabulary', name: 'count' }, {}]
  ])
})

test("returned observable updates when source is updated", async () => {
  const o = resolver(expressions, host)({}, ['sum'])
  await new Promise(r => setTimeout(r))
  source.publish(2)
  expect(o()).toEqual({ sum: 2 })
})

test("resolver adds synced property to returned observable", async () => {
  const o = resolver(expressions, host)({}, [{ o1: o => o, o2: o => o }])
  expect(o.synced).toBe(false)
  await new Promise(setTimeout)
  expect(o.synced).toBe(true)
})