const React = require('react')
const enzyme = require('enzyme')
const expressions = require('@x/expressions')
const { Provider, Navigator, Machine } = require('../../src')
const { act } = require('react-dom/test-utils')

const machine = Machine({
  initial: 'Browse',
  on: {
    'home': 'Browse'
  },
  states: {
    'Browse': {
      on: { 'productSelected': 'Product' },
    },
    'Product': {
      meta: { scope: ['productId'] }
    }
  }
})

const components = {
  Browse: () => <div>Browse</div>,
  Product: ({ scope }) => <div>Product {scope.productId}</div>
}
const resolveComponent = path => components[path]

const setup = async () => {
  const bus = expressions.subject()
  let wrapper

  const execute = async action => {
    await act(async () => {
      action()
      await new Promise(r => setTimeout(r))
      wrapper.update()
    })
  }

  await execute(() =>
    wrapper = enzyme.mount(<Provider host={{}} localBus={bus}><Navigator machine={machine} resolveComponent={resolveComponent} /></Provider>)
  )

  const verify = text => expect(wrapper.find(Navigator).text()).toBe(text)
  const publish = message => execute(() => bus.publish(message))

  return { publish, verify, wrapper }
}


test("initial route is set", async () => {
  const { verify } = await setup()
  verify('Browse')
})

test("paths are followed", async () => {
  const { publish, verify } = await setup()
  await publish({ topic: 'productSelected' })
  verify('Product ')
})

test("scope is set", async () => {
  const { publish, verify } = await setup()
  await publish({ topic: 'productSelected', productId: 1 })
  verify('Product 1')
})

test("children are wrapped in scoped HoC", async () => {
  const { publish, wrapper } = await setup()
  await publish({ topic: 'productSelected', productId: 1 })
  // this uses the fact that the scoped HoC spreads the scope properties on to the component props
  expect(wrapper.find(components.Product).props().productId).toBe(1)
})