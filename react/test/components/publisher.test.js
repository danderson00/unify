const React = require('react')
const enzyme = require('enzyme')
const { Provider, publisher } = require('../../src')

test("publisher supplies publish prop to component", () => {
  const host = { publish: jest.fn(() => Promise.resolve()) }
  const Child = () => <div></div>
  const Publisher = publisher(Child)
  const wrapper = enzyme.mount(<Provider host={host}><Publisher /></Provider>)
  const publish = wrapper.find(Child).props().publish
  publish({ value: 'test' })
  expect(host.publish.mock.calls).toEqual([[{ value: 'test' }]])
})

test("publisher attaches topic first argument to published messages", () => {
  const host = { publish: jest.fn(() => Promise.resolve()) }
  const Child = () => <div></div>
  const Publisher = publisher(Child)
  const wrapper = enzyme.mount(<Provider host={host}><Publisher /></Provider>)
  const publish = wrapper.find(Child).props().publish
  publish('test', { value: 'test' })
  expect(host.publish.mock.calls).toEqual([[{ value: 'test', topic: 'test' }]])
})

test("publisher cascades scope to child components", () => {
  const Child = () => <div></div>
  const PublisherChild = publisher(Child)
  const Parent = () => <PublisherChild scope={{ p1: 1 }} />
  const PublisherParent = publisher(Parent)
  const wrapper = enzyme.mount(
    <Provider resolve={() => model} publish={() => Promise.resolve()} host={{}}>
      <PublisherParent scope={{ p2: 2 }} />
    </Provider>
  )
  expect(wrapper.find(Child).props()).toMatchObject({ p1: 1, p2: 2 })
})

test("publisher returns message property from result", async () => {
  const host = { publish: jest.fn(() => Promise.resolve({ message: 'test' })) }
  const Child = () => <div></div>
  const Publisher = publisher(Child)
  const wrapper = enzyme.mount(<Provider host={host}><Publisher /></Provider>)
  const publish = wrapper.find(Child).props().publish
  expect(await publish('test', { value: 'test' })).toBe('test')
})
