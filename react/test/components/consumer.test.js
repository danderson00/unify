const React = require('react')
const enzyme = require('enzyme')
const { Provider, consumer } = require('../../src')

test("consumer supplies host prop to component", () => {
  const host = {}
  const Child = () => <div></div>
  const Consumer = consumer(Child)
  const wrapper = enzyme.mount(<Provider host={host}><Consumer /></Provider>)
  expect(wrapper.find(Child).props().host).toBe(host)
})
