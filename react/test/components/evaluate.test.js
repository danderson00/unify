const React = require('react')
const enzyme = require('enzyme')
const { Provider, evaluate } = require('../../src')
const hostModule = require('@x/unify.host')
const { subject, construct } = require('@x/expressions')

test("evaluate returns an observable for the specified expression", async () => {
  const host = hostModule({ log: { level: 'error' } })
  const Child = class extends React.Component {
    state = {}

    constructor(props) {
      super(props)
      props.evaluate(
        o => o.where(x => x.categoryId === categoryId).sum('value'),
        { categoryId: 1 }
      ).then(o => {
        this.setState({ sum: o() })
        o.subscribe(() => this.setState({ sum: o() }))
      })
    }

    render() {
      return <div>{this.state.sum}</div>
    }
  }
  const Evaluator = evaluate(Child)
  const wrapper = enzyme.mount(<Provider host={host}><Evaluator /></Provider>)
  await host.publish({ categoryId: 1, value: 2 })
  await new Promise(r => setTimeout(r, 20))
  expect(wrapper.find('div').text()).toBe('2')
  await host.publish({ categoryId: 1, value: 3 })
  expect(wrapper.find('div').text()).toBe('5')
  await host.publish({ categoryId: 2, value: 1 })
  expect(wrapper.find('div').text()).toBe('5')
  await host.publish({ categoryId: 1, value: 3 })
  expect(wrapper.find('div').text()).toBe('8')
})

test("evaluate returns an observable for the specified vocabulary name and parameters", async () => {
  const vocabulary = {
    sumOfMultiple: (o, multiple) => o.where(x => x % multiple === 0).sum()
  }
  const host = hostModule({ log: { level: 'error' }, expressions: construct(), vocabulary })
  const Child = class extends React.Component {
    state = {}

    constructor(props) {
      super(props)
      props.evaluate('sumOfMultiple', 3).then(o => {
        this.setState({ sum: o() })
        o.subscribe(() => this.setState({ sum: o() }))
      })
    }

    render() {
      return <div>{this.state.sum}</div>
    }
  }
  const Evaluator = evaluate(Child)
  const wrapper = enzyme.mount(<Provider host={host}><Evaluator /></Provider>)
  await host.publish(3)
  await new Promise(r => setTimeout(r, 20))
  expect(wrapper.find('div').text()).toBe('3')
  await host.publish(6)
  expect(wrapper.find('div').text()).toBe('9')
  await host.publish(5)
  expect(wrapper.find('div').text()).toBe('9')
  await host.publish(9)
  expect(wrapper.find('div').text()).toBe('18')
})

test("evaluate disconnects previous evaluation of the same expression unless a key is specified", async () => {
  const host = hostModule({ log: { level: 'error' } })
  const Child = class extends React.Component {
    state = { observables: [] }

    async UNSAFE_componentWillMount() {
      const expression = o => o.where(x => x.categoryId === categoryId).sum('value')

      // this first evaluation will be disconnected by the evaluation of o2
      const o1 = await this.props.evaluate(expression, { categoryId: 1 })
      o1.subscribe(() => this.forceUpdate())
      
      const o2 = await this.props.evaluate(expression, { categoryId: 1 })
      o2.subscribe(() => this.forceUpdate())

      const o3 = await this.props.evaluate(expression, { categoryId: 1 }, 'key1')
      o3.subscribe(() => this.forceUpdate())

      const o4 = await this.props.evaluate(expression, { categoryId: 2 }, 'key2')
      o4.subscribe(() => this.forceUpdate())

      this.setState({ observables: [o1, o2, o3, o4] })
    }

    render() {
      return <ul>{this.state.observables.map((o, index) => <li key={index}>{o()}</li>)}</ul>
    }
  }
  const Evaluator = evaluate(Child)
  const wrapper = enzyme.mount(<Provider host={host}><Evaluator /></Provider>)
  await host.publish({ categoryId: 1, value: 1 })
  await new Promise(r => setTimeout(r, 50))
  expect(wrapper.find('ul').text()).toBe('0110')
  await host.publish({ categoryId: 1, value: 1 })
  await host.publish({ categoryId: 2, value: 2 })
  expect(wrapper.find('ul').text()).toBe('0222')
})

test("evaluations of parameterised vocabulary are unique for different parameters", async () => {
  const vocabulary = {
    categoryValue: (o, categoryId) => o.where(x => x.categoryId === categoryId).map(x => x.value)
  }
  const host = hostModule({ log: { level: 'error' }, expressions: construct(), vocabulary })
  const Child = class extends React.Component {
    state = { observables: [] }

    async UNSAFE_componentWillMount() {
      const o1 = await this.props.evaluate('categoryValue', 1)
      o1.subscribe(() => this.forceUpdate())
      
      const o2 = await this.props.evaluate('categoryValue', 2)
      o2.subscribe(() => this.forceUpdate())

      this.setState({ observables: [o1, o2] })
    }

    render() {
      return <ul>{this.state.observables.map((o, index) => <li key={index}>{o()}</li>)}</ul>
    }
  }
  const Evaluator = evaluate(Child)
  const wrapper = enzyme.mount(<Provider host={host}><Evaluator /></Provider>)
  await host.publish({ categoryId: 1, value: 1 })
  await host.publish({ categoryId: 2, value: 2 })
  await new Promise(r => setTimeout(r, 50))
  expect(wrapper.find('ul').text()).toBe('12')
  await host.publish({ categoryId: 1, value: 3 })
  await host.publish({ categoryId: 2, value: 4 })
  expect(wrapper.find('ul').text()).toBe('34')
})

test("evaluate cascades scope to child components", () => {
  const model = subject()
  const Child = () => <div></div>
  const EvaluateChild = evaluate(Child)
  const Parent = () => <EvaluateChild scope={{ p1: 1 }} />
  const EvaluateParent = evaluate(Parent)
  const wrapper = enzyme.mount(
    <Provider resolve={() => model} publish={() => {}} host={{}}>
      <EvaluateParent scope={{ p2: 2 }} />
    </Provider>
  )
  expect(wrapper.find(Child).props()).toMatchObject({ p1: 1, p2: 2 })
})

test("resetScope resets scope to provided scope", () => {
  const model = subject()
  const Child = () => <div></div>
  const EvaluateChild = evaluate(Child)
  const Parent = () => <EvaluateChild resetScope={{ p1: 1 }} />
  const EvaluateParent = evaluate(Parent)
  const wrapper = enzyme.mount(
    <Provider resolve={() => model} publish={() => {}} host={{}}>
      <EvaluateParent scope={{ p2: 2 }} />
    </Provider>
  )
  expect(wrapper.find(Child).props().p1).toEqual(1)
})
