const React = require('react')
const enzyme = require('enzyme')
const { Provider, scoped } = require('../../src')

test("scoped cascades scope to child components", () => {
  const Child = () => <div></div>
  const ScopedChild = scoped(Child)
  const Parent = () => <ScopedChild scope={{ p1: 1 }} />
  const ScopedParent = scoped(Parent)
  const wrapper = enzyme.mount(
    <Provider resolve={() => model} publish={() => {}} host={{}}>
      <ScopedParent scope={{ p2: 2 }} />
    </Provider>
  )
  expect(wrapper.find(Child).props()).toMatchObject({ p1: 1, p2: 2 })
  expect(wrapper.find(Child).props().scope).toEqual({ p1: 1, p2: 2 })
})

test("resetScope resets scope to provided scope", () => {
  const Child = () => <div></div>
  const ScopedChild = scoped(Child)
  const Parent = () => <ScopedChild resetScope={{ p1: 1 }} />
  const ScopedParent = scoped(Parent)
  const wrapper = enzyme.mount(
    <Provider resolve={() => model} publish={() => {}} host={{}}>
      <ScopedParent scope={{ p2: 2 }} />
    </Provider>
  )
  expect(wrapper.find(Child).props().p1).toEqual(1)
  expect(wrapper.find(Child).props().scope).toEqual({ p1: 1 })
})