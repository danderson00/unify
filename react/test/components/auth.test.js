const React = require('react')
const enzyme = require('enzyme')
const { Provider, auth } = require('../../src')
const hostModule = require('@x/socket/host')
const consumerModule = require('@x/socket/consumer')
const hostFeature = require('@x/socket.auth/host')
const consumerFeature = require('@x/socket.auth/consumer')
const WebSocket = require('ws')

let server

const setup = async () => {
  server = new WebSocket.Server({ port: 1234 })
  hostModule({ server, log: { level: 'error' } })
    .useFeature(hostFeature({ log: { level: 'error' }, password: true }))
    .useApi({ hello: () => 'world' })

  return await consumerModule({ socketFactory: () => new WebSocket('ws://localhost:1234') })
    .useFeature(consumerFeature())
    .connect()
}

afterEach(() => server.close())

test("auth makes auth functions available", async () => {
  const host = await setup()
  const Child = () => <div></div>
  const Auth = auth(Child)
  const wrapper = enzyme.mount(<Provider host={host}><Auth /></Provider>)

  const authProp = wrapper.find(Child).props().auth
  expect(Object.keys(authProp)).toEqual([
    'authenticate', 'createUser', 'updateUserData', 'requestUserVerification', 'verifyUser', 'resetPassword', 'user', 'authenticated', 'logout'
  ]) // is a little flaky
  expect(authProp.authenticated).toBe(false)
})

test("users can be created, logged out and authenticated", async () => {
  const host = await setup()
  const Child = ({ auth }) => <div>{auth.user && auth.user.username}</div>
  const Auth = auth(Child)
  const wrapper = enzyme.mount(<Provider host={host}><Auth /></Provider>)
  const { createUser, logout, authenticate } = wrapper.find(Child).props().auth

  await createUser({ provider: 'password', username: 'test', password: 'abc123' })
  expect(wrapper.find('div').text()).toBe('test')

  await logout()
  expect(wrapper.find('div').text()).toBe('')

  await authenticate({ provider: 'password', username: 'test', password: 'abc123' })
  expect(wrapper.find('div').text()).toBe('test')
})