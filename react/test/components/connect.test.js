const React = require('react')
const enzyme = require('enzyme')
const { subject } = require('@x/expressions')
const { Provider, connect } = require('../../src')

test("connect spreads resolved observable onto props", () => {
  const model = subject({ initialValue: { model: { text: 'test' } } })
  const Child = ({ model }) => <div>{model.text}</div>
  const Connected = connect({ model: o => o })(Child)
  const wrapper = enzyme.mount(<Provider resolve={() => model} publish={() => {}} host={{}}><Connected /></Provider>)
  expect(wrapper.find('div').text()).toBe('test')
  model.publish({ model: { text: 'test2' } })
  expect(wrapper.find('div').text()).toBe('test2')
})

test("model can be specified in a function that receives props", () => {
  const createModel = text => subject({ initialValue: { model: { text } } })
  const Child = ({ model }) => <div>{model.text}</div>
  const Connected = connect(props => props.p1)(Child)
  const wrapper = enzyme.mount(<Provider
    resolve={(scope, model) => createModel(model[0])}
    publish={() => {}}
    host={{}}><Connected p1="test" />
  </Provider>)

  expect(wrapper.find('div').text()).toBe('test')
})

test("connect cascades scope to child components", () => {
  const model = subject()
  const Child = () => <div></div>
  const ConnectedChild = connect({ model: o => o })(Child)
  const ChildContainer = () => <ConnectedChild scope={{ p1: 1 }} />
  const Parent = () => <ChildContainer />
  const ConnectedParent = connect({ model: o => o })(Parent)
  const wrapper = enzyme.mount(
    <Provider resolve={() => model} publish={() => {}} host={{}}>
      <ConnectedParent scope={{ p2: 2 }} />
    </Provider>
  )
  expect(wrapper.find(Child).props()).toMatchObject({ p1: 1, p2: 2 })
})

test("resetScope resets scope to provided scope", () => {
  const model = subject()
  const Child = () => <div></div>
  const ConnectedChild = connect({ model: o => o })(Child)
  const Parent = () => <ConnectedChild resetScope={{ p1: 1 }} />
  const ConnectedParent = connect({ model: o => o })(Parent)
  const wrapper = enzyme.mount(
    <Provider resolve={() => model} publish={() => {}} host={{}}>
      <ConnectedParent scope={{ p2: 2 }} />
    </Provider>
  )
  expect(wrapper.find(Child).props().p1).toEqual(1)
})