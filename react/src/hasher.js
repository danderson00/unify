import hasher from 'string-hash'

export default definition => hasher(JSON.stringify(definition))