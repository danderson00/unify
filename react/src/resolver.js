import { userModel, queries, defaultModel } from './mapModel'

export default (expressions, host) => (scope, modelOrVocabulary, component) => {
  const { observable, unwrap } = expressions

  const model = userModel(modelOrVocabulary)

  const promise = Promise.all(queries(model, expressions.extractDefinition).map(
    query => host.subscribe(query, scope)
  )).then(observables => (
    expressions.mergeHash(
      Object.keys(model).reduce(
        (hash, name, index) => ({ ...hash, [name]: observables[index] }),
        {}
      )
    )
  ))

  let hostObservable

  const renderObservable = observable(publish => {
    const publishUnwrapped = o => publish(unwrap(o))

    promise
      .then(o => {
        hostObservable = o
        renderObservable.synced = true
        hostObservable.subscribe(publishUnwrapped)
        publishUnwrapped(hostObservable)
      })
      .catch(error => {
        throw new Error(`Error connecting component ${component.name || '(anonymous)'}:\n${errorMessage(error)}`)
      })
  }, {
    initialValue: defaultModel(model, expressions)
  })

  renderObservable.disconnect = () => {
    if(hostObservable) {
      hostObservable.disconnect()
    } else {
      return promise.then(observable => observable.disconnect())
    }
  }

  renderObservable.synced = false

  return renderObservable
}

const errorMessage = error => error && error.message || error || 'Unknown error'