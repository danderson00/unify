export default function flatten (array) {
  return (array && array.constructor === Array)
    ? array.reduce(
        (result, element) => [...result, ...flatten(element)],
        []
      )
    : array === undefined
      ? []
      : [array]
}