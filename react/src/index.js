import { createElement, createContext } from 'react'
import * as components from './components'
import * as hooks from './hooks'

const reactContext = createContext()
const renderConsumer = contextReceiver => createElement(reactContext.Consumer, { children: contextReceiver })
export const Provider = components.Provider(reactContext.Provider)

// HoCs
export const auth = component => originalProps => renderConsumer(
  ({ host }) => createElement(components.Auth, { host, component, originalProps })
)
export const connect = (...model) => component => scoped(originalProps => renderConsumer(
  context => createElement(components.Connect, { context, model, component, originalProps })
))
export const consumer = component => originalProps => renderConsumer(
  ({ host, expressions }) => createElement(components.Consumer, { host, expressions, component, originalProps })
)
export const evaluate = component => scoped(originalProps => renderConsumer(
  context => createElement(components.Evaluate, { context, component, originalProps })
))
export const publisher = component => scoped(originalProps => renderConsumer(
  ({ publish, publishLocal }) => createElement(components.Publisher, { publish, publishLocal, component, originalProps })
))
export const scoped = component => originalProps => renderConsumer(
  context => createElement(components.Scoped, { context, Provider, component, originalProps })
)

// hooks
export const useObservable = hooks.useObservable
export const useEvaluate = hooks.useEvaluate(reactContext)

// components
export const Navigator = ({ connect: connectProp, ...props }) => renderConsumer(
  ({ localBus }) => createElement(
    connectProp ? connect(connectProp)(components.Navigator) : components.Navigator,
    { scoped, localBus, ...props }
  )
)
// export const Navigator = originalProps => renderConsumer(
//   context => createElement(components.Navigator, { context, Provider, originalProps })
// )

export { Machine } from '@x/unify.fsm'
export { unwrap } from '@x/expressions'
