import { useState, useEffect, useContext, useCallback } from 'react'
import { defaultModelValue } from '../mapModel'
import { usePrevious } from '.'
import { deepEqual } from '@x/expressions/src/utilities'

export const useEvaluate = context => function useEvaluate(queryOrVocabulary, initialScope, initialArgs) {
  const { host, expressions } = useContext(context)
  const previousInitialArgs = usePrevious(initialArgs)
  const previousInitialScope = usePrevious(initialScope)

  const defaultValue = queryOrVocabulary => defaultModelValue(
    typeof queryOrVocabulary === 'string'
      ? { type: 'vocabulary', name: queryOrVocabulary }
      : { type: 'raw', expression: queryOrVocabulary }
  , expressions)

  const [promise, setPromise] = useState()
  const [value, setValue] = useState(defaultValue(queryOrVocabulary))

  const evaluate = useCallback((args, scope) => {
    disconnectCurrentPromise()

    const requestPromise = host
      .subscribe(getRequest(args), scope, { transient: true })
      .then(result => {
        setValue(result())
        result.subscribe(setValue)
        return result
      })

    setPromise(requestPromise)

    return requestPromise

    function getRequest(args) {
      switch(typeof queryOrVocabulary) {
        case 'string': return {
          type: 'vocabulary',
          name: queryOrVocabulary,
          parameters: args
        }
        case 'function': return {
          type: 'raw',
          definition: expressions.extractDefinition(
            queryOrVocabulary,
            { executionContext: { closures: args.length > 0 && args[0] }, source: false }
          )
        }
        default: throw new Error(`The first parameter to useEvaluate must be the name of a vocabulary or a stream expression`)
      }
    }
  }, [promise, host, expressions])

  const disconnectCurrentPromise = useCallback(() => {
    if(promise) {
      promise.then(o => o.disconnect())
    }
  }, [promise])

  useEffect(() => {
    if(
      (initialArgs && !deepEqual(initialArgs, previousInitialArgs)) ||
      (initialScope && !deepEqual(initialScope, previousInitialScope))
    ) {
      evaluate(initialArgs, initialScope)
      return disconnectCurrentPromise
    }
  }, [initialArgs, initialScope])

  useEffect(() => {

    return () => {
      disconnectCurrentPromise()
      // if(promise) {
      //   promise.then(o => o.disconnect())
      // }
    }
  }, [promise])

  // useEffect(() => {
  //   if(initialArgs) {
  //     evaluate(initialArgs)
  //   }
  //
  //   return disconnectCurrentPromise
  // }, [])

  return [
    value,
    (...args) => evaluate(args)
  ]
}