import { useState, useEffect } from 'react'
import { isObservable } from '@x/expressions'

export function useObservable(observable) {
  const [value, setValue] = useState(
    isObservable(observable)
      ? observable()
      : observable instanceof Array
        ? observable.map(o => o())
        : observable
  )

  useEffect(() => {
    if(isObservable(observable)) {
      setValue(observable())
      return observable.subscribe(setValue).unsubscribe

    } else if(observable instanceof Array) {
      const subscriptions = observable.map(
        (o, i) => o.subscribe(newValue => setValue(value => [
          ...value.slice(0, i),
          newValue,
          ...value.slice(i + 1)
        ]))
      )
      return () => subscriptions.forEach(x => x.unsubscribe())
    }
  }, [observable])

  return value
}