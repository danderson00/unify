module.exports = {
  userModel: modelOrVocabulary => {
    if(!modelOrVocabulary || modelOrVocabulary.length === 0) {
      throw new Error("No model was passed to the connect higher order component")
    }

    const mapVocabulary = (name, parameters) => ({ type: 'vocabulary', name, parameters })
    const mapExpression = expression => ({ type: 'raw', expression })

    function mapValue(value) {
      switch(typeof value) {
        case 'string': return { [value]: mapVocabulary(value) }
        case 'object': return mapObject(value)
        default: throw new Error('Connect parameter must be a vocabulary name or an object')
      }
    }

    function mapObject(value) {
      return Object.keys(value).reduce(
        (model, key) => ({ ...model, [key]: mapTargetValue(key, value[key]) }),
        {}
      )
    }

    function mapTargetValue(key, value) {
      switch(value && value.constructor) {
        case Array: return mapVocabulary(key, value)
        case Function: return mapExpression(value)
        case String: return mapVocabulary(value)
        default: throw new Error('Model object value must be a vocabulary name, array of vocabulary parameters or an expression')
      }
    }

    return modelOrVocabulary.reduce(
      (model, value) => ({ ...model, ...mapValue(value) }),
      {}
    )
  },
  queries: (model, extractDefinition) => {
    const query = ({ type, expression, name, parameters }) => {
      switch(type) {
        case 'raw': return { type: 'raw', definition: extractDefinition(expression) }
        case 'vocabulary': return { type: 'vocabulary', name, parameters }
        default: throw new Error('Invalid type')
      }
    }

    return Object.values(model).map(query)
  },
  defaultModelValue: ({ type, name, expression }, expressions) => {
    switch (type) {
      case 'raw':
        return expressions.unwrap(expression(expressions.subject()))
      case 'vocabulary':
        return expressions.unwrap(expressions.subject().construct(name))
      default:
        throw new Error('Invalid type')
    }
  },
  defaultModel: (model, expressions) => {
    return Object.keys(model).reduce(
      (result, key) => ({ ...result, [key]: module.exports.defaultModelValue(model[key], expressions) }),
      {}
    )
  }
}
