import { Component, createElement } from 'react'
import hasher from '../hasher'

export class Evaluate extends Component {
  constructor(props) {
    super(props)
    this.promises = {}

    this.execute = (queryKey, request) => {
      if (this.promises[queryKey]) {
        this.promises[queryKey].then(observable => observable.disconnect())
      }

      return this.promises[queryKey] = this.props.context.host.subscribe(request, this.props.originalProps.scope, { transient: true })
    }

    this.evaluate = async (...args) => {
      if (args.length === 0) {
        throw new Error("You must provide an expression or vocabulary name")
      }
      if (typeof args[0] === 'string') {
        return this.evaluateVocabulary.apply(this, args)
      } else {
        return this.evaluateExpression.apply(this, args)
      }
    }

    this.evaluateExpression = (expression, closures, key) => {
      // construct a key from the expression without closures so evaluating the same expression
      // causes the previous evaluation to be disconnected. for multiple evaluations, use an
      // additional key
      const { expressions } = this.props.context
      const queryKey = hasher(expressions.extractDefinition(expression)) +
        (key !== null && key !== undefined && key.toString())
      const definition = expressions.extractDefinition(expression, { executionContext: { closures }, source: false })
      return this.execute(queryKey, { type: 'raw', definition })
    }

    this.evaluateVocabulary = (name, ...parameters) => {
      const queryKey = JSON.stringify({ name, parameters })
      return this.execute(queryKey, { type: 'vocabulary', name, parameters })
    }
  }

  componentWillUnmount() {
    Object.values(this.promises).forEach(x => x.then(observable => observable.disconnect()))
  }

  render() {
    const { originalProps, component } = this.props
    const extendedProps = { 
      ...originalProps, 
      evaluate: this.evaluate
    }

    return createElement(component, extendedProps)
  }
}
