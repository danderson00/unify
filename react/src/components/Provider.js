import { Component, Fragment, createElement } from 'react'
import resolver from '../resolver'
import expressions from '@x/expressions'

let renderedTools = false

export function Provider(ContextProvider) {
  return class Provider extends Component {
    constructor(props) {
      super(props)

      if(!props.host) {
        throw new Error("You must pass a host to the Provider")
      }
    
      this.expressionsInstance = props.expressions || expressions
      if(props.vocabulary) {
        this.expressionsInstance.addVocabulary(props.vocabulary)
      }

      // localBus exists to support the Navigator component and is a complete hack
      // it will completely change when state machines are implemented
      this.localBus = props.localBus || expressions.subject()
    }

    publish(message) {
      return (this.props.publish || this.props.host.publish)(message)
        .then(result => {
          this.localBus.publish(message)
          return result
        })
    }

    publishLocal(message) {
      this.localBus.publish(message)
    }

    render() {
      const context = { 
        host: this.props.host,
        expressions: this.expressionsInstance,
        resolve: this.props.resolve || resolver(this.expressionsInstance, this.props.host), 
        publish: message => this.publish(message),
        publishLocal: message => this.publishLocal(message),
        parentScope: this.props.parentScope || {},

        localBus: this.localBus
      }

      // let devTools
      // if(process.env.NODE_ENV === 'development' && this.props.devTools !== false) {
      //   try {
      //     devTools = require('@x/unify.devtools/dist/components/Embed').default
      //   } catch {}
      // }
      //
      // if(renderedTools || !devTools) {
        return createElement(ContextProvider, { value: context, children: this.props.children })
      // } else {
      //   renderedTools = true
      //   return createElement(Fragment, {
      //     children: [
      //       createElement(ContextProvider, { key: 'context', value: context, children: this.props.children }),
      //       createElement(devTools, { key: 'tools', host: this.props.host })
      //     ]
      //   })
      // }
    }
  }
}