import { Component, createElement } from 'react'
import { proxy, isObservable } from '@x/expressions'

export class Auth extends Component {
  constructor(props) {
    super(props)

    if(!isObservable(props.host.authenticationStatus)) {
      throw new Error("The socket.auth host feature has not been loaded")
    }

    this.authenticationStatus = proxy(props.host.authenticationStatus)
    this.subscription = this.authenticationStatus.subscribe(() => this.forceUpdate())
  }

  componentWillUnmount() {
    this.subscription.unsubscribe()
    this.authenticationStatus.disconnect()
  }

  render() {
    const { component, host, originalProps } = this.props

    return createElement(component, { 
      auth: {
        authenticate: host.authenticate, 
        createUser: host.createUser,
        updateUserData: host.updateUserData,
        requestUserVerification: host.requestUserVerification,
        verifyUser: host.verifyUser,
        resetPassword: host.resetPassword,
        user: this.authenticationStatus().user,
        authenticated: this.authenticationStatus().authenticated,
        logout: host.logout
      },
      ...originalProps
    })
  }
}
