import { createElement } from 'react'

export function Scoped({ context, originalProps, Provider, component }) {
  const scope = originalProps.resetScope || 
    { ...context.parentScope, ...originalProps.scope }
  const extendedProps = { ...originalProps, ...scope, scope }

  return createElement(Provider, {
    ...context,
    parentScope: scope,
    key: JSON.stringify(scope),
    children: createElement(component, extendedProps)
  })
}
