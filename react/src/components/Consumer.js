import { createElement } from 'react'

export function Consumer({ host, expressions, component, originalProps }) {
  return createElement(component, {
    host,
    expressions,
    ...originalProps
  })
}
