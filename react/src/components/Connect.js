// these components are is serious need of an upgrade to functional components
import { Component, createElement } from 'react'
import { unwrap, deepEqual } from '@x/expressions/src/utilities'
import flatten from '../flatten'

export class Connect extends Component {
  constructor(props) {
    super(props)
    this.connect(props)
  }

  componentDidUpdate(prevProps) {
    if(!deepEqual(this.props.originalProps.scope, prevProps.originalProps.scope)) {
      this.disconnect()
      this.connect(this.props)
    }
  }

  componentDidMount() {
    this._isMounted = true
  }

  componentWillUnmount() {
    this._isMounted = false
    this.disconnect()
  }

  connect(props) {
    const model = flatten(props.model).map(x => typeof x === 'function' ? x(props.originalProps) : x)
    this.modelObservable = props.context.resolve(props.originalProps.scope, model, this.props.component)
    this.subscription = this.modelObservable.subscribe(() => this._isMounted && this.forceUpdate())
  }

  disconnect() {
    this.subscription.unsubscribe()
    this.modelObservable.disconnect()
  }

  render() {
    const extendedProps = {
      synced: this.modelObservable.synced,
      modelObservable: this.modelObservable,
      ...this.props.originalProps,
      ...unwrap(this.modelObservable)
    }

    return createElement(this.props.component, extendedProps)
  }
}
