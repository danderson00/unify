import { createElement } from 'react'

export function Publisher({ publish, publishLocal, component, originalProps }) {
  const messageFromArgs = (args, scope) => {
    if(args.length < 1) {
      throw new Error('You must pass arguments to the publish function')
    }

    if(typeof args[0] === 'string') {
      return { ...scope, topic: args[0], ...args[1] }
    }

    return { ...scope, ...args[0] }

  }

  const externalPublish = (...args) => {
    return publish(messageFromArgs(args, originalProps.scope))
      .then(({ message } = {}) => message)
  }

  externalPublish.local = (...args) => publishLocal(messageFromArgs(args, originalProps.scope))

  return createElement(component, { publish: externalPublish, ...originalProps })
}