import { createElement, useEffect, useState } from 'react'
import { router } from '@x/unify.fsm'

export function Navigator({ scoped, localBus, synced, modelObservable, machine, resolveComponent }) {
  const [route, setRoute] = useState()
  useEffect(
    () => {
      if(!modelObservable || synced) {
        router(localBus).attach(machine, resolveComponent, setRoute, { modelObservable })
      }
    },
    [localBus, synced, modelObservable, machine]
  )

  return route
    ? createElement(scoped(route.component), route.props)
    : null
}