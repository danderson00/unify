# Host Configuration

Path|Type|Default|Description
---|---|---|---
**General Options**|
logger|@x/log| |Log instance to use
log.level|String|"info"|One of "debug", "info", "warn", "error" or "fatal"
log.filter|Function| |Function that is passed log messages to determine which to log
watch|Boolean|`true` if development mode|Watch for changes to configuration files and reload on change
extendHost|Function| |A function that is passed the @x/socket host instance on initialisation to allow extensions to be loaded
socket|Object| |@x/socket options
storage|Object|{ client: "sqlite", { options: { filename: ":memory" } } }|@x/store options
**Server Options**|
server|HTTPServer| |HTTP server instance to attach to
port|Integer|3001|Port to start listening on
requireSsl|Boolean|`true` if production mode|Require SSL configuration
keyFile|String| |Path to key file 
certFile|String| |Path to certificate file
buildPath|String|"build"|Path to serve static application files from
redirectInsecure|Boolean|true|Redirect HTTP requests to HTTPS if SSL is configured
insecurePort|Integer|80|Port to serve redirect requests from
development|Boolean| |Run host in development mode
startDevelopmentServer|Boolean|`true` if in development mode|Start react development server
letsEncrypt.email|Email| |Email to use when requesting Let's Encrypt SSL certificate
letsEncrypt.domain|String| |Domain to use when requesting Let's Encrypt SSL certificate
letsEncrypt.configDir|String|./acme|Path to store Let's Encrypt configuration files
**`Unify` Host Options**|
scopes|\[String]| |Array of property names to use as scopes
vocabulary|String|"src/vocabulary"|Module path to vocabulary definitions
constraints|String|"src/constraints"|Module path to constraint definitions
types|String|"src/types"|Module path to message type definitions
strictTypes|Boolean|`true` if production mode|Enforce strict message types
strictApi|Boolean|`true` if production mode|Enforce strict vocabulary access
ownedScopes|[String]| |Array of scope names to maintain an "owners" list for
unify|@x/expressions| |@x/expressions instance to use
**Authentication Options**|
auth.secret|String|"secret"|Hashing key to use for JWT tokens
auth.password|Boolean|false|Enable password authentication
auth.auth0.domain|String| |Auth0 authentication domain
auth.facebook|Boolean|false|Enable facebook authentication
