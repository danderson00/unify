const hostModule = require('..')
const consumerModule = require('../consumer')
const expressions = require('@x/expressions')

test("simple publish subscribe", async () => {
  const host = await hostModule({ port: 1234, log: { level: 'fatal' } })
  const consumer = await consumerModule({ url: 'ws://localhost:1234/' }).connect()

  await consumer.publish({ value: 1 })
  const o = await consumer.subscribe({ type: 'raw', definition: expressions.extractDefinition(o => o.map(x => x.value * 2)) })
  expect(o()).toBe(2)
  await consumer.publish({ value: 2 })
  expect(o()).toBe(4)

  await host.close()
})

test("simple unsubscribe", async () => {
  const host = await hostModule({ port: 1234, log: { level: 'fatal' } })
  const consumer = await consumerModule({ url: 'ws://localhost:1234/' }).connect()
  const query = { type: 'raw', definition: expressions.extractDefinition(o => o.map(x => x.value * 2)) }

  await consumer.publish({ value: 1 })
  const o = await consumer.subscribe(query)
  expect(o()).toBe(2)
  await o.disconnect()
  await consumer.publish({ value: 2 })
  expect(o()).toBe(2)

  const o2 = await consumer.subscribe(query)
  expect(o2()).toBe(4)

  await host.close()
})

test("subscriptions are updated before publish promise resolves", async () => {
  const host = await hostModule({ port: 1234, log: { level: 'fatal' } })
  const consumer = await consumerModule({ url: 'ws://localhost:1234/' }).connect()
  const definition = expressions.extractDefinition(o => o.count())

  const spy = jest.fn()
  const o = await consumer.subscribe({ type: 'raw', definition })
  o.subscribe(spy)

  await consumer.publish({}).then(spy)
  expect(spy.mock.calls.length).toBe(2)
  expect(spy.mock.calls[0][0]).toBe(1)

  await host.close()
})

test("vocabulary can be executed on the host", async () => {
  const hostExpressions = expressions.construct()
  const host = await hostModule({ port: 1234, log: { level: 'fatal' }, expressions: hostExpressions, vocabulary: { getCount: o => o.count() } })
  const consumer = await consumerModule({ url: 'ws://localhost:1234/' }).connect()
  const o = await consumer.subscribe({
    type: 'raw',
    definition: hostExpressions.extractDefinition(o => o.getCount())
  })
  expect(o()).toBe(0)
  await consumer.publish({})
  expect(o()).toBe(1)

  await host.close()
})

test("join", async () => {
  const host = await hostModule({ port: 1234, log: { level: 'fatal' }, expressions: expressions.construct(), scopes: ['productId', 'orderId'] })
  const consumer = await consumerModule({ url: 'ws://localhost:1234/', expressions: expressions.construct() }).connect()
  const definition = expressions.extractDefinition(
    o => o.groupBy('productId',
      o => o.compose(
        o => o.topic('productAdded').accumulate('detail'),
        o => o.join('productId', o => o.topic('product').accumulate('detail')),
        (addedDetails, productDetails) => {
        return {
          ...addedDetails,
          ...productDetails,
          total: addedDetails.quantity * productDetails.price
        }
        }
      )
    )
  )

  await consumer.publish({ topic: 'product', productId: 1, detail: { name: 'widget', price: 1 } })
  await consumer.publish({ topic: 'product', productId: 2, detail: { name: 'component', price: 3 } })
  await consumer.publish({ topic: 'productAdded', orderId: 1, productId: 1, detail: { quantity: 2 } })
  await consumer.publish({ topic: 'productAdded', orderId: 1, productId: 2, detail: { quantity: 1 } })

  let o = await consumer.subscribe({ type: 'raw', definition }, { orderId: 1 })
  await new Promise(r => setTimeout(r, 100))
  expect(expressions.unwrap(o)).toEqual([
    { name: 'widget', price: 1, quantity: 2, total: 2 },
    { name: 'component', price: 3, quantity: 1, total: 3 }
  ])

  o.disconnect()
  await new Promise(r => setTimeout(r, 100))
  o = await consumer.subscribe({ type: 'raw', definition }, { orderId: 1 })
  await new Promise(r => setTimeout(r, 100))

  expect(expressions.unwrap(o)).toEqual([
    { name: 'widget', price: 1, quantity: 2, total: 2 },
    { name: 'component', price: 3, quantity: 1, total: 3 }
  ])
  await host.close()
})