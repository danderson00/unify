const watcher = require('../host/watcher')
const { writeFileSync, mkdirSync, unlinkSync, rmdirSync } = require('fs')
const nextTick = () => new Promise(r => setTimeout(r, 70))

test("triggers once when file with exact name is created", async () => {
  const spy = jest.fn()
  watcher(['test/watcher/blah.js'], () => {
    spy()
  })
  writeFileSync('test/watcher/blah.js', 'test')
  writeFileSync('test/watcher/blah.js', 'test2')
  await nextTick()
  expect(spy.mock.calls.length).toBe(1)
  unlinkSync('test/watcher/blah.js')
})

test("triggers once when file with exact name is modified", async () => {
  const spy = jest.fn()
  watcher(['test/watcher/existing.js'], () => {
    spy()
  })
  writeFileSync('test/watcher/existing.js', 'test')
  writeFileSync('test/watcher/existing.js', 'test2')
  await nextTick()
  expect(spy.mock.calls.length).toBe(1)
})

test("triggers once when directory with exact name is created", async () => {
  const spy = jest.fn()
  watcher(['test/watcher/blah'], () => {
    spy()
  })
  mkdirSync('test/watcher/blah')
  await nextTick()
  expect(spy.mock.calls.length).toBe(1)
  rmdirSync('test/watcher/blah')
})

test("triggers once when file with matching prefix is created", async () => {
  const spy = jest.fn()
  watcher(['test/watcher/blah'], () => {
    spy()
  })
  writeFileSync('test/watcher/blah.js', 'test')
  writeFileSync('test/watcher/blah.js', 'test2')
  await nextTick()
  expect(spy.mock.calls.length).toBe(1)
  unlinkSync('test/watcher/blah.js')
})

test("triggers once when file with matching prefix is modified", async () => {
  const spy = jest.fn()
  watcher(['test/watcher/existing'], () => {
    spy()
  })
  writeFileSync('test/watcher/existing.js', 'test')
  writeFileSync('test/watcher/existing.js', 'test2')
  await nextTick()
  expect(spy.mock.calls.length).toBe(1)
})

test("triggers once when file with matching prefix is deleted", async () => {
  const spy = jest.fn()
  writeFileSync('test/watcher/blah.js', 'test')
  watcher(['test/watcher/blah'], () => {
    spy()
  })
  unlinkSync('test/watcher/blah.js')
  await nextTick()
  expect(spy.mock.calls.length).toBe(1)
})

test("triggers once when file in matching directory is created", async () => {
  const spy = jest.fn()
  watcher(['test/watcher'], () => {
    spy()
  })
  writeFileSync('test/watcher/blah.js', 'test')
  writeFileSync('test/watcher/blah.js', 'test2')
  await nextTick()
  expect(spy.mock.calls.length).toBe(1)
  unlinkSync('test/watcher/blah.js')
})
