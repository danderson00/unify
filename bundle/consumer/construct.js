module.exports = (socket, authFeature, unifyFeature, filesFeature, devtoolsFeature) => {
  const defaultUrl = `ws://${typeof window === 'undefined' ? 'localhost' : window.location.hostname}:3001/`

  return (userOptions = {}) => {
    const options = {
      socket: {
        url: userOptions.url || defaultUrl,
        socketFactory: userOptions.socketFactory,
        ...(userOptions.log && { log: userOptions.log }),
        ...(userOptions.reconnectDelay && { reconnectDelay: userOptions.reconnectDelay })
      },
      logFeature: userOptions.log && {
        unhandled: userOptions.log.unhandled,
        filter: userOptions.log.filter
      },
      auth: userOptions.auth,
      files: userOptions.files,
      expressions: userOptions.expressions
    }

    const constructed = socket(options.socket)
      .useFeature(authFeature(options.auth))
      .useFeature('log', options.logFeature)
      .useFeature('reestablishSessions')
      .useFeature(filesFeature(options.files))
      .useFeature(unifyFeature(options.expressions))

    if(devtoolsFeature) {
      constructed.useFeature(devtoolsFeature())
    }

    return constructed
  }
}