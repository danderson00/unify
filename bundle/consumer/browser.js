const socket = require('@x/socket/consumer')
const authFeature = require('@x/socket.auth/consumer')
const unifyFeature = require('@x/socket.unify/consumer')
const filesFeature = require('@x/socket.files/consumer')

let devtoolsFeature
if(process.env.NODE_ENV === 'development') {
  try {
    devtoolsFeature = require('@x/unify.devtools/dist/feature').default
  } catch {}
}
module.exports = require('./construct')(socket, authFeature, unifyFeature, filesFeature, devtoolsFeature)