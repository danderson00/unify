const socket = require('@x/socket/consumer')
const authFeature = require('@x/socket.auth/consumer')
const unifyFeature = require('@x/socket.unify/consumer')
const filesFeature = require('@x/socket.files/consumer')

module.exports = require('./construct')(socket, authFeature, unifyFeature, filesFeature)