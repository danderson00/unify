const app = require('./app')
const greenlockServer = require('./greenlockServer')
const http = require('http')
const https = require('https')
const fs = require('fs')
const spawn = require('cross-spawn')
const WebSocket = require('ws')

module.exports = async (options, entryPackage, log) => {
  const listeningCallback = (name, port) => log.info(`${name} listening on ${port ? `port ${port}` : 'provided server'}`)

  const providedServer = options => ({ server: options.server })

  const developmentServer = (options, restart) => ({
    server: http.createServer(),
    ...(options.startDevelopmentServer && !restart && { devProcess: (() => {
      try {
        return spawn(
          'node',
          [require.resolve('react-scripts/scripts/start')],
          { stdio: 'inherit' }
        )
      } catch (error) {
        log.warn(`Unable to start react development server: ${error.message}`)
      }
    })() })
  })

  const secureServer = options => ({
    server: https.createServer(
      {
        key: fs.readFileSync(options.keyFile),
        cert: fs.readFileSync(options.certFile)
      },
      app(options, entryPackage)
    ),
    redirectServer: options.redirectInsecure && http.createServer((req, res) => {
      res.writeHead(301, { "Location": "https://" + req.headers['host'] + req.url })
      res.end()
    })
  })

  const insecureServer = options => ({
    server: http.createServer(app(options, entryPackage))
  })

  const createServers = async (options, restart) => {
    if(options.server) {
      return providedServer(options)
    }

    if(options.development) {
      return developmentServer(options, restart)
    }

    if(options.letsEncrypt) {
      if(!options.letsEncrypt.email) {
        throw new Error('email must be specified if letsEncrypt option is passed')
      }
      return greenlockServer(options, app(options, entryPackage), entryPackage, log)
    }

    if(options.requireSsl && (!options.keyFile || !options.certFile)) {
      throw new Error('SSL is required but a key file or certificate file was not provided')
    }

    if(options.keyFile && options.certFile) {
      return secureServer(options)

    }

    return insecureServer(options)
  }

  return startServers()

  async function startServers(restart) {
    const { server, redirectServer, devProcess } = await createServers(options, restart)
    const websocketServer = new WebSocket.Server({ server }, listeningCallback('@x/unify', options.port))

    const startServer = (target = {}, port) => {
      target.listening === false && target.listen(port)
    }
    startServer(server, options.port)
    if(redirectServer) {
      startServer(redirectServer, options.insecurePort)
    }

    const close = restart => new Promise(resolve => {
      devProcess && !restart && devProcess.kill()
      websocketServer.close(() => {
        server.close(() => {
          if (redirectServer) {
            redirectServer.close(resolve)
          } else {
            resolve()
          }
        })
      })
    })

    return {
      restartServers: () => close(true).then(() => startServers(true)),
      websocketServer,
      server,
      close
    }
  }
}
