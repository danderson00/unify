const express = require('express')
const { join } = require('path')

module.exports = (options, entryPackage) => {
  const app = express()
  // TODO: very nasty hack to make files middleware work - will break with different upload paths
  app.use((req, res, next) => {
    if(!req.path.startsWith('/uploads')) {
      next()
    }
  })
  app.use(express.static(join(entryPackage.path, options.buildPath)))
  app.get('*', (req, res) => (
    res.sendFile(join(entryPackage.path, options.buildPath, 'index.html'))
  ))
  return app
}