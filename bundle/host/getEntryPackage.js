const { execSync } = require('child_process')

module.exports = () => {
  try {
    const path = execSync('npm prefix').toString().trim()
    return { path, ...require(path + '/package.json') }
  } catch {
    return { path: process.cwd(), name: 'unknown', version: '0.0.0' }
  }
}
