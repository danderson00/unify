const { watch, lstatSync } = require('fs')
const { resolve, dirname, basename } = require('path')

module.exports = (paths, onChange) => {
  const pathsToWatch = paths.map(resolveWatcherPath)
  let restarted = false

  const watchers = pathsToWatch.map(({ path, filePrefix }) =>
    watch(path, (reason, filename) => {
      if(!filename || !filePrefix || filename.startsWith(filePrefix)) {
        restart()
      }
    })
  )

  const close = () => watchers.forEach(watcher => watcher.close())

  return { close }

  function restart() {
    if(!restarted) {
      restarted = true
      close()
      setTimeout(onChange, 50)
    }
  }

  function resolveWatcherPath(sourcePath) {
    const fullPath = resolve(sourcePath)
    return isDirectory(fullPath)
      ? { path: fullPath }
      : { path: dirname(fullPath), filePrefix: basename(fullPath) }
  }

  function isDirectory(path) {
    try {
      return lstatSync(path).isDirectory()
    } catch {
      return false
    }
  }
}

/*
const { watch } = require('fs')
const { join } = require('path')
const loadConfiguration = require('./configuration')

module.exports = (start, servers, configuration, userOptions, entryPackage) => {
  const { configFile, vocabulary, constraints, types } = userOptions
  const pathsToWatch = [configFile, `${configFile}.${process.env.NODE_ENV}`, vocabulary, constraints, types]
    .map(path => join(entryPackage.path, path))
    .map(resolvePath)
    .filter(Boolean)

  const socket = start(configuration, servers.websocketServer)
  const watchers = pathsToWatch.map(path => watch(path, restart))

  return socket

  function restart() {
    watchers.forEach(watcher => watcher.close())
    return servers.restartServers().then(newServers => {
      const newConfiguration = loadConfiguration(userOptions, entryPackage)
      return module.exports(start, newServers, newConfiguration, userOptions, entryPackage)
    })
  }
}

function resolvePath(path) {
  try { return require.resolve(path) } catch { }
}
 */