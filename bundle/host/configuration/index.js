const log = require('@x/log')
const moduleLoader = require('./moduleLoader')

const defaultOptions = {
  configFile: 'unify',
  vocabulary: 'src/vocabulary',
  constraints: 'src/constraints',
  types: 'src/types',
  port: process.env.NODE_ENV === 'production' ? 443 : 3001,
  storage: { client: 'sqlite3', connection: { filename: ':memory:' } },
  scopes: [],
  development: !process.env.NODE_ENV || process.env.NODE_ENV === 'development',
  buildPath: 'build',
  redirectInsecure: process.env.NODE_ENV === 'production',
  insecurePort: 80,
  watch: !process.env.NODE_ENV || process.env.NODE_ENV === 'development'
}

const defaultLogOptions = {
  level: 'info',
  scope: { source: 'expressions.bundle.host' }
}

module.exports = (userOptions = {}, entryPackage) => {
  const { configurationFile, sourceOption } = moduleLoader(entryPackage)
  const options = {
    ...defaultOptions,
    ...configurationFile(userOptions.configFile),
    ...userOptions
  }
  const logger = log({ ...defaultLogOptions, ...options.log })

  return {
    logger,
    watch: options.watch,

    socketOptions: {
      logger,
      ...options.socket
    },

    serverOptions: {
      ...(options.server
        ? { server: options.server }
        : { port: options.port }),
      requireSsl: options.requireSsl !== false && process.env.NODE_ENV === 'production',
      keyFile: options.keyFile,
      certFile: options.certFile,
      buildPath: options.buildPath,
      redirectInsecure: options.redirectInsecure,
      insecurePort: options.insecurePort,
      development: options.development,
      startDevelopmentServer: options.startDevelopmentServer,
      letsEncrypt: options.letsEncrypt
    },

    unifyOptions: {
      scopes: options.scopes || [],
      ...sourceOption('vocabulary', options.vocabulary, 'src/vocabulary'),
      ...sourceOption('constraints', options.constraints, 'src/constraints'),
      ...sourceOption('types', options.types, 'src/types'),
      strictTypes: options.strictTypes,
      strictApi: options.strictApi,
      ownedScopes: options.ownedScopes,
      ...(options.expressions && { expressions: options.expressions }),
      storage: options.storage,
      logger
    },

    authOptions: options.auth && {
      storage: options.storage,
      ...options.auth
    },

    filesOptions: options.files && {
      ...options.files
    },

    bundleOptions: {
      extendHost: options.extendHost
    }
  }
}
