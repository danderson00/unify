const { join } = require('path')
const errorSource = require('./errorSource')

module.exports = entryPackage => {
  const sourceOption = (name, value, defaultValue) => ({
    [name]: (typeof value === 'string' || !value)
      ? loadModule(name, value || defaultValue, defaultValue)
      : value
  })

  const shortStack = stack => stack.substring(0, stack.split('\n', 2).join('\n').length)

  const loadModule = (name, path, defaultPath = path) => {
    const fullPath = join(entryPackage.path, path || defaultPath)
    try {
      clearRequireCache(fullPath)
      return require(fullPath)
    } catch (error) {
      // this is not quite right - swallows MODULE_NOT_FOUND from the actual configuration file
      // I think we can use `error.requireStack[0] !== __filename` or similar?
      if(error.code !== 'MODULE_NOT_FOUND' || (error.requireStack && error.requireStack[0] !== __filename)) {
        throw new Error(`Error loading ${name} module ${fullPath}:\n${errorSource(error, entryPackage)}`)
      }

      if (path && path !== defaultPath) {
        throw new Error(`${name} module not found: ${fullPath}`)
      }
    }
  }

  const fullConfigurationPath = path => (
    (process.env.NODE_ENV === 'development' || !process.env.NODE_ENV)
      ? path
      : `${path}.${process.env.NODE_ENV}`
  )

  // ideally we want to check a default set of paths: `unify.{environment}.js`, `config/unify.{environment}.js`, etc.
  const configurationFile = path =>
    loadModule('configuration', fullConfigurationPath(path || 'unify'), fullConfigurationPath('unify')) ||
    loadModule('configuration', path, 'unify') // fall back to development config

  return { sourceOption, configurationFile }
}

function clearRequireCache(path) {
  try {
    delete require.cache[require.resolve(path)]
  } catch {}
}