// from https://github.com/errwischt/stacktrace-parser/blob/9b2d5b6114e7a3b3596f7092f0d1160738125374/src/stack-trace-parser.js
const stackRegex = /^\s*at (?:((?:\[object object\])?[^\\/]+(?: \[as \S+\])?) )?\(?(.*?):(\d+)(?::(\d+))?\)?\s*$/i;
const UNKNOWN_FUNCTION = '<unknown>'

const parseLine = line => (
  (parts => parts && {
    file: parts[2],
    methodName: parts[1] || UNKNOWN_FUNCTION,
    arguments: [],
    lineNumber: +parts[3],
    column: parts[4] ? +parts[4] : null,
  })
  (stackRegex.exec(line))
)

const errorSource = stack => (
  stack.split('\n').reduce((stack, line) => (
    (parsed => parsed ? [...stack, parsed] : stack)
    (parseLine(line))
  ), [])
)

module.exports = (error, entryPackage) => (
  (parsed => `${parsed[0].file.replace(entryPackage.path + '/', '')} (${parsed[0].lineNumber}:${parsed[0].column}): ${error.message}`)
  (errorSource(error.stack))
)
