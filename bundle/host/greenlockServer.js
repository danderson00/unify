// the `cert-info` package module loader initialises differently if the global `window` property is set
// jest screws with this, and we'll never be running in a browser, so just don't load greenlock
if(typeof window === 'undefined')
{
  const greenlock = require('greenlock')
  const greenlockExpress = require('greenlock-express')

  module.exports = async (options, staticContentListener, entryPackage, log) => {
    if(options.port !== 443) {
      throw new Error('greenlock-express currently requires port 443')
    }

    const { letsEncrypt } = options

    const packageRoot = entryPackage.path
    const configDir = letsEncrypt.configDir || (packageRoot + '/acme')
    const packageAgent = `${entryPackage.name}/${entryPackage.version}`

    const instance = greenlock.create({
      packageRoot,
      configDir,
      packageAgent,
      email: letsEncrypt.email,
      subscriberEmail: letsEncrypt.email,
      maintainerEmail: letsEncrypt.email,
      agreeTos: true,
      communityMember: false,
      securityUpdates: false,
      version: 'draft-12',
      server: 'https://acme-v02.api.letsencrypt.org/directory',
      rsaKeySize: 2048,
      challengeType: 'http-01',
      cluster: false
    })

    await instance.add({
      subject: letsEncrypt.domain,
      altnames: [letsEncrypt.domain]//, 'www.' + letsEncrypt.domain]
    })

    return new Promise((resolve, reject) => {
      try {
        greenlockExpress
          .init({
            packageRoot,
            configDir,
            maintainerEmail: letsEncrypt.email
          })
          .ready(({ serveApp, httpsServer }) => {
            serveApp(staticContentListener)
            resolve({ server: httpsServer() })
          })
      } catch(error) {
        reject(error)
      }
    })
  }
}