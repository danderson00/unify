const index = require('@x/socket/host')
const authFeature = require('@x/socket.auth/host')
const unifyFeature = require('@x/socket.unify/host')
const filesFeature = require('@x/socket.files/host')
const loadConfiguration = require('./configuration')
const createServers = require('./createServers')
const getEntryPackage = require('./getEntryPackage')
const startWatcher = require('./watcher')
const { writers } = require('@x/log')

module.exports = async userOptions => {
  const entryPackage = getEntryPackage()
  const configuration = loadConfiguration(userOptions, entryPackage)
  const servers = await createServers(configuration.serverOptions, entryPackage, configuration.logger)
  const { websocketServer, server, close } = servers

  if(configuration.watch) {
    watch(servers, configuration, userOptions, entryPackage)
  } else {
    return {
      ...start(configuration, websocketServer, server),
      close
    }
  }
}

module.exports.logWriters = writers

function start(configuration, websocketServer, httpServer) {
  const { socketOptions, unifyOptions, authOptions, filesOptions, bundleOptions } = configuration
  const unify = unifyFeature(unifyOptions)

  const socket = index({ server: websocketServer, httpServer, ...socketOptions })
    .useFeature('log')
    .useFeature(unify)

  if (filesOptions) {
    socket.useFeature(filesFeature(filesOptions))
  }

  if (authOptions) {
    socket.useFeature(authFeature(authOptions))
  }

  if (bundleOptions.extendHost) {
    bundleOptions.extendHost(socket)
  }

  return { ...socket, host: unify.host }
}

function watch(servers, configuration, userOptions, entryPackage) {
  const socket = start(configuration, servers.websocketServer, servers.server)

  const { configFile, vocabulary, constraints, types } = userOptions
  const paths = [configFile, `${configFile}.${process.env.NODE_ENV}`, vocabulary, constraints, types]
  startWatcher(paths, restart)

  return socket

  async function restart() {
    await socket.host.purgeExpressionCache()
    servers = await servers.restartServers()
    const newConfiguration = loadConfiguration(userOptions, entryPackage)
    startWatcher(paths, restart)
    return start(newConfiguration, servers.websocketServer, servers.server)
  }
}
