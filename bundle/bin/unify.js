#!/usr/bin/env node
const yargs = require('yargs')
const host = require('..')

// TODO: log, auth, HTTPS

const argv = yargs
  .usage('unify platform host process\n')
  .usage('Usage: $0 [...options]')
  .describe({
    config: 'Path to JS or JSON config file',
    port: 'TCP port number to listen on',
    scope: 'Property name that can participate in scopes. Multiple can be specified',
    sqliteFile: 'Path to a SQLite database to use',
    vocabulary: 'Path to a file containing vocabulary definitions',
    constraints: 'Path to a file containing constraint definitions',
    types: 'Path to a file containing type declarations',
    strictApi: 'Enforce strict vocabulary API surface',
    strictTypes: 'Enforce explicit message types',
    requireSsl: 'Allows disabling of SSL requirement for production',
    auth0Domain: 'Enable auth0 authentication with the provided domain',
    authSecret: 'Secret to use for signing authentication tokens',
    startDevelopmentServer: 'Start the react development server if present and not in production mode',
    watch: 'Watch for changes to configuration files'
  })
  .default('config', 'unify')
  .default('constraints', 'src/constraints')
  .default('vocabulary', 'src/vocabulary')
  .default('types', 'src/types')
  .default('startDevelopmentServer', true)
  .option({
    startDevelopmentServer: { type: 'boolean' },
    watch: { type: 'boolean' },
    strictApi: { type: 'boolean' },
    strictTypes: { type: 'boolean' },
    requireSsl: { type: 'boolean' }
  })
  .number('port')
  .alias({
    constraints: 'c',
    help: 'h',
    port: 'p',
    scope: 's',
    sqliteFile: 'f',
    vocabulary: 'v',
    types: 't'
  })
  .example(
    '$0 -s orderId -s productId',
    'Use two scopes, `orderId` and `productId`'
  )
  .example(
    '$0 -v config/vocabulary.js -c config/constraints.js',
    'Load vocabulary from `config/vocabulary.js` and constraints from `config/constraints.js`'
  )
  .argv

const optional = name => argv.hasOwnProperty(name) && { [name]: argv[name] }

host({
  configFile: argv.config,
  ...(argv.watch !== undefined && { watch: argv.watch }),
  ...optional('port'),
  startDevelopmentServer: argv.startDevelopmentServer,
  ...(argv.scope && { scopes: [].concat(...[argv.scope]) }),
  ...optional('constraints'),
  ...optional('vocabulary'),
  ...optional('types'),
  ...optional('strictApi'),
  ...optional('strictTypes'),
  ...optional('requireSsl'),
  ...(argv.sqliteFile && { storage: { client: 'sqlite3', connection: { filename: argv.sqliteFile } } }),
  ...((argv.auth0Domain || argv.authSecret) && { auth: {
    secret: argv.authSecret,
    ...(argv.authSecret && { auth0: { domain: argv.auth0Domain } })
  } })
})
