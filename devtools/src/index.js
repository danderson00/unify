import React from 'react'
import ReactDOM from 'react-dom'
import Standalone from './components/Standalone'

ReactDOM.render(
  <Standalone />,
  document.getElementById('root')
)
