import React from 'react'

export default () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" enableBackground="new 0 0 24 24" fill="#ffffff" width="24px" height="24px">
    <path d="M 5 3 C 3.9 3 3 3.9 3 5 L 3 19 C 3 20.1 3.9 21 5 21 L 19 21 C 20.1 21 21 20.1 21 19 L 21 5 C 21 3.9 20.1 3 19 3 L 5 3 z M 7 8 L 17 8 L 17 17 L 7 17 L 7 8 z M 9 11 L 9 15 L 15 15 L 15 11 L 9 11 z" />
  </svg>
)
