import React from 'react'

export default () => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="#ffffff" width="24px" height="24px">
    <path d="M20,2H4C2.895,2,2,2.895,2,4v16c0,1.105,0.895,2,2,2h16c1.105,0,2-0.895,2-2V4C22,2.895,21.105,2,20,2z M16.9,15.59 L15.49,17l-3.54-3.54L8.41,17L7,15.59l3.54-3.54L7,8.51L8.41,7.1l3.54,3.54l3.54-3.54l1.41,1.41l-3.54,3.54L16.9,15.59z"/>
  </svg>
)