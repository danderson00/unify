import React, { createContext, useContext } from 'react'

const context = createContext()

export const ContextProvider = ({ internalHost, externalHost, vocabulary, log, children }) => (
  <context.Provider
    value={{ internalHost, externalHost, vocabulary, log }}
    children={children}
  />
)

export const useDevtoolsContext = () => useContext(context)