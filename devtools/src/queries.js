import { extractDefinition } from '@x/expressions'

export function compile(text) {
  try {
    // we'll implement a better way later...
    // eslint-disable-next-line
    return raw(eval(text))
  } catch(error) {
    throw new Error(`An error occurred compiling your query: ${error.message}`)
  }
}

export function raw(query, closures) {
  return {
    type: 'raw',
    definition: extractDefinition(query, { executionContext: { closures }, source: false })
  }
}