export default function injectCss(id, css) {
  const fullId = `__unify__devtools_${id}`
  if (!css || typeof document === 'undefined' || document.querySelector(`#${fullId}`)) return

  const head = document.head || document.getElementsByTagName('head')[0]
  const style = document.createElement('style')
  style.id = fullId
  style.type = 'text/css'

  head.appendChild(style)

  if (style.styleSheet) {
    style.styleSheet.cssText = css
  } else {
    style.appendChild(document.createTextNode(css))
  }
}