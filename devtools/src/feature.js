import React from 'react'
import ReactDOM from 'react-dom'
import { subject } from '@x/expressions'
import Devtools from './components/Devtools'

const feature = (options = {}) => ({ log }) => {
  log = log.child({ source: 'unify.devtools' })

  return {
    name: 'unify.devtools',
    initialise: ({ api: host, handshakeData }) => {

      const { standalone } = options
      const vocabulary = subject({ initialValue: handshakeData.unify.vocabulary })

      render({ host, vocabulary, log, standalone })

      return {
        connect: ({ handshakeData }) => {
          vocabulary.publish(handshakeData.unify.vocabulary)
        }
      }
    }
  }

  function render(props) {
    const hostElement = document.createElement('div')
    document.body.appendChild(hostElement)
    ReactDOM.render(<Devtools {...props} />, hostElement)
  }
}

export default feature
