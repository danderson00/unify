import injectCss from '../injectCss'

injectCss('Query', `
.__unify__devtools_Query {
margin: 2px 0;
  display: flex;
}

.__unify__devtools_Query > *:first-child {
  flex-grow: 1;
}

.__unify__devtools_Query > div:last-child {
  display: flex;
  flex-direction: column;
}

.__unify__devtools_Query textarea {
  font-family: monospace;
  resize: vertical;
}

.__unify__devtools_Query button {
  width: 80px;
  flex-grow: 1;
}

.__unify__devtools_Query label {
  margin: 2px;
  font-size: 12px;
  font-weight: normal;
  display: flex;
  justify-content: space-between;
  align-items: center;
}

.__unify_devtools_vocabulary {
  display: flex;
  flex-direction: row;
}
`)
