import React, { useState } from 'react'
import feature from '../feature'
import consumer from '@x/unify'

export default ({ onConnect }) => {
  const [error, setError] = useState(undefined)
  const [url, setUrl] = useState('ws://localhost:3001/')

  const connect = () => consumer({ url })
    .useFeature(feature({ standalone: true }))
    .connect()
    .then(onConnect)
    .catch(setError)

  const submit = e => {
    e.preventDefault()
    connect()
    return false
  }

  return (
    <form onSubmit={submit}>
      <input value={url} onChange={e => setUrl(e.target.value)} required />
      <input type="submit" value="Connect" />
      <p className="error">{error && error.message}</p>
    </form>
  )
}