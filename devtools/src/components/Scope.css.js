import injectCss from '../injectCss'

injectCss('Scope', `
.__unify__devtools_Scope {
  padding: 0 5px;
}

.__unify__devtools_Scope label {
  display: block;
  font-size: 10px;
}

.__unify__devtools_Scope input {
  box-sizing: border-box;
  width: 100%;
}
`)
