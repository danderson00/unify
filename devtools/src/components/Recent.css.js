import injectCss from '../injectCss'

injectCss('Recent', `
.__unify__devtools_Recent {
  padding: 0 5px;
}

.__unify__devtools_Recent h4 {
  margin: 5px 0;
}

.__unify__devtools_Recent ul li {
  padding: 0 5px;
}

.__unify__devtools_Recent ul li:hover {
  cursor: pointer;
  background: rgba(255, 255, 255, 0.3);
}

.__unify__devtools_Recent ul code {
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
}
`)
