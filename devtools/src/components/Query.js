import React, { useState, useEffect, useCallback } from 'react'
import { consumer, useObservable } from '@x/unify.react'
import { raw } from '../queries'
import './Query.css.js'
import { useDevtoolsContext } from '../context'

export default consumer(({ host, evaluate, scope }) => {
  const [query, setQuery] = useState('')
  const [rawQuery, setRawQuery] = useState(true)
  const [selectedVocabulary, setSelectedVocabulary] = useState({})
  const [parameters, setParameters] = useState([])
  const { vocabulary } = useDevtoolsContext()
  const vocabularyList = useObservable(vocabulary)

  const selectVocabulary = useCallback(name => {
    const nextSelectedVocabulary = vocabularyList.find(x => x.name === name)
    setSelectedVocabulary(nextSelectedVocabulary)
    setParameters(Array(nextSelectedVocabulary.parameters.length).fill(''))

  }, [vocabularyList, setSelectedVocabulary])

  const setParameter = (i, value) => setParameters([
    ...parameters.slice(0, i),
    value,
    ...parameters.slice(i + 1)
  ])

  useEffect(() => {
    async function subscribeToQueries() {
      const observable = await host.subscribe(raw(
        o => o.topic('querySelected')
      ))
      observable.subscribe(({ text, name, parameters }) => {
        if(name) {
          setRawQuery(false)
          selectVocabulary(name)
          setParameters(parameters)
        } else {
          setRawQuery(true)
          setQuery(text)
        }
      })
    }
    subscribeToQueries()
  }, [host, selectVocabulary])

  const execute = () => {
    evaluate(rawQuery ? { text: query } : { name: selectedVocabulary.name, parameters }, scope)
  }

  return (
    <div className="__unify__devtools_Query">
      {rawQuery
        ? <textarea
          label="Query"
          onChange={e => setQuery(e.target.value)}
          value={query}
          rows="4"
          placeholder="Enter query"
        />
        : <div className="__unify__devtools_vocabulary">
          <select
            value={selectedVocabulary.name}
            onChange={e => selectVocabulary(e.target.value)}
          >
            {vocabularyList.map(({ name }) => (
              <option key={name}>{name}</option>
            ))}
          </select>
          <div>
            {(selectedVocabulary.parameters || []).map((name, i) => (
              <div key={name}>
                {name}
                <input value={parameters[i] || ''} onChange={e => setParameter(i, e.target.value)} />
              </div>
            ))}
          </div>
        </div>
      }

      <div>
        <label>
          Vocabulary
          <input type="checkbox" onChange={() => setRawQuery(!rawQuery)} checked={!rawQuery} />
        </label>
        <button onClick={execute}>Execute</button>
      </div>
    </div>
  )
})
