import React, { useState } from 'react'
import CloseIcon from '../../assets/CloseIcon'
import MaximiseIcon from '../../assets/MaximiseIcon'
import MinimiseIcon from '../../assets/MinimiseIcon'
import NewWindowIcon from '../../assets/NewWindowIcon'
import RestoreIcon from '../../assets/RestoreIcon'
import './Window.css.js'

export default ({ open, header, allowResize, closeButton, machinesButton, dark, onClose, children, maximised }) => {
  const [windowState, setWindowState] = useState(maximised ? 'maximised' : '')

  const [machinesOpen, setMachinesOpen] = useState(window.localStorage.getItem('unify.devtools.inspect') === 'true')

  const toggleOpenMachines = () => {
    setMachinesOpen(!machinesOpen)

    if(machinesOpen) {
      window.localStorage.removeItem('unify.devtools.inspect')
    } else {
      window.localStorage.setItem('unify.devtools.inspect', 'true')
      window.location.reload()
    }
  }

  return (
    <div
      className={`__unify__devtools_Window ${windowState}${dark ? ' dark' : ''}`}
      style={{ display: open ? 'flex' : 'none' }}
    >
      <header>
        <div>{header}</div>
        {machinesButton &&
          <label>
            Machines
            <NewWindowIcon />
            <input type="checkbox" onChange={toggleOpenMachines} checked={machinesOpen} />
          </label>
        }
        {allowResize && <>
          <button
            onClick={() => setWindowState(windowState === 'minimised' ? '' : 'minimised')}
          >
            {windowState === 'minimised' ? <RestoreIcon /> : <MinimiseIcon />}
          </button>
          <button
            onClick={() => setWindowState(windowState === 'maximised' ? '' : 'maximised')}
          >
            {windowState === 'maximised' ? <RestoreIcon /> : <MaximiseIcon />}
          </button>
        </>}
        {closeButton !== false ? <button onClick={onClose}><CloseIcon /></button> : null}
      </header>

      <main>
        {children}
      </main>
    </div>
  )
}