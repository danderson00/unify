import injectCss from '../../injectCss'

injectCss('Dialog', `
.__unify__devtools_Dialog > div:first-child {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: rgba(0, 0, 0, 0.4);
}

.__unify__devtools_Dialog .__unify__devtools_Window {
  height: 200px;
  width: 300px;
  top: calc(50% - 100px);
  left: calc(50% - 150px);
  border-color: #999999;
}
`)