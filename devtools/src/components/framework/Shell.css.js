import injectCss from '../../injectCss'

injectCss('Shell', `
.__unify__devtools_Shell {
  display: flex;
  flex-direction: column;
}

.__unify__devtools_Shell > main {
  min-height: 0;
  flex: 1;
  display: flex;
}

.__unify__devtools_Results {
  overflow: auto;
  flex: 1;
}

.__unify__devtools_Shell > main > aside {
  width: 150px;
  overflow: auto;
}

.__unify__devtools_Shell > main > aside > * {
  margin-top: 5px;
}

.maximised .__unify__devtools_Shell > main > aside {
  width: 250px;
}

.__unify__devtools_Shell ul {
  list-style: none;
  margin: 0;
  padding: 0;
}

.__unify__devtools_Shell h4 {
  margin: 0;
  padding: 0;
}
`)
