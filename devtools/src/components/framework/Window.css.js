import injectCss from '../../injectCss'

injectCss('Window', `
.__unify__devtools_Window * {
  font-family: sans-serif;
}

.__unify__devtools_Window input, .__unify__devtools_Window textarea {
  font-family: monospace;
}

.__unify__devtools_Window {
  flex-direction: column;
  padding: 0;
  margin: 0;
  position: fixed;
  width: 800px;
  height: 600px;
  right: 20px;
  bottom: 20px;
  border-radius: 5px;
  border: 1px solid #dddddd;
  overflow: hidden;
  box-shadow: 3px 3px 5px 0px rgba(0, 0, 0, 0.1);
  background: white;
}

.__unify__devtools_Window.maximised {
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  width: inherit;
  height: inherit;
  display: flex;
  border-radius: 0;
  border: none;
}

.__unify__devtools_Window.maximised main {
  flex: 1;
}

.__unify__devtools_Window.minimised, .__unify__devtools_Window.minimised main {
  height: 30px;
}

.__unify__devtools_Window header {
  background: #cccccc;
  display: flex;
  align-items: center;
  padding-left: 10px;
  font-weight: bold;
  flex: 0 0 auto;
  height: 30px;
}

.__unify__devtools_Window header > div {
  flex: 1;
  font-weight: bold;
}

.__unify__devtools_Window button {
  outline: none;
}

.__unify__devtools_Window header > button {
  max-width: 28px;
  width: 28px;
  height: 28px;
  display: flex;
  align-items: center;
  justify-content: center;
  outline: none;
  cursor: pointer;
}

.__unify__devtools_Window header > label svg {
  margin: 0 4px;
}

.__unify__devtools_Window header > label {
  font-size: 12px;
  font-weight: normal;
  display: flex;
  align-items: center;
  margin-right: 12px;
}

.__unify__devtools_Window main {
  height: 370px;
}

.__unify__devtools_Window main > * {
  height: 100%;
}


/* dark theme */
.__unify__devtools_Window.dark {
  background: rgba(0, 0, 0, 0.9);
  color: #ffffff;
}

.__unify__devtools_Window.dark header {
  background: rgba(0, 0, 0, 0.2);
}

.__unify__devtools_Window.dark input, .__unify__devtools_Window.dark textarea, .__unify__devtools_Window.dark button {
  color: #ffffff;
  background: rgba(255, 255, 255, 0.2);
  border: 1px solid rgba(255, 255, 255, 0.3);
  border-radius: 4px;
  margin: 1px;
}

.__unify__devtools_Window.dark ::placeholder {
  color: #999999;
}

.__unify__devtools_Window.dark input, .__unify__devtools_Window.dark textarea {
  background: rgba(255, 255, 255, 0.1);
  padding: 4px;
}

.__unify__devtools_Window.dark ::-webkit-scrollbar {
  width: 10px;
  height: 10px;
}

.__unify__devtools_Window.dark ::-webkit-scrollbar-button {
  display: none;
}

.__unify__devtools_Window.dark ::-webkit-scrollbar-corner {
  background: transparent;
}

.__unify__devtools_Window.dark ::-webkit-scrollbar-thumb {
  background: rgba(255, 255, 255, 0.2);
  outline: 1px solid slategrey;
  border-radius: 2px;
}


`)

/* @media screen and (prefers-color-scheme: light) { } */