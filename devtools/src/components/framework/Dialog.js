import React from 'react'
import Window from './Window'
import './Dialog.css.js'

export default function Dialog(props) {
  const backgroundClicked = () => (props.onClose && props.closeButton !== false) && props.onClose()

  return (
    <div className="__unify__devtools_Dialog">
      <div style={{ display: props.open ? 'block' : 'none' }} onClick={backgroundClicked}></div>
      <Window {...props} />
    </div>
  )
}
