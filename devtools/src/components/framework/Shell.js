import React, { useState } from 'react'
import { Provider } from '@x/unify.react'
import { useDevtoolsContext } from '../../context'
import Evaluator from '../Evaluator'
import Query from '../Query'
import Results from '../Results'
import Scope from '../Scope'
import Recent from '../Recent'
import './Shell.css.js'

export default function Shell() {
  const [scope, setScope] = useState({})
  const { internalHost } = useDevtoolsContext()
  const scopeChanged = (key, value) => setScope({ ...scope, [key]: value })

  return !internalHost ? null : (
    <Provider host={internalHost}>
      <Evaluator
        render={({ evaluate, observable }) =>
          <div className="__unify__devtools_Shell">
            <Query evaluate={evaluate} scope={scope} />
            <main>
              <Results observable={observable} />
              <aside>
                <Scope scope={scope} onScopeChanged={scopeChanged} />
                <Recent />
              </aside>
            </main>
          </div>
        }/>
    </Provider>
  )
}
