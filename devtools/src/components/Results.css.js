import injectCss from '../injectCss'

injectCss('Results', `
.__unify__devtools_Results {
  padding: 5px;
}

.__unify__devtools_Results pre {
  margin: 0;
}
`)