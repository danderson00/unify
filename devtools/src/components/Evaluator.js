import React, { useState } from 'react'
import { compile } from '../queries'
import { publisher } from '@x/unify.react'
import { useDevtoolsContext } from '../context'

export default publisher(({ publish, render }) => {
  const [{ observable }, setObservable] = useState({})
  const { externalHost: host } = useDevtoolsContext()

  const evaluate = async (query, scope) => {
    if(!host) {
      throw new Error('Not connected')
    }

    if(observable && observable.disconnect) {
      observable.disconnect()
    }

    setObservable({})

    const trimmedScope = Object.keys(scope).reduce(
      (result, key) => ({
        ...result,
        ...(scope[key] && { [key]: scope[key] })
      }),
      {}
    )

    try {
      const request = query.text ? compile(query.text) : { type: 'vocabulary', ...query }
      setObservable({ observable: await host.subscribe(request, trimmedScope) })
      await publish({ topic: 'querySucceeded', ...query })
    } catch({ message }) {
      setObservable({ observable: { message } })
    }
  }

  return <>{render({ evaluate, observable })}</>
})
