import React from 'react'
import Json from 'react-json-view'
import { unwrap } from '@x/unify.react'
import './Results.css.js'

export default ({ observable }) => {
  const results = unwrap(observable)

  return <div className="__unify__devtools_Results">
    <Json theme="mocha" src={results || { message: 'Enter a query' }} style={{ background: 'inherit' }} />
  </div>
}
