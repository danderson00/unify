import React from 'react'
import { connect, publisher } from '@x/unify.react'
import './Recent.css.js'

export default publisher(connect({
  queries: o => o.topic('querySucceeded').all()
})(
  ({ publish, queries }) => {
    const selectQuery = (text, name, parameters) =>
      publish({ topic: 'querySelected', text, name, parameters })

    return (
      <div className="__unify__devtools_Recent">
        <h4>Recent Queries</h4>
        <ul>
          {queries.map((query, key) => (
            <li key={key} onClick={() => selectQuery(query.text, query.name, query.parameters)}>
              <code>{query.text || `${query.name}(${(query.parameters || []).map(x => JSON.stringify(x)).join(', ')})`}</code>
            </li>
          ))}
        </ul>
      </div>
    )
  }
))
