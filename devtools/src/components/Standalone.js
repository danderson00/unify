import React, { useState } from 'react'
import Connection from './Connection'

export default () => {
  const [host, setHost] = useState()

  return host ? null : <Connection onConnect={setHost} />
}