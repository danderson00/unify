import React, { useEffect, useState } from 'react'
import { ContextProvider } from '../context'
import Embed from './Embed'
import createHost from '@x/unify.host'

export default ({ vocabulary, log, host, standalone }) => {
  const [internalHost, setInternalHost] = useState()

  useEffect(() => {
    const newHost = createHost({ log: { level: 0 } })
    setInternalHost(newHost)
    return () => newHost.close()
  }, [])

  return (
    <ContextProvider
      vocabulary={vocabulary}
      log={log}
      externalHost={host}
      internalHost={internalHost}
    >
      <Embed
        maximised={standalone}
        open={standalone}
        allowResize={!standalone}
        allowClose={!standalone}
      />
    </ContextProvider>
  )
}
