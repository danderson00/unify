import React from 'react'
import { consumer } from '@x/unify.react'
import './Scope.css.js'

export default consumer(function Scope({ expressions, scope, onScopeChanged }) {
  return (
    <div className="__unify__devtools_Scope">
      <h4>Scope</h4>
      {expressions.scopes && expressions.scopes.flatMap((key, index) => (
        <div key={index}>
          <label>{key}</label>
          <input value={scope[key] || ''} onChange={e => onScopeChanged(key, e.target.value)} />
        </div>
      ))}
    </div>
  )
})
