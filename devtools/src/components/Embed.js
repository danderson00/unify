import React, { useState, useEffect } from 'react'
import Window from './framework/Window'
import Shell from './framework/Shell'
import { inspect } from '@xstate/inspect'

if(window.localStorage.getItem('unify.devtools.inspect') === 'true') {
  inspect({ iframe: false })
}

export default function Embed({ open: startOpen, maximised, allowResize, allowClose }) {
  const [open, setOpen] = useState(startOpen)

  useEffect(() => {
    window.addEventListener('keydown', e => {
      if(e.code === 'Backslash' && e.ctrlKey && e.shiftKey && !e.repeat) {
        setOpen(open => !open)
      }
    })
  }, [])

  return (
    <div>
      <Window
        header="unify devtools"
        allowResize={allowResize !== false}
        closeButton={allowClose !== false}
        maximised={maximised}
        machinesButton
        dark
        open={open}
        onClose={() => setOpen(false)}
      >
        <Shell />
      </Window>
    </div>
  )
}