const agent = require('../../src/agent')

const definition = (agentType = 'delay') => ({
  testFile: __dirname + '/testDefinition',
  host: 'ws://localhost:1234/',
  agentType,
  agentId: 1
})

test("agent status is initially uninitialised", () => {
  const api = agent()
  expect(api.status()()).toEqual({ status: 'uninitialised' })
})

test("calling start without first initialising throws error", async () => {
  const api = agent()
  await expect(api.start()).rejects.toMatchObject({ message: 'Must be ready' })
})

test("initialise throws if no test file is specified", async () => {
  const api = agent()
  await expect(api.initialise({ })).rejects.toMatchObject({})
})

test("initialising agent sets status to ready", async () => {
  const api = agent()
  await api.initialise(definition())
  expect(api.status()()).toEqual({ status: 'ready' })
})

test("status is set to running while test is running and ready when complete", async () => {
  const api = agent()
  await api.initialise(definition())
  const promise = api.start()
  expect(api.status()()).toEqual({ status: 'running' })
  await promise
  expect(api.status()()).toEqual({ status: 'ready' })
})

test("logs returns latest logs", async () => {
  const api = agent()
  await api.initialise(definition('logger'))
  await api.start()
  expect(api.logs()).toMatchObject([
    { type: 'testAgentStarted' },
    { message: 'test' },
    { type: 'testAgentFinished' }
  ])
  expect(api.logs()).toEqual([])
})