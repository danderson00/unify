module.exports = {
  agents: {
    delay: {
      handler: async ({ delay }) => {
        return delay()
      }
    },
    logger: {
      handler: async ({ log }) => {
        log('test')
      }
    }
  }
}