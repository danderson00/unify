const { subject } = require('@x/expressions')

module.exports = factory => {
  const currentStatus = subject({ initialValue: { status: 'uninitialised' } })

  let runner // eww...

  const ensureStatus = status => {
    if(currentStatus().status !== status) {
      throw new Error(`Must be ${status}`)
    }
  }

  const setStatus = status => currentStatus.publish({ status, ...(runner && runner.definition) })

  return {
    initialise: async definition => {
      ensureStatus('uninitialised')
      runner = factory.create(definition)
      setStatus('ready')
    },
    start: async runId => {
      ensureStatus('ready')
      setStatus('running')
      const result = await runner.start(runId)
      setStatus('ready')
      return result
    },
    stop: async () => {
      ensureStatus('running')
      await runner.stop()
      setStatus('ready')
    },
    status: () => currentStatus
  }
}