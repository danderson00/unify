const runnerFactory = require('./runnerFactory')
const api = require('./api')

module.exports = () => {
  const factory = runnerFactory()

  return api(factory)
}
