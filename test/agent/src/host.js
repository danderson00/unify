const hostModule = require('@x/socket/host')
const agent = require('.')
const WebSocket = require('ws')

const server = new WebSocket.Server({ port: 1233 })
hostModule({ server }).useApi(agent)
