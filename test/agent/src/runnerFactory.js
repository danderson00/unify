const runner = require('./runner')
const xlog = require('@x/log')
const publishingWriter = require('@x/unify.test.common/src/publishingWriter')
const { deserializeTest } = require('@x/unify.test.common/src/serialization')

module.exports = () => {
  return {
    create(definition) {
      const { agentType, agentId } = definition
      const logger = agentLogger.child({ source: 'agent', agentType, agentId })
      const options = deserializeTest(definition.options)

      const agentLogger = xlog({
        level: 'debug',
        writers: [
          publishingWriter({ url: `ws://${options.logHost || 'localhost'}:1235` }),
          xlog.writers.console({ level: 'info' })
        ],
        scope: { timestamp: () => Date.now() }
      })

      const testRunner = runner(
        options.test.agents[agentType].handler,
        options,
        logger,
        agentId
      )

      return {
        definition,
        async start() {
          logger.log({ type: 'testAgentStarted' })
          const result = await testRunner.start()
          logger.log({ type: 'testAgentFinished', result })
          return result
        },
        async stop() {
          logger.log({ type: 'testAgentStopped' })
          await testRunner.stop()
        }
      }
    }
  }
}
