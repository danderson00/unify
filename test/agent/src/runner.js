const WebSocket = require('ws')
const createConsumer = require('@x/unify/consumer')
const createHost = require('@x/unify/host')
const middleware = require('@x/unify.test.common/src/middleware')
const { delay, uidFactory } = require('@x/unify.test.common/src/utilities')
const { v4: uuid } = require('uuid')

module.exports = (test, { appHost } = {}, logger, agentId) => {
  const uid = uidFactory()

  const socketFactory = () => new WebSocket(`ws://${appHost || 'localhost'}:1234`)
  const connect = (connectionId = uid()) => (
    createConsumer({ socketFactory })
      .use(middleware(logger.child({ source: 'agent.middleware', connectionId })))
      .connect()
  )
  const connectMany = (count, callback) => (
    Promise.all(
      Array(count).fill().map(() => connect().then(callback))
    )
  )

  const defaultHostOptions = {
    port: 1234,
    storage: {
      client: 'pg',
      connection: {
        host: 'localhost',
        user: 'postgres',
        password: 'abc123',
        database: 'hordesauce'
      }
    }
  }
  const startHost = userOptions => (
    createHost({ ...defaultHostOptions, ...userOptions })
      .then(host => host.use(middleware(logger.child({ source: 'host.middleware' }))))
  )

  const defaultLoggerOptions = {
    port: 1235,
    // scopes: ['timestamp', 'source', 'agentId', 'agentType', 'connectionId', 'runId', 'operation', 'type', 'value'],
    // storage: { client: 'sqlite', connection: { filename: 'results.db' } }
    storage: {
      client: 'pg',
      connection: {
        host : 'localhost',
        user : 'postgres',
        password : 'abc123',
        database : 'results'
      }
    },
  }
  const startLogger = userOptions => (
    createHost({ ...defaultLoggerOptions, ...userOptions })
  )

  return {
    start: runId => {
      const userLogger = logger.child({ runId, type: 'user' })
      return test({
        agentId,
        startHost,
        startLogger,
        connect,
        connectMany,
        delay,
        uuid,
        log: userLogger.debug,
        info: userLogger.info
      })
    },
    stop() {}
  }
}