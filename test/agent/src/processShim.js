const hostModule = require('@x/socket/host')
const agent = require('.')

hostModule({ socket: process }).useApi(agent())

process.stdin.resume()
