const host = require('@x/unify')
const vocabulary = require('./vocabulary')

host({
  port: 1234,
  scopes: ['questionId', 'responseId', 'clientId'],
  vocabulary,
  // yeah, baby!
  // throttle: false,
  // makes fuck all difference
  throttle: { timeout: 200 },
  storage: {
    client: 'mysql',
    connection: {
      host : 'localhost',
      user : 'root',
      password : 'abc123',
      database : 'test'
    }
  }
  // storage: { client: 'sqlite', filename: 'test.db' }
})
