module.exports = {
  question: o => o.compose(
    o => o.topic('question').accumulate(),
    o => o.groupBy('responseId',
      o => o,
      o => o.topic('response').where(x => x.deleted)
    ).count(),
    (details, responseCount) => ({ ...details, responseCount })
  ),
  questions: o => o.groupBy('questionId', o => o.question()),
  selectedQuestionId: o => o.topic('questionSelected').select('questionId'),
  activeQuestionId: o => o.topic('questionActivated').select('questionId'),

  crowdsourceResponse: o => o.compose(
    o => o.topic('response').accumulate(),
    o => o.topic('score').groupBy('clientId').sum('value'),
    (details, score) => ({ ...details, score })
  ),
  crowdsource: o => o.compose(
    o => o.question(),
    o => o.groupBy('responseId',
      o => o.crowdsourceResponse(),
      o => o.topic('response').where(x => x.deleted)
    ),
    o => o.groupBy('responseId',
      o => o.topic('score').groupBy('clientId').sum('value').map(sum => ({ sum }))
    )
      .orderByDescending('sum')
      .mapAll(x => x.length > 0 ? x[0] && x[0].sum : 0),
    (question, responses, highestScore) => ({
      ...question,
      responses: responses.map(response => ({ ...response, max: highestScore, min: highestScore }))
    })
  ),
  crowdsourceScore: o => o.topic('score').select('value')
}