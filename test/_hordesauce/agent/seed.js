module.exports = async function(publish) {
  await publish({ topic: 'question', questionId: '1', text: 'Question 1' })
  for(let i = 1; i <= 20; i++) {
    await publish({ topic: 'response', questionId: '1', responseId: i.toString(), text: `Response ${i}` })
  }
}
