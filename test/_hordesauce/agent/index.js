const consumer = require('@x/unify/consumer')
const seed = require('./seed')
const test = require('./test')
const WebSocket = require('ws')

require('babel-polyfill')

module.exports = async function(id, log) {
  const api = await consumer({
    // socketFactory: () => new WebSocket('ws://cordeltaconference.com:1234/')
    socketFactory: () => new WebSocket('ws://localhost:1234/')
  }).connect()

  return {
    start: () => test(id, log, api),
    finish: () => { },
    seed: () => seed(api.publish),
    log: () => log.log
  }
}

