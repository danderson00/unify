const delay = ms => new Promise(r => setTimeout(r, Math.random() * (ms || 1000)))

module.exports = async (id, log, { publish, subscribe }) => {
  await delay(2000)

  console.log(`Starting test ${id}`)

  const question = await log.time('crowdsource', subscribe({ type: 'vocabulary', name: 'crowdsource' }, { questionId: '1' }))

  const scores = await Promise.all(Array(20).fill(null).map(
    (x, index) => log.time(`crowdsourceScore - ${index + 1}`, subscribe(
      { type: 'vocabulary', name: 'crowdsourceScore' },
      { questionId: '1', responseId: (index + 1).toString(), clientId: id }
    ))
  ))

  const waitForScore = (responseId, score, o) => new Promise(resolve => {
    const { unsubscribe } = o.subscribe(() => {
      unsubscribe()
      resolve()
    })

    publish({ topic: 'score', questionId: '1', responseId, clientId: id, value: score })
  })

  await scores.reduce(
    (previous, next, index) => previous.then(delay).then(
      () => log.time(`waitForScore - ${index + 1}`, waitForScore((index + 1).toString(), 1, next))
    ),
    Promise.resolve()
  )
}