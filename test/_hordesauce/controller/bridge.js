const { fork } = require('child_process')

module.exports = () => {
  const child = fork('host')

  return {
    close: () => child.kill()
  }
}
