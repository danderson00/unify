const { isObservable } = require('@x/expressions')

module.exports = () => {
  const logs = []

  return {
    logs,
    aggregate: () => logs[0].log.map(({ operation }) => ({
      operation,
      time: operationPropertyAggregates(logs, operation, 'time'),
      count: operationPropertyAggregates(logs, operation, 'count'),
      bytes: operationPropertyAggregates(logs, operation, 'bytes'),
    })),
    ignore: () => ({ time: (operation, target) => target }),
    create: id => {
      const log = []
      logs.push({ id, log })

      return {
        log,
        time: (operation, target) => {
          const start = Date.now()
          const record = { operation, id }

          target.then(result => {
            if (isObservable(result)) {
              record.count = 0
              record.bytes = 0

              result.subscribe(value => {
                // console.log(value)
                record.bytes += JSON.stringify(value).length
                record.count++
              })
            }
            return result
          })

          target.finally(() => {
            record.time = Date.now() - start
            log.push(record)
          })

          return target
        }
      }
    }
  }
}

function operationPropertyAggregates(logs, operation, property) {
  const values = logs.map(log => log.log.find(x => x.operation === operation)[property])
  return {
    min: Math.min.apply(null, values),
    max: Math.max.apply(null, values),
    average: values.reduce((sum, value) => sum + value, 0) / values.length,
    total: values.reduce((sum, value) => sum + value, 0)
  }
}