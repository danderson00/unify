const hostBridge = require('./bridge')
const agent = require('../agent')
const logger = require('./logger')

const bridge = hostBridge()
const log = logger()

const NUMBER_OF_AGENTS = 100

agent('seeder', log.ignore())
  .then(async seeder => {
    await seeder.seed()

    await Promise.all(Array(NUMBER_OF_AGENTS).fill(null).map(
      (x, index) => agent(index + 1, log.create(index + 1))
        .then(test => test.start())
    ))

    // console.log('Waiting before logging...')
    // await new Promise(r => setTimeout(r, 1000))

    console.log(log.aggregate())
  })
  .catch(error => console.error(error))
  .finally(() => {
    bridge.close()
    process.exit() // ?
  })
