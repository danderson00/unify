import React from 'react'
import { useObservable } from '@x/unify.react'

export default ({ agents: { o } }) => {
  const agents = useObservable(o)

  const colorFor = {
    ready: '#cc7722',
    running: '#44cc44'
  }

  const styleFor = status => ({
    width: 16,
    height: 16,
    display: 'inline-flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colorFor[status],
    color: '#eeeeee',
    marginRight: 2
  })

  return <>
    {agents.map(({ agentId, agentType, status }) =>
      <div
        key={agentId}
        style={styleFor(status)}
      >
        {agentId}
      </div>
    )}
  </>
}