import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { Provider } from '@x/unify.react'
import unify from '@x/unify'

unify({ url: 'ws://localhost:1235' }).connect().then(host =>
  ReactDOM.render(
    <Provider host={host}>
      <App />
    </Provider>,
    document.getElementById('root')
  )
)
