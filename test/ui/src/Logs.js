import React from 'react'
import { connect } from '@x/unify.react'

export default connect({
  logs: o => o.topic('log').where(x => x.level <= 3).takeLast(30).orderByDescending('timestamp')
})(({ logs }) => {
  return (
    <div>
      <ul>
        {logs.map(entry =>
          <li key={entry.timestamp}>
            {JSON.stringify(entry)}
          </li>
        )}
      </ul>
    </div>
  )
})
