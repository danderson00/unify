import React, { useEffect, useState } from 'react'
import { consumer } from '@x/unify.react'
import Agents from './Agents'

export default consumer(({ host }) => {
  const [agents, setAgents] = useState()

  useEffect(() => {
    host.agents().then(agents => setAgents({ o: agents }))
  }, [host])

  return (
    <div>
      {agents && <Agents agents={agents} />}
      <button onClick={() => host.run()}>Run</button>
    </div>
  )
})
