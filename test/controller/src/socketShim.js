const createController = require('.')
const hostModule = require('@x/socket/host')
const { Server } = require('ws')
const { resolve } = require('path')

createController({ testFile: resolve(__dirname, '../tests/pulse.js') }).then(controller =>
  hostModule({ server: new Server({ port: 1233 }) })
    .useApi(controller)
)
