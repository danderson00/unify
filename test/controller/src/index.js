const agentFactory = require('./agentFactory')
const publishingWriter = require('@x/unify.test.common/src/publishingWriter')
const xlog = require('@x/log')
const { mergeArray } = require('@x/expressions')
const { v4: uuid } = require('uuid')

module.exports = async (userOptions = {}) => {
  const options = parseOptions(userOptions)

  const logger = xlog({
    level: 'debug',
    writers: [
      publishingWriter({ url: `ws://${options.logHost}:1235` }),
      xlog.writers.console({ level: 'info' })
    ],
    scope: { timestamp: () => Date.now() }
  })


  const factory = agentFactory(options, logger)
  const agents = await factory.create()

  const statuses = await Promise.all(agents.map(({ status }) => status()))

  statuses.forEach((o, i) => {
    const { agentType, agentId } = agents[i].definition
    o.subscribe(({ status }) => logger.info({
      message: `${agentType} #${agentId} changed status to ${status}`,
      agentId,
      agentType
    }))
  })

  return {
    run: () => Promise.all(agents.map(x => x.start(uuid()))),
    stop: () => agents.forEach(x => x.kill()), // :-\
    agents: async () => mergeArray(...statuses)
  }
}

const parseOptions = options => {
  const result = {}

  if(!options.testFile) {
    throw new Error('No test file specified')
  }

  result.test = require(options.testFile)
  result.testFile = options.testFile

  if(options.hosts) {
    if(options.hosts.length < 3) {
      throw new Error('You must provide at least 3 hosts')
    }

    result.mode = 'socket'
    result.appHost = options.hosts[0]
    result.logHost = options.hosts[1]
    result.hosts = options.hosts
  } else {
    result.mode = 'process'
    result.appHost = 'localhost:1234'
    result.logHost = 'localhost:1235'
  }

  return result
}