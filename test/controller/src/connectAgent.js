const consumerModule = require('@x/socket/consumer')
const { serializeTest } = require('@x/unify.test.common/src/serialization')
const { fork } = require('child_process')

module.exports = async (agentOptions, definition = {}) => {
  const { options, agentType, agentId } = definition
  const agentConfig = options.test.agents[agentType]

  const api = await consumerModule(socketOptions(agentOptions, agentConfig)).connect()
  await api.initialise({ options: serializeTest(options), agentType, agentId })

  return {
    start: runId => api.start(runId),
    stop: () => api.stop(),
    kill: () => { /* childProcess.kill() */ },
    status: () => api.status(),
    definition
  }
}

const socketOptions = ({ mode, host }, agentConfig) => {
  switch(mode) {
    case 'socket':
      return { url: `ws://${host}` }

    case 'process':
      const childProcess = fork(require.resolve('@x/unify.test.agent/src/processShim.js'), agentConfig.processOptions)
      childProcess.on('exit', e => { /* ??? */ }) // exit for unknown reason *should be* an error
      return { socket: childProcess }

    default:
      throw new Error('Mode must be socket or process')
  }
}