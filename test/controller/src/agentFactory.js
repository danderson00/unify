const { uidFactory, pluralise } = require('@x/unify.test.common/src/utilities')
const connectAgent = require('./connectAgent')

module.exports = (options, logger) => {
  const { hosts, test, mode } = options
  const uid = uidFactory()

  const createAgents = () => {
    const availableHosts = [...(hosts || [])]

    const agentOptions = () => {
      if (mode === 'socket' ) {
        if(availableHosts.length === 0) {
          throw new Error('No available hosts')
        }
        return { mode: 'socket', host: availableHosts.splice(0, 1)[0] }
      } else {
        return { mode: 'process' }
      }
    }

    return Object.keys(test.agents).reduce(
      (agents, agentType) => {
        const count = test.agents[agentType].count || 1

        logger.info({ message: `Creating ${count} ${agentType} ${pluralise('agent', count)}` })
        return [
          ...agents,
          ...Array(count).fill().map(
            () => connectAgent(agentOptions(), { options, agentType, agentId: uid() })
          )
        ]
      },
      []
    )
  }

  return {
    create: async () => {
      const agents = await Promise.all(createAgents())
      logger.info({ message: `Created ${agents.length} ${pluralise('agent', agents.length)}` })
      return agents
    }
  }
}

