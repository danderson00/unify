select
  received.timestamp - published.timestamp as elapsed,
  *
from (
  select
    scope::json->>'responseId' as responseId,
    scope::json->>'clientId' as clientId,
    timestamp
  from messageValues
  where type = 'update'
    and agentType = 'load'
    and vocabularyName = 'crowdsourceScore'
) received left join (
  select
    fullMessage::json->'message'->>'responseId' responseId,
    fullMessage::json->'message'->>'clientId' clientId,
    timestamp
  from messageValues
  where operation = 'publish'
    and type = 'start'
    and agentType = 'load'
    and topic = 'score'
) published
  on received.responseId = published.responseId
    and received.clientId = published.clientId
    
order by published.timestamp
    
--delete from messages
--select count(*) from messages