drop view if exists messageValues;

create view messageValues as
select
  content::json->'message'->>'runId' as runId,
  content::json->'message'->>'source' as source,
  content::json->'message'->>'agentId' as agentId,
  content::json->'message'->>'agentType' as agentType,
  content::json->'message'->>'connectionId' as connectionId,
  content::json->'message'->>'operation' as operation,
  (content::json->'message'->>'timestamp')::bigint as timestamp,
  content::json->'message'->>'type' as type,
  content::json->'message'->>'value' as value,
  content::json->'message'->>'message' as message,
  content::json->'message'->'message'->>'topic' as topic,
  content::json->'message'->>'scope' as scope,
  content::json->'message'->'definition'->>'name' as vocabularyName,
  content::json->'message'->>'options' as options,
  content::json->'message' as fullMessage
from messages;
