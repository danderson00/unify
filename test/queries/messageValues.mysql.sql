drop view if exists messageValues;

create view messageValues as
select
  json_extract(content, '$.message.runId') as runId,
  json_extract(content, '$.message.source') as source,
  json_extract(content, '$.message.agentId') as agentId,
  json_extract(content, '$.message.agentType') as agentType,
  json_extract(content, '$.message.connectionId') as connectionId,
  json_extract(content, '$.message.operation') as operation,
  json_extract(content, '$.message.timestamp') as timestamp,
  json_extract(content, '$.message.type') as type,
  json_extract(content, '$.message.value') as value,
  json_extract(content, '$.message.message') as message,
  json_extract(content, '$.message.message.topic') as topic,
  json_extract(content, '$.message.scope') as scope,
  json_extract(content, '$.message.definition.name') as vocabularyName,
  json_extract(content, '$.message.options') as options,
  json_extract(content, '$.message') as fullMessage
from messages;
