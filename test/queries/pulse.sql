select avg(ms) from (
select *, received - sent as ms
from (
  select value, timestamp as received from messageValues
  where agentType = 'test'
) as test
  inner join (
    select value, timestamp as sent from messageValues
    where agentType = 'publisher'
  ) as publisher on test.value = publisher.value
)
  
--delete from messages