const controller = require('@x/unify.test.controller')
const { resolve } = require('path')

module.exports = {
  port: 1235,
  storage: { client: 'sqlite', connection: { filename: 'results.db' } },
  // extendHost: async host => {
  //   host.useApi(await controller({ testFile: resolve(__dirname, 'src/tests/pulse.js') }))
  // }
}