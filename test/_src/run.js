const controller = require('@x/unify.test.controller')
const { resolve } = require('path')

controller({ testFile: resolve(__dirname, '../tests/hordesauce') })
  .then(api => api.run())
  .catch(error => console.log(error))
