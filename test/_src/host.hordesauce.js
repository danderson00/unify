const host = require('@x/unify')
const vocabulary = require('../../hordesauce/src/vocabulary')

host({
  port: 1234,
  vocabulary,
  scopes: ['questionSetId', 'questionSetCode', 'questionId', 'responseId', 'clientId', 'userId'],
  ownedScopes: ['questionSetId'],
  socket: { throttle: { timeout: 200 } },
  storage: {
    client: 'pg',
    connection: {
      host : 'localhost',
      user : 'postgres',
      password : 'abc123',
      database : 'hordesauce'
    }
  }
})
