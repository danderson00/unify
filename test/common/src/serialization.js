const FUNCTION_HEADER = '__function{'
const FUNCTION_FOOTER = '}__'

const serializeFunction = (key, value) => typeof value === 'function'
  ? `${FUNCTION_HEADER}${value.toString()}${FUNCTION_FOOTER}`
  : value
const deserializeFunction = (key, value) => (typeof value === 'string' && value.startsWith(FUNCTION_HEADER) && value.endsWith(FUNCTION_FOOTER))
  ? (new Function(`return ${value.substring(FUNCTION_HEADER.length, value.length - FUNCTION_FOOTER.length)}`))()
  : value

module.exports = {
  serializeTest: test => JSON.stringify(test, serializeFunction),
  deserializeTest: payload => JSON.parse(payload, deserializeFunction)
}