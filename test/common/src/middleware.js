const { unwrap, isObservable } = require('@x/expressions')

module.exports = logger => {
  const uid = ((id = 0) => () => ++id)()

  const time = (operation, argNames) => async ({ next }, ...args) => {
    const log = logger.child({ operation, operationId: uid() }).debug

    const argObject = argNames.reduce(
      (result, name, index) => ({ ...result, [name]: args[index] }),
      {}
    )

    log({ type: 'start', ...argObject })
    const result = await next()
    log({ type: 'end', ...argObject })

    if(isObservable(result)) {
      const originalDisconnect = result.disconnect
      result.disconnect = () => {
        log({ type: 'disconnect', ...argObject })
        originalDisconnect && originalDisconnect()
      }

      result.subscribe(() => log({ type: 'update', ...argObject }))
    }

    return result
  }

  return {
    publish: time('publish', ['message']),
    subscribe: time('subscribe', ['definition', 'scope', 'options'])
  }
}