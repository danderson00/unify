module.exports = () => {
  let logs = []
  return {
    writer: () => entry => logs.push(entry),
    logs: () => {
      const cache = logs
      logs = []
      return cache
    }
  }
}