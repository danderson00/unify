module.exports = {
  delay: ms => new Promise(resolve => setTimeout(resolve, ms)),
  uidFactory: (startAt = 1) => (id => () => id++)(startAt),
  pluralise: (word, count) => count === 1 ? word : (word + 's')
}