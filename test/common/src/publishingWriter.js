const consumerModule = require('@x/unify/consumer')
const WebSocket = require('ws')

module.exports = ({ url }) => {
  const connectToHost = consumerModule({ socketFactory: () => new WebSocket(url) }).connect()

  return options => entry => (
    connectToHost.then(host => {
      host.publish({ topic: 'log', ...entry })
        .catch(error => console.error('Unable to publish to log server', error))
    })
  )
}
