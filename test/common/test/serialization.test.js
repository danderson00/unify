const { serializeTest, deserializeTest } = require('../src/serialization')

test("functions can be serialized and deserialized", () => {
  const test = {
    agents: {
      host: {
        handler: ({ startHost }) => startHost({
          vocabulary: { pulse: o => o }
        })
      }
    }
  }

  const rehydrated = deserializeTest(serializeTest(test))

  const startHost  = jest.fn()
  rehydrated.agents.host.handler(({ startHost }))
  expect(startHost.mock.calls[0][0].vocabulary.pulse(1)).toBe(1)
})