const middlewareModule = require('@x/unify.test.common/src/middleware')
const { subject } = require('@x/expressions')

const mockLogger = () => {
  const log = jest.fn()
  const child = jest.fn(() => ({ debug: log }))
  const logger = { child }
  return { log, child, logger }
}

const delay = ms => new Promise(r => setTimeout(r, ms))

test("middleware creates child logger with unique id for each operation", async () => {
  const { child, logger } = mockLogger()
  const middleware = middlewareModule(logger)
  await middleware.publish({ next: () => {} })
  expect(child.mock.calls).toEqual([[{ operation: 'publish', operationId: 1 }]])
})

test("middleware logs start and end times for publish operations", async () => {
  const { log, logger } = mockLogger()
  const middleware = middlewareModule(logger)
  await middleware.publish({ next: () => delay(5) }, { topic: 'test' })
  expect(log.mock.calls.length).toBe(2)
  expect(log.mock.calls).toMatchObject([
    [{ type: 'start', message: { topic: 'test' } }],
    [{ type: 'end', message: { topic: 'test' } }]
  ])
  // expect(log.mock.calls[1][0].timestamp - log.mock.calls[0][0].timestamp).toBeLessThan(10)
})

test("middleware logs update times for returned observables", async () => {
  const { log, logger } = mockLogger()
  const middleware = middlewareModule(logger)
  const source = subject()
  await middleware.publish({ next: () => source }, { topic: 'test' })
  source.publish({ topic: 'test2' })
  expect(log.mock.calls.length).toBe(3)
  expect(log.mock.calls[2]).toMatchObject([
    { type: 'update', message: { topic: 'test' } }
  ])
  // expect(log.mock.calls[2][0].timestamp).not.toBeUndefined()
})

test("middleware logs disconnect times for returned observables", async () => {
  const { log, logger } = mockLogger()
  const middleware = middlewareModule(logger)
  const result = await middleware.publish({ next: () => subject() }, { topic: 'test' })
  result.disconnect()
  expect(log.mock.calls.length).toBe(3)
  expect(log.mock.calls[2]).toMatchObject([{ type: 'disconnect' }])
  // expect(log.mock.calls[2][0].timestamp).not.toBeUndefined()
})