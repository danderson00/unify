module.exports = {
  agents: {
    host: {
      handler: ({ startHost }) => startHost({
        vocabulary: {
          pulse: o => o
        }
      })
    },
    // logger: {
    //   handler: ({ startLogger }) => startLogger()
    // },
    publisher: {
      handler: async ({ connect, delay, log, info }) => {
        let value = 0

        const host = await connect()

        const sendPulse = async () => {
          value++
          log({ message: 'Pulsing...', value })
          host.publish({ topic: 'pulse', value })
          if (value < 50) {
            await delay(100)
            await sendPulse()
          }
        }

        await delay(1000)
        info('Starting test...')
        await sendPulse()
      }
    },
    load: {
      count: 3,
      handler: async ({ connectMany }) => {
        await connectMany(1, host => host.subscribe({ type: 'vocabulary', name: 'pulse' }))
      }
    },
    test: {
      handler: async ({ delay, connect, log }) => {
        await delay(800)
        const host = await connect('test')
        const o = await host.subscribe({ type: 'vocabulary', name: 'pulse' })
        o.subscribe(({ value }) => log({ message: 'Received', value }))
      }
    }
  }
}

