module.exports = {
  agents: {
    host: {
      // processOptions: { execArgv: ['--prof'] },
      handler: ({ startHost }) => startHost({
        vocabulary: ({ scoped, userScope, ownerOnly, scopeOwnerOnly }) => ({
          question: [scoped('questionSetId', 'questionId'),
            o => o.assign({
              '...details': o => o.topic('question').accumulate(),
              'responseCount': o => o.groupBy('responseId',
                undefined,
                o => o.topic('response').where(x => x.deleted)
              ).count()
            })
          ],
          questions: [scoped('questionSetId'), o => o.groupBy('questionId', o => o.question())],
          questionSet: [scoped('questionSetId'), o => o.assign({
            '...details': o => o.topic('questionSet').accumulate(),
            'code': o => o.topic('questionSetCode').select('questionSetCode'),
            'questionCount': o => o.groupBy('questionId').count()
          })],
          questionSets: [userScope, o => o.groupBy('questionSetId', o => o.questionSet())],

          selectedQuestionSetId: [userScope, o => o.topic('questionSetSelected').select('questionSetId')],
          selectedQuestionId: [scoped('questionSetId'), o => o.topic('questionSelected').select('questionId')],
          activeQuestionId: [scoped('questionSetId'), o => o.topic('questionActivated').select('questionId')],
          questionSetIdFromCode: [scoped('questionSetCode'), o => o.topic('questionSetCode').select('questionSetId')],

          crowdsourceResponse: o => o.assign({
            '...details': o => o.topic('response').accumulate(),
            'score': o => o.topic('score').groupBy('clientId').sum('value')
          }),
          crowdsource: [scoped('questionSetId', 'questionId'), o => o.compose(
            o => o.question(),
            o => o.groupBy('responseId',
              o => o.crowdsourceResponse(),
              o => o.topic('response').where(x => x.deleted)
            ),
            o => o.groupBy('responseId',
              o => o.topic('score').groupBy('clientId').sum('value').map(sum => ({ sum }))
            )
              .orderByDescending('sum')
              .mapAll(x => x.length > 0 ? x[0] && x[0].sum : 0),
            (question, responses, highestScore) => ({
              ...question,
              responses: responses.map(response => ({ ...response, max: highestScore, min: highestScore }))
            })
          )],
          crowdsourceScore: [scoped('questionSetId', 'questionId', 'responseId', 'clientId'),
            o => o.topic('score').select('value')
          ],

          multipleChoice: [scoped('questionSetId', 'questionId'), o => o.compose(
            o => o.question(),
            o => o.groupBy('responseId',
              o => o.topic('response').accumulate(),
              o => o.topic('response').where(x => x.deleted)
            ),
            o => o.topic('score').groupBy('clientId')
              .mapAll(x => x.reduce(
                (responses, { responseId }) => ({
                  ...responses,
                  [responseId]: (responses[responseId] || 0) + 1,
                }),
                {}
              )),
            o => o.topic('score').groupBy('clientId').count(),

            (question, responses, scores, totalResponses) => ({
              ...question,
              responses: responses.map(response => ({
                ...response,
                score: (scores && scores[response.responseId]) || 0,
                max: totalResponses
              }))
            })
          )],
          multipleChoiceSelectedResponse: [
            scoped('questionSetId', 'questionId', 'clientId'),
            o => o.topic('score').select('responseId')
          ],

          ratingsResponse: o => o.compose(
            o => o.topic('response').accumulate(),
            o => o.topic('score').groupBy('clientId').average('value'),
            (details, score) => ({ ...details, score, max: 5 })
          ),
          ratings: [scoped('questionSetId', 'questionId'), o => o.assign({
            '...question': o => o.question(),
            'responses': o => o.groupBy('responseId',
              o => o.ratingsResponse(),
              o => o.topic('response').where(x => x.deleted)
            )
          })],
          ratingsScore: [scoped('questionId'), o => o.topic('score').select('value')]
        }),
        scopes: ['questionSetId', 'questionSetCode', 'questionId', 'responseId', 'clientId', 'userId'],
        ownedScopes: ['questionSetId'],
        socket: { throttle: { timeout: 200 } },
        storage: {
          client: 'pg',
          connection: {
            host: 'localhost',
            user: 'postgres',
            password: 'abc123',
            database: 'hordesauce'
          }
        }
      })
    },
    logger: {
      handler: ({ startLogger }) => startLogger()
    },
    load: {
      count: 1,
      handler: async ({ connect, connectMany, delay, agentId, info, uuid }) => {
        const host = await connect()
        const questionSetId = await createTest(host.publish)

        await connectMany(5, async host => {
          const clientId = uuid()
          const { activeQuestionId, question, crowdsource, responses } = await subscribe(host.subscribe, questionSetId, clientId)
          await delay(500)
          info(`Agent ${agentId} starting publish`)
          await publishScores(host.publish, delay, crowdsource().responses, clientId)

          ;[activeQuestionId, question, crowdsource, ...responses].forEach(o => o.disconnect())
        })

        function publishScores(publish, delay, responses, clientId) {
          return responses.reduce(
            (previous, { questionSetId, questionId, responseId }) => (
              previous
                .then(() => { publish({ topic: 'score', questionSetId, questionId, responseId, value: 1, clientId }) })
                .then(() => delay(500))
            ),
            Promise.resolve()
          )
        }

        async function subscribe(subscribe, questionSetId, clientId) {
          const activeQuestionId = await subscribe({ type: 'vocabulary', name: 'activeQuestionId' }, { questionSetId })
          const questionId = activeQuestionId()
          const question = await subscribe({ type: 'vocabulary', name: 'question' }, { questionSetId, questionId })
          const crowdsource = await subscribe({ type: 'vocabulary', name: 'crowdsource' }, { questionSetId, questionId })
          const responses = await(Promise.all(crowdsource().responses.map(({ responseId }) =>
            subscribe({ type: 'vocabulary', name: 'crowdsourceScore' }, { questionSetId, questionId, responseId, clientId })
          )))

          return { activeQuestionId, question, crowdsource, responses }
        }

        function createTest(publish) {
          const userId = "user1"

          return createQuestionSet(1)

          async function createQuestionSet(setNumber) {
            const questionSetId = uuid()
            const questionSetCode = Array(4).fill(setNumber.toString()).join('').substring(0, 4)

            await publish({ topic: 'questionSet', questionSetId, name: `Question Set ${setNumber}`, userId })
            await publish({ topic: 'questionSetCode', questionSetId, userId, questionSetCode })

            const questionId = await createQuestion(1, 'crowdsource')
            await publish({ topic: 'questionActivated', questionSetId, questionId })

            return questionSetId

            async function createQuestion(questionNumber, type) {
              const questionId = uuid()
              await publish({
                topic: 'question',
                questionSetId,
                questionId,
                text: `Question ${questionNumber} (${type})`,
                type,
                explanatoryText: "Tap the plus and minus buttons to record your vote against each item. You can change or withdraw your vote at any time while voting is active.",
                showScores: true,
                allowClientAdd: true,
                userId
              })

              await Promise.all(Array(5).fill().map((x, i) => createResponse(i)))

              return questionId

              async function createResponse(responseNumber) {
                const responseId = uuid()
                await publish({
                  topic: 'response',
                  questionSetId,
                  questionId,
                  responseId,
                  respondedAt: Date.now(),
                  text: `Response ${responseNumber}`,
                  userId
                })
              }
            }
          }
        }
      }
    }
  }
}