# Unify

Complete State Management Solution

## What is it?

At its core, Unify is an event sourcing database and state machine engine. Events are published to the
message bus and intelligently routed through *stream expressions* heavily inspired by
[`ReactiveX`](http://reactivex.io/) and *statecharts* built on the popular [`xstate`](https://xstate.js.org/) library. 
User interface bindings allow simple, component level access to the event store through intuitive "scoping" of 
expressions and statecharts.

Unify completely abstracts away the client/server paradigm, allowing developers to focus on application 
business logic and user interface. 

It enables:

- A single, central source of truth for all business logic
- Vastly less complexity and code
- All data secured and updated in real-time
- Visually modelled workflows and processes

by providing: 

- a lightweight "run anywhere" host and low configuration consumer
- a simple, intuitive core expression language focused on extensibility and reuse
- workflows and processes built on the [`xstate`](https://xstate.js.org/) library
- higher order components and hooks for [`React`](https://reactjs.org/) (more frameworks coming soon)
- out of the box authentication and binary storage providers

## Documentation

For newcomers, both reactive programming noobs and Rx afficionados, we recommend running through the
[getting started guide](https://danderson00.gitlab.io/unify/docs#/guides/getting-started/1-introduction.md) (video 
tutorial 
coming soon!)

To get up and running quickly, follow the instructions in the 
[bundle documentation](https://danderson00.gitlab.io/unify/docs#/unify/bundle/).

API documentation is available on the [documentation site](https://danderson00.gitlab.io/unify/docs).

## Packages

|||||
|---|---|---|---|
|unify|Framework bundle|[<img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-gray-rgb.svg" alt="gitlab" height="24" />](https://gitlab.com/danderson00/unify)|[<img src="https://raw.githubusercontent.com/npm/logos/master/npm%20logo/npm-logo-red.svg" alt="npm" height="18" />](https://www.npmjs.com/package/unify)|
|unify.devtools|Development tools (query runner)|[<img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-gray-rgb.svg" alt="gitlab" height="24" />](https://gitlab.com/danderson00/unify)|[<img src="https://raw.githubusercontent.com/npm/logos/master/npm%20logo/npm-logo-red.svg" alt="npm" height="18" />](https://www.npmjs.com/package/unify.devtools)|
|unify.fsm|State machine tools|[<img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-gray-rgb.svg" alt="gitlab" height="24" />](https://gitlab.com/danderson00/unify)|[<img src="https://raw.githubusercontent.com/npm/logos/master/npm%20logo/npm-logo-red.svg" alt="npm" height="18" />](https://www.npmjs.com/package/unify.fsm)|
|unify.host|"Run anywhere" host process|[<img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-gray-rgb.svg" alt="gitlab" height="24" />](https://gitlab.com/danderson00/unify)|[<img src="https://raw.githubusercontent.com/npm/logos/master/npm%20logo/npm-logo-red.svg" alt="npm" height="18" />](https://www.npmjs.com/package/unify.host)|
|unify.react|Higher order components and hooks for `React`|[<img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-gray-rgb.svg" alt="gitlab" height="24" />](https://gitlab.com/danderson00/unify)|[<img src="https://raw.githubusercontent.com/npm/logos/master/npm%20logo/npm-logo-red.svg" alt="npm" height="18" />](https://www.npmjs.com/package/unify.react)|
|@x/expressions|Core expression language and serialization mechanisms|[<img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-gray-rgb.svg" alt="gitlab" height="24" />](https://gitlab.com/danderson00/expressions)|[<img src="https://raw.githubusercontent.com/npm/logos/master/npm%20logo/npm-logo-red.svg" alt="npm" height="18" />](https://www.npmjs.com/package/@x/expressions)|
|@x/socket|Reliable observable socket framework|[<img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-gray-rgb.svg" alt="gitlab" height="24" />](https://gitlab.com/danderson00/socket)|[<img src="https://raw.githubusercontent.com/npm/logos/master/npm%20logo/npm-logo-red.svg" alt="npm" height="18" />](https://www.npmjs.com/package/@x/socket)|
|@x/socket.auth|Authentication middleware for `@x/socket`|[<img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-gray-rgb.svg" alt="gitlab" height="24" />](https://gitlab.com/danderson00/socket-auth)|[<img src="https://raw.githubusercontent.com/npm/logos/master/npm%20logo/npm-logo-red.svg" alt="npm" height="18" />](https://www.npmjs.com/package/@x/socket.auth)|
|@x/socket.unify|Unify optimizations as middleware for `@x/socket`|[<img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-gray-rgb.svg" alt="gitlab" height="24" />](https://gitlab.com/danderson00/socket-unify)|[<img src="https://raw.githubusercontent.com/npm/logos/master/npm%20logo/npm-logo-red.svg" alt="npm" height="18" />](https://www.npmjs.com/package/@x/socket.unify)|
