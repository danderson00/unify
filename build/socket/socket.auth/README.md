# @x/socket.auth

Authentication feature for @x/socket

## API

Adds the following functions to the @x/socket API surface:

### authenticate(payload, persistent)

`payload` must contain a property called `provider` that is one of either 
`password` or `facebook`. Other properties depend on which provider is being 
used.

#### Password Authentication

|Property|Description|
|---|---|
|username|Unique identifier for the user
|password|Password to authenticate with
|token|A valid JWT token issued by @x/socket

#### Facebook Authentication

|Property|Description|
|---|---|
|facebookToken|A valid JWT token issued by facebook, as returned from a successful login attempt
|token|A valid JWT token issued by @x/socket

### createUser({ username, password, data }, persistent)

### updateUserData({ data })

### logout()

### getAuthenticationStatus()

Returns an observable containing an object with the following properties:

|Property|Description|
|---|---|
|authenticated|True if the user is authenticated|
|user|The user object, if authenticated|

### requestUserVerification({ type })

Only `sms` verification type is currently supported, provided by Twilio.

### verifyUser({ type, verification = { code } })

## License

**The MIT License (MIT)**

Copyright © 2022 Dale Anderson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
