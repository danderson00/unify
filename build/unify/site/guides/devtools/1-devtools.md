# `Unify` Development Tools

The `Unify` platform provides a set of development tools that allow you to submit arbitrary queries, both raw 
expressions and vocabulary, as well as visualise running navigation graphs. The tools are implemented as a 
`@x/socket` feature that is loaded as part of the bundle package. They are automatically embedded in the output 
of the application when running in development mode.

![Development tools](devtools.png)

<p class="yellowTip">
  Open the development tools by typing <code>Ctrl-Shift-Backslash</code> while your application has focus and is in 
  development mode.
</p>

## Executing Queries

By default, the development tools open in raw expression execution mode. An expression can be entered (including the 
leading `o =>`) and scope values can be specified on the right hand pane. To switch to vocabulary mode, click the 
checkbox in the upper right corner.

## Visualising Navigation State Machines

Enabling the checkbox in the title bar marked "Machines" will refresh the page and open a new window containing the 
xstate visualisation tool for the current navigation state machine. The refresh is necessary to provide the 
visualiser the state machine definition. The visualiser will refresh each time the page is refreshed and update to 
the latest state machine.

To close the visualiser, close the browser window and uncheck the "Machines" checkbox.

## Roadmap

Many enhancements are planned for the development tools:

- Intelligent automatic code completion
- Data set load / save
- Visualisation of state machines operating on the host
- Smart scope population