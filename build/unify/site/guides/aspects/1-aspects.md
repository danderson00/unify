# Aspects

The following tables summarise the aspects provided by `Unify`.

## Property Aspects

Aspect|Description
---|---
**Provided by [`@x/types`](/core/types/README.md)**|
`required`|Fail validation if the property value is not set or is null
`min(value)`|Fail validation if the property value is less than the specified amount
`max(value)`|Fail validation if the property value is more than the specified amount
`any`|Allow any value to be provided
`defaultValue(value)`|Sets the property value to the specified value if it is not provided
`generated(generator)`|Sets the property value to the return value of the provided generator function
**Provided by [`unify.host`](/unify/host/README.md)**|
`uuid`|Sets the property to a random universally unique identifier
**Provided by [`@x/socket.unify`](/socket/socket.unify/README.md)**|
`userId`|Sets the property to the current user identifier

## Type Aspects

Aspect|Description
---|---
**Provided by [`@x/types`](/core/types/README.md)**|
`trim`|Remove any properties from the message that are not defined on the type
`strict`|Fail validation if any properties exist on the message that are not defined on the type
`requireAll`|Require all properties defined on the type to be provided and not null
**Provided by [`@x/socket.unify`](/socket/socket.unify/README.md)**|
`authenticated`|Requires that the user is authenticated before publishing the message
`userConstraint(expression, failureMessage)`|Requires the provided expression evaluates to a truthy value before allowing the message to be published
`scopeOwnerOnly(...scopeProperties)`|Requires that the current user owns the scope with the specified properties, or that the scope is not owned


## Call Aspects

Aspect|Description
---|---
**Provided by [`unify.host`](/unify/host/README.md)**|
`open`|Allows external access to the vocabulary with any scope
`scoped(...scopeProperties)`|Allows external access to the vocabulary with the specified scope properties set
**Provided by [`@x/socket.unify`](/socket/socket.unify/README.md)**|
`authenticated`|Requires that the user is authenticated before subscribing to the vocabulary
`userConstraint(expression, failureMessage)`|Requires the provided expression evaluates to a truthy value before allowing the user to subscribe
`userScope`|Forces the vocabulary to be scoped by the current userId, along with any other required scope properties
`ownerOnly`|Requires the user to be the owner of the scope specified with a `scoped` aspect
`scopeOwnerOnly(...scopeProperties)`|Requires the user to be the owner of the scope specified by the provided property names