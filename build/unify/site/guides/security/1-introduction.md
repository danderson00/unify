 # Securing `Unify` - Part 1 - Introduction

`Unify` provides comprehensive functionality for securing your applications through out of the box authentication 
providers and modelling tools to restrict access to read and write models to a subset of users.

## Authentication

Authentication is "pluggable" in nature, allowing for a diverse set of authentication providers to be supported with 
minimal configuration. Currently, Facebook and Auth0 are supported as well as a simple, development only password 
provider. JWT tokens are used to encapsulate user details and claims.

Authentication can be enabled with simple configuration, extending the host API with functions for authenticating 
users and manipulating user data.

## Enforcing Message Schemas
  
Importantly, `Unify` provides the ability to ensure published messages conform to a specified schema using simple 
type annotations configured on the host. These type annotations, or "aspects", are specified using plain, intuitive 
Javascript, enabling simple, virtually unlimited extensibility.

While Javascript was selected to ensure plain Javascript consumers can still leverage the full power of the 
underlying type system, TypeScript support is planned for the near future to enable consumers to enjoy the full 
benefits of compile-time type safety.

## Authorisation

We have seen in the [getting started guide](../getting-started/1-introduction.md) how vocabulary can be used to create 
an "API surface" to restrict the queries that can be executed by the`Unify` host. By annotating the vocabulary 
definitions with simple "aspects", we can specify which vocabulary entries are "public", enforce rules for how they 
should be scoped and restrict access to specific users.

Similarly, a set of aspects is provided for annotating message schemas with rules to restrict the message types that 
users can publish at any given time.

In both cases, rules are expressed using the core `Unify` expression language and are evaluated in real time, 
ensuring user access is automatically kept up to date at all times.

## Next Up - Authentication

[Next](./2-authentication.md), let's take a look at how authentication works and how to get it up and running.