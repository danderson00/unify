# Unify

Unify for Node.js and the browser in a simple package.

For Unify documentation, visit https://unifyjs.io/docs.

## Getting Started

### Install Dependencies

```shell
yarn add unify unify.react sqlite3
```

### Start Application

```shell
npx unify
```

This starts both the host and the react development server. The following command line options are available:

Option| |Type|Default|Description
---|---|---|---|---
--help|-h|boolean| |Show help
--version| |boolean| |Show version number
--config| |string|"unify"|Path to JS or JSON config file
--port|-p|integer|3001|TCP port number to listen on
--scope|-s|string| |Property name that can participate in scopes. Multiple can be specified
--sqliteFile|-f|string| |Path to a SQLite database to use
--vocabulary|-v|string|"src/vocabulary"|Path to a file containing vocabulary definitions
--constraints|-c|string|"src/constraints"|Path to a file containing constraint definitions
--types|-t|string|"src/types"|Path to a file containing type declarations
--strictApi| |boolean| |Enforce strict vocabulary API surface
--strictTypes| |boolean| |Enforce explicit message types
--requireSsl| |boolean| |Allows disabling of SSL requirement for production
--auth0Domain| |boolean| |Enable auth0 authentication with the provided domain
--authSecret| |boolean| |Secret to use for signing authentication tokens
--startDevelopmentServer| |boolean| |Start react development server
--watch| |boolean| |Watch for changes to configuration files and reload on change

For alternate ways of starting the host, including using other database providers, see the section below.

### Configure Consumer

For applications bootstrapped with `create-react-app`, replace the contents of `src/index.js` with the following:

```jsx
import React from 'react'
import ReactDOM from 'react-dom'
import consumer from 'unify'
import { Provider } from 'unify.react'
import App from './App'

consumer().connect()
  .then(host => ReactDOM.render(
    <Provider host={host}><App /></Provider>,
    document.getElementById('root')
  ))
```

Vocabulary can also be passed in as a prop to the `Provider` component.

An options object can also be passed to the consumer factory. Options are as follows:

Name|Type|Description
---|---|---
url|string|The websocket URL of the host to connect to
reconnectDelay|number|Time in milliseconds between reconnect attempts. Defaults to 1000ms
log|object|Logging configuration options. `level` property can be `error`, `warn`, `debug`, `trace` or `none`

Any components in your application can now use the 
[higher order components](https://unifyjs.io/docs#/unify/react/docs/hocs) and
[hooks](https://unifyjs.io/docs#/unify/react/docs/hooks) from the
[`unify.react`](https://unifyjs.io/docs#/unify/react/) library.

## Host Startup / Configuration Options

The example here uses `npx` to invoke the Unify command line interface. A number of other options are available for 
starting and configuring the host.

### Installing Globally

The Unify package can be installed globally by executing:

```shell
npm i -g unify
# or
yarn global add unify
```

This makes the CLI available globally throughout your system and can be invoked without `npx`.

### Using a Configuration File

Configuration options can be specified in a Javascript or JSON file. By default, this file is called `unify.js`.
The full set of options can be found [here](https://unifyjs.io/docs#/unify/bundle/docs/host).

### Starting the Host Programmatically

The host process can be started from any Node.js module.

```javascript
const host = require('unify')
const vocabulary = require('./vocabulary')

host({
  port: 1234,
  scopes: ['orderId', 'productId'],
  storage: { client: 'sqlite3', connection: { filename: 'data.sqlite' } },
  vocabulary
})
```

Any database supported by [`knex`](https://knexjs.org/) can be used and should be configured accordingly here. An 
in-memory instance of the `sqlite` provider is used by default, but the `sqlite3` package must be installed separately. 

## License

**The MIT License (MIT)**

Copyright © 2022 Dale Anderson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
