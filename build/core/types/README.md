# @x/types

Simple, concise data structure validation and mutation.

## Why?

You want to validate data structures and apply simple mutations at run time, like attaching the currently authenticated 
user ID, you don't care about method signatures or return types and aren't willing to bring a heavy piece of 
infrastructure like TypeScript.

## Install

```shell
yarn add @x/types
```

## Use

```javascript
const types = require('@x/types')

const validator = types({
  typeAspects: {
    attachUserId: (target, { context }) => ({ result: { ...target, userId: context.user.id }})
  },
  propertyAspects: {
    timestamp: () => ({ newValue: Date.now() })  
  },
  types: ({ required, attachUserId, timestamp }) => ({
    product: [attachUserId, {
      productId: [required, Number],
      name: [required, String],
      description: String,
      createdAt: timestamp
    }]
  })
})

validator.apply('product', {
  productId: 1,
  name: 'Widget',
  description: 'Best widget on the market!'
}, {
  user: { id: 123, username: 'admin' }
})
/* {
  success: true,
  failures: [],
  result: {
    productId: 1,
    name: 'Widget',
    description: 'Best widget on the market!',
    userId: 123,
    createdAt: 1588675904672
  }
} */

validator.apply('product', {
  productId: 'd45bc',
  description: 'Component' 
}, {
  user: { id: 123, username: 'admin' }
})
/* {
  success: false,
  failures: [
    { aspect: 'Number', property: 'productId', message: 'productId must be a Number' },
    { aspect: 'required', property: 'name', message: 'name is required' }
  ],
  result: {
    productId: 1,
    name: 'Widget',
    description: 'Best widget on the market!',
    userId: 123,
    createdAt: 1588675904672
  }
} */
```

## API

### Constructor

```javascript
const types = require('@x/types')
const validator = types(options)
```

`options` parameter can have the following properties:

Property|Type|Description
---|---|---
types|Object &#124; function|An object map of type names to a collection of type aspects, or a function that returns the same. The function is passed an object map of all known types and aspects.
propertyAspects|Object|An object map of aspect names to custom property aspect functions
typeAspects|Object|An object map of aspect names to custom type aspect functions
globalPropertyAspects|Array|An array of property aspects or property aspect names to apply to every property
globalTypeAspects|Array|An array of type aspects or type aspect names to apply to every type
strict|Boolean|Fail if types or aspects are not registered, or any additional properties exist on objects being validated (see `strict` type aspect)

### `apply(type, target, context)`

Apply the property and type aspects for the specified type name to the target using the provided context.

### `call(target, parameters, context)`

Call the supplied target, applying the call aspects defined.

### `validateCall(target, parameters, context)`

Perform all validations defined by call aspects without actually calling the target.

## Property Aspects

The following built-in property aspects are available:

### `required`

Fail validation if the property value is not set or is null.

### `min(value)`

Fail validation if the property value is less than the specified amount.

### `max(value)`

Fail validation if the property value is more than the specified amount.

### `any`

Allow any value to be provided.

### `defaultValue(value)`

Set the property value to the specified value if it is not provided.

### `generated(generator)`

Set the property value to the return value of the provided generator function. The function is passed the existing 
value of the property.

### Custom Property Aspects

## Type Aspects

The following built-in type aspects are available:

### `trim`

Remove any properties from the message that are not defined on the type.

### `strict`

Fail validation if any properties exist on the message that are not defined on the type.

### `requireAll`

Require all properties defined on the type to be provided and not null.

### Custom Type Aspects

## Call Aspects

`@x/types` does not provide any call aspects out of the box, but implementing your own is simple. 

### Custom Call Aspects

## License

**The MIT License (MIT)**

Copyright © 2022 Dale Anderson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
documentation files (the “Software”), to deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
