rm -rf build
rsync -a -m --include 'core*/' --include 'unify*/' --include 'socket*/' --exclude '/*/' --exclude 'node_modules/' --exclude 'misc/' --exclude '/*' --include '*/' --include '*.md' --exclude '*' .. build/
rsync -a -m site/ build/
cp README.md build
