# Securing `Unify` - Part 4 - Securing Read Access

As we have seen in the previous section on [securing incoming messages](./3-write.md), `Unify` provides a set of 
"aspects" that can be used to restrict what messages users can publish. Similarly, a set of "call aspects" are 
provided that can be used to restrict what vocabulary specific users can subscribe to. 

The set of vocabulary entries can be provided as a function that returns the hash of entries - this function is 
passed an object containing the available set of call aspects, similar to property and type aspects discussed in the 
previous section. 

## Strict Mode

When the `strictApi` configuration option is enabled, i.e. the application is in production mode, each vocabulary 
entry that requires external access must have an aspect applied that indicates it is "public". This can be done with 
the `open` aspect to allow unrestricted access, but it is much more likely that you will use the `scoped` aspect. 

The `scoped` aspect accepts a variable number of string property names indicating which properties must be present 
on the scope object provided when subscribing. For example, vocabulary that returns a list of transactions might 
look like:

```javascript
const vocabulary = {
  // transaction is "internal" vocabulary, not available externally in production mode
  transaction: o => o.topic('transaction').accumulate(),
  
  // ensure that transaction lists are scoped by the accountId
  transactions: [scoped('accountId'),
    o => o.transaction().all()
  ]
}
```

## Vocabulary Authorisation Aspects

Similar to their corresponding type aspects, `authenticated` and `userConstraint` call aspects are provided. 
Additionally, a `userScope` aspect is provided that forces the vocabulary to be scoped by the userId of the 
authenticated user, along with any other scope properties required. It should be used when declaring the `user` 
vocabulary.

Scope ownership aspects are also provided - `ownerOnly` ensures the subscribing user is the current owner of the 
scope declared with `scope` aspects, and `scopeOwnerOnly` that accepts a variable number of string property names as 
parameters to explicitly declare the scope ownership to test.

See the [aspects page](/guides/aspects/1-aspects) for detail on all built in aspects provided.