# Securing `Unify` - Part 3 - Securing Incoming Messages

Ensuring users can only write data they are authorized to is a critical part of any distributed application. 
`Unify` provides a simple but comprehensive, pure Javascript type system with authorization annotations, or "aspects",
to ensure all incoming data conforms to predefined schemas, and users can only publish messages they are permitted to. 

## Enforcing Message Schemas

Individual message types that correspond with each unique topic can be described by creating a module called 
`types.js` in the `src` directory of your application that exports a function returning type information, or 
providing a similar function to the `types` property of your main configuration object.

For example, a simple type declaration to describe a transaction against an account might look like:

```javascript
module.exports = () => ({
  transaction: {
    accountId: String,
    userId: String,
    value: Number
  }
})
```

### Introducing Aspects

Using Javascript built-in types like this is called applying "property aspects" to properties. Along with Javascript 
types, the exported function is passed an object containing a number of other aspects that you can apply. This object 
also contains aspects that can be applied to a topic's type, or "type aspects".

For example, a more complete definition for the transaction above might look like:

```javascript
module.exports = ({ strict, required, min }) => ({
  transaction: [strict, { // don't allow the message to contain any other properties
    accountId: [required, String],
    userId: [required, String],
    value: [required, min(-1000), Number],  // maximum withdrawal of $1000
    description: String                     // optional description
  }]
})
```

For details on all of the available built in aspects, see the [aspects guide](../aspects/1-aspects.md).

## Message Authorisation Aspects

Along with the built-in aspects provided by the `@x/types` library, enabling an authentication provider makes a 
number of authorisation related aspects available for controlling which users can publish specific messages at a 
given time.

The three core aspects are a property aspect called `userId` that sets the property to the current user ID and two type 
aspects, `authenticated` that simply ensures that users are authenticated before they can publish the message and 
`userConstraint`. 

Before the `userConstraint` aspect can be used, you must first declare a vocabulary entry called `user` that defines 
the user model. The first parameter to the `userConstraint` aspect is a function that is passed the evaluated, 
unwrapped result of the expression and must evaluate to a truthy value before the user can publish the message. The 
second is the error message that is generated if the constraint fails.

A simple example that ensures users must have at least one "reputation" before they can publish a "comment" might 
look like:

```javascript
const vocabulary = {
  user: o => o.topic('reputation').count()
}

const types = ({ userId, userConstraint }) => ({
  reputation: { userId },
  comment: [
    userConstraint(reputation => reputation > 0, "You must have at least 1 reputation"),
    { 
      userId,
      text: [required, String]
    }
  ]
})
```

## Owned Scopes

In addition to simple user model constraints, `Unify` can enforce "scope ownership", where the first user to publish 
a message containing unique scope values is considered the owner of that scope. Aspects can be added that restrict 
publishing of messages to scope owners only, as well as restricting subscriptions to specific vocabulary, as we'll 
see in the next section.

In order to be able to use scope ownership aspects, the desired scopes must be added to the `Unify` configuration 
using the `ownedScopes` node. Similar to the `scopes` node, this is an array of property names corresponding to the 
scopes that should have ownership. These owned scopes must also be present in the `scopes` array.

To revisit our account transaction one last time, here is a secured version:

```javascript
module.exports = ({ scopeOwnerOnly, strict, required, min }) => ({
  transaction: [strict, scopeOwnerOnly('accountId'), {
    userId,
    accountId: [required, String],
    value: [required, min(-1000), Number],
    description: String
  }]
})
```

## Global Aspects

Aspects can also be set up to apply to every property or type by providing a `globalPropertyAspects` or 
`globalTypeAspects` configuration node to the `Unify` host. The nodes should contain an array of aspects (or the 
string names of built-in aspects) to be applied to all properties and types.

## Strict Mode

When the `strictTypes` configuration option is enabled, i.e. the application is in production mode, a type must be 
explicitly declared for each message. Additionally, the `strict` aspect is automatically applied to all 
types to ensure messages do not contain additional, undefined properties.

## Custom Aspects

For information on implementing custom aspects, consult the [`@x/types` documentation](/core/types/README.md).

## Next - Securing Your Read Model

[Next up](./4-read.md) we'll have a look at ways to restrict access to vocabulary.