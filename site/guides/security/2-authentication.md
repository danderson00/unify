# Securing `Unify` - Part 2 - Authentication Overview

When authentication is enabled on the `Unify` host by providing an authentication configuration node, the host API is
extended with functions for creating and authenticating users. The `auth` React higher order component exposes these 
functions.

The following built in authentication providers are available:

## Auth0

The Auth0 authentication provider is enabled by providing the following configuration:

```javascript
{
  auth: {
    secret: '<my_JWT_hashing_key>'
    auth0: {
      domain: '<your_domain>.auth0.com'
    }
  }
}
```

The domain is created when you create the application in the [Auth0 portal](https://manage.auth0.com/).

<p class="yellowTip">
  It is important to provide a unique secret for signing JWT tokens by populating the <code>secret</code> property 
  of the authentication configuration.
</p>

An example component to authenticate and provide the token to the `Unify` host might look like:

```javascript
import React from 'react'
import { auth } from 'unify.react'
import { useAuth0 } from '@auth0/auth0-react'

export default auth(function Auth0Login({ auth }) {
  const { getAccessTokenWithPopup } = useAuth0()
  
  const login = () => getAccessTokenWithPopup()
    .then(auth0Token =>
      // authenticate with the Unify host  
      auth.authenticate({ 
        provider: 'auth0', 
        auth0Token 
      }, true)
    )

  return (
    <button onClick={login}>Login</button>
  )
})
```

All authentication and validation is done by Auth0 servers - `Unify` simply validates tokens by calling the Auth0 API.

Logging out can be performed by calling the `logout` function exposed by the host and the `auth` React HoC.

## Facebook

No additional configuration is required to enable the Facebook authentication provider - passing a truthy value to 
the `facebook` property of the authentication configuration node will enable the provider:

```javascript
{
  auth: {
    secret: '<my_JWT_hashing_key>'
    facebook: true
  }
}
```

Similar to the Auth0 provider, logging in is a matter of using the Facebook SDK to authenticate and passing the 
token to the `authenticate` function:

```javascript
const login = () => FB.login(
  ({ authResponse }) => auth.authenticate({
    provider: 'facebook',
    facebookToken: authResponse.accessToken
  }, true)
)
```

## Password

<p class="yellowTip">The password authentication provider is intended for development purposes only.</p>

The password provider is enabled by simply passing a truthy value to the `password` property. The `authenticate` 
function should be passed the username and password:

```javascript
const login = (username, password) => auth.authenticate({
  provider: 'password',
  username,
  password
})
```

As well as functions to authenticate and log out, the password provider expose a function for creating users:

```javascript
const createUser = (username, password) => auth.createUser({
  provider: 'password',
  username,
  password
})
```

Users are logged in immediately on calling `createUser`, there is no need to subsequently call `authenticate`.

## Accessing User Details

The `auth` HoC also exposes a boolean property `authenticated` and the original user token can claims can be 
accessed through the `user` property.

## Next Up: Introduction to Authorization

[Next](./3-write.md), we look at how we can use the authenticated user details to restrict what users can publish to 
the host, as well as ensuring messages conform to a particular structure.

