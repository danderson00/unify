# Deploying `Unify` Applications

`Unify` has been designed to make the transition from development through other environments to production as simple 
and frictionless as possible. In most cases, deployment consists of creating a configuration file for the 
specific environment and triggering a standard `npm` build process.

## Preparing for Production

When in development mode, the `Unify` host operates with a number of security measures disabled as well as default 
console logging settings. More detail on the security issues and best practice can be found in the
[security guide](../security/1-introduction.md).

### Configuration Files

By default, `Unify` looks for configuration in the `unify.js` module in the root directory of your project. Alternatively,
if the `NODE_ENV` environment variable is set, it will first look for a module matching the naming convention
`unify.<environment>.js`. If found, this file will be used instead. 

This allows different configurations to be created for each different environment, including database connections, 
logging, etc. Keep in mind that `jest` and other test frameworks automatically set the value of `NODE_ENV` to "test".

### Configuration Defaults

The following table lists the configuration defaults that apply to relevant environments and their effect:

Setting|Development|Production|Effect
---|---|---|---
`allowRawQueries`|`true`|`false`|Disables the ability to execute raw queries directly. Subscriptions must target vocabulary
`strictApi`|`false`|`true`|Vocabulary must be explicitly marked as public using an appropriate aspect to be executed remotely
`strictTypes`|`false`|`true`|Published messages must pass explicitly defined type checks for each topic
`requireSSL`|`false`|`true`|Connections to the host must be made over an encrypted transport layer (SSL)

It is highly recommended to test the effects of these configuration changes in a staging environment before 
deploying to production.

### Serving Static Files

When running in production mode, by default, the `Unify` bundle will attempt to serve static files from the `build` 
directory of your project as well as start the host websocket server. The following configuration settings affect 
the server set up:

Setting|Default|Effect
---|---|---
`server`| |HTTP server instance to attach to. All other settings are ignored if this is provided
`port`|443|Port to start listening on
`keyFile`| |Path to key file 
`certFile`| |Path to certificate file
`buildPath`|build|Path to serve static application files from
`requireSSL`|`true`|Connections to the host must be made over an encrypted transport layer (SSL)
`redirectInsecure`|`true`|Redirect HTTP requests to HTTPS if SSL is configured
`insecurePort`|80|Port to serve redirect requests from

### Configuring Logging

By default, logging in both development and production mode is limited to console logging of all entries `info` 
level and above. For production, a log writer should be used that stores log records in a persistent medium, such as 
the file system.

To configure file based logging, use the following configuration:

```javascript
const { writers: { file, console } } = require('@x/log')

module.exports = {
  log: {
    writers: [
      console(),
      file({ destination: '/var/log/unify', rotate: true })
    ]
  }
}
```

For complete configuration information and details on implementing custom log writers, see the 
[`@x/log` documentation](/core/log/).

### Preparing SSL

The `Unify` bundle includes out of the box integration with Let's Encrypt for seamless, automatic requesting and 
renewal of SSL certificates. To enable, provide a `letsEncrypt` configuration node with the following properties:

Name|Default|Description
---|---|---
domain| |Domain name to request
email| |Administrative email contact for the domain
configDir|./acme|Directory to store certificates and configuration information

It is recommended setting the `configDir` option to a location outside the application directory. If the application 
directory is deleted and recreated during a deployment, this will cause certificates to be requested every time the 
application is deployed, potentially triggering rate limits.

For more information, please see the [Greenlock documentation](https://www.npmjs.com/package/greenlock).

For manually configured SSL certificates, provide the `keyFile` and `certFile` configuration options, as described 
above.

## Deployment

Once configured for production, deployment can be performed using any standard build tools by building the React
application by running

```shell
yarn build
```

The application can then be started with the following command:

```shell
NODE_ENV=production yarn start
```

<p class="yellowTip">
  Remember to set the NODE_ENV environment variable to the appropriate value when starting the application!
</p>

It is recommended to use a Node process manager tool such as [`pm2`](https://pm2.keymetrics.io/) to ensure the host 
process restarts after unexpected exits.

## Scaling

The ability to seamlessly scale out to additional hosts is currently being developed and will be available later in 
2022. Please contact unifyjs@gmail.com for more information.

In the interim, simple partitioning schemes, such as geographical partitioning or per-customer host instances can be 
used to expand user capacity.
