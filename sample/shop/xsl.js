module.exports = {
  scopes: ['productId', 'cartId'],
  strictApi: true,
  strictTypes: true,
  storage: { client: 'sqlite', connection: { filename: 'data.db' } },
  files: { provider: 'local', cors: true },
  auth: { password: true, secret: '1337' }
}