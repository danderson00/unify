import { Machine } from '@x/unify.react'

const navigation = Machine({
  initial: 'shop',
  on: {
    'home': 'shop',
    'checkout': 'checkout',
    'admin': 'admin'
  },
  states: {
    'shop': {
      initial: 'Browse',
      states: {
        'Browse': {
          on: { 'productSelected': 'Product' },
        },
        'Product': {
          meta: { scope: ['productId'] }
        },
      }
    },
    'checkout': {
      initial: 'ReviewItems',
      states: {
        'ReviewItems': {
          on: { 'itemsConfirmed': 'Shipping' }
        },
        'Shipping': {
          on: { 'shippingDetails': 'Payment' }
        },
        'Payment': {
          on: { 'paymentDetails': 'Complete' }
        },
        'Complete': {}
      }
    },
    'admin': {
      initial: 'Products',
      states: {
        'Products': {
          on: { 'productSelected': 'Product' }
        },
        'Product': {
          meta: { scope: ['productId'] }
        }
      }
    },
  }
})

export default navigation
