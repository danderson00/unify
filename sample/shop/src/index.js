import React from 'react'
import ReactDOM from 'react-dom'
import consumer from '@x/unify'
import { Provider } from '@x/unify.react'
import { CssBaseline, MuiThemeProvider } from '@material-ui/core'
import { ContextProvider } from './context'
import { App } from './components'
import theme from './theme'

consumer().connect()
  .then(host => ReactDOM.render(
    <Provider host={host}>
      <ContextProvider>
        <MuiThemeProvider theme={theme}>
          <App />
          <CssBaseline />
        </MuiThemeProvider>
      </ContextProvider>
    </Provider>,
    document.getElementById('root')
  ))
