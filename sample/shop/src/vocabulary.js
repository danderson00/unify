module.exports = ({ scoped }) => ({
  products: [scoped(),
    o => o.groupBy('productId',
      o => o.product(),
      o => o.topic('productDeleted')
    )
  ],
  product: [scoped('productId'),
    o => o.assign({
      '...details': o => o.topic('product').accumulate(),
      'images': o => o.topic('productImage').all(),
      'rating': o => o.averageRating()
    })
  ],

  reviews: [scoped('productId'), o => o.topic('review').all()],
  averageRating: [scoped('productId'), o => o.topic('review').average('rating')],

  cartQuantity: [scoped('cartId'), o => o.topic('productAdded').sum('quantity')],
  order: [scoped('cartId'), o => o.compose(
    o => o.topic('productAdded').groupBy('productId', o => o.orderLine()),
    items => ({
      items,
      total: items.reduce((total, item) => total + item.total, 0)
    })
  )],
  orderLine: o => o.compose(
    o => o.select('productId'),
    o => o.sum('quantity'),
    o => o.join('productId', o => o.product()),
    (productId, quantity, product) => ({
      productId,
      quantity,
      ...product,
      total: quantity * product.price
    })
  ),

  search: [scoped(), (o, keyword, category) => o.products().where(x =>
    (!keyword || x.name.includes(keyword) || (x.description && x.description.includes(keyword))) &&
    (!category || x.category === category)
  )]
})