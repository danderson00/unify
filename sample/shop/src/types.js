module.exports = ({ authenticated, required, requireAll }) => ({
  product: [authenticated, requireAll, {
    productId: String,
    name: String,
    description: String,
    price: Number,
    category: String
  }],
  productDeleted: [authenticated, {
    productId: [required, String]
  }],
  productImage: [authenticated, requireAll, {
    imageId: String,
    productId: String,
    url: String
  }],
  productSelected: {
    productId: [required, String]
  },
  productAdded: [requireAll, {
    productId: String,
    cartId: String,
    quantity: Number
  }],
  review: [requireAll, {
    reviewId: String,
    productId: String,
    text: String,
    rating: Number
  }],

  itemsConfirmed: {},
  shippingDetails: {
    details: [required, Object]
  },
  paymentDetails: {
    details: [required, Object]
  }
})