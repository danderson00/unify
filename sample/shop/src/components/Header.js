import { publisher, auth, useEvaluate } from '@x/unify.react'
import { Button } from '@material-ui/core'
import { RowBox } from './widgets'
import logo from '../assets/logo-light.png'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'
import { Login } from './widgets/Login'
import { useCart } from '../context'

export const Header = auth(publisher(
  ({ publish, auth: { authenticated } }) => {
    const { cartId } = useCart()
    const [cartQuantity] = useEvaluate('cartQuantity', { cartId })

    return (
      <RowBox fullWidth p={2} mb={2}>
        <RowBox
          grow={1}
          alignItems="center"
          gap={8}
        >
          <img src={logo} alt="logo" className="clickable" onClick={() => publish.local('home')} />
          <h1 className="clickable" onClick={() => publish.local('home')}>Store</h1>
        </RowBox>

        <Button
          onClick={() => publish.local('checkout')}
          disabled={cartQuantity === 0}
        >
          <ShoppingCartIcon /> ({cartQuantity})
        </Button>

        {authenticated &&
          <Button onClick={() => publish.local('admin')}>Admin</Button>
        }

        <Login />
      </RowBox>
    )
  }
))
