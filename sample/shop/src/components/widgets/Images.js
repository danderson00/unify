import { consumer, publisher } from '@x/unify.react'
import { Box, IconButton } from '@material-ui/core'
import AddIcon from '@material-ui/icons/AddCircle'
import { v4 as uuid } from 'uuid'
import { Image } from '.'

export const Images = consumer(publisher(({ host, publish, images, publishProps }) => (
  <Box>
    <Image image={images[0]} primary />
    <Box display="flex">
      {images.slice(1).map(image =>
        <Image key={image.imageId} image={image} />
      )}
      {publishProps &&
        <IconButton style={{ width: 100 }} onClick={() => host.uploadFilesDialog().then(({ urls }) => {
          Object.values(urls).forEach(
            url => publish({ imageId: uuid(), url, ...publishProps })
          )
        })}>
          <AddIcon fontSize="large" />
        </IconButton>
      }
    </Box>
  </Box>
)))
