import placeholder from '../../assets/placeholder.png'

export const Image = ({ image, primary, className, ...props }) => (
  <img
    src={image?.url || placeholder}
    {...props}
    alt="product"
    className={`${primary ? 'primary' : 'secondary'} ${className ? className : ''}`}
  />
)