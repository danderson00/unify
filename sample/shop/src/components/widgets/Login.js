import { auth } from '@x/unify.react'
import { useState } from 'react'
import { Button, Dialog, DialogActions, DialogContent, Typography } from '@material-ui/core'
import { Form, Submit, Text } from '@danderson00/react-forms-material'

export const Login = auth(({ auth: { authenticate, createUser, authenticated, logout } }) => {
  const [open, setOpen] = useState(false)
  const [create, setCreate] = useState(false)

  const handleClick = () => authenticated ? logout() : setOpen(true)
  const login = ({ username, password }) => (create ? createUser : authenticate)
    ({ provider: 'password', username, password }, true)
      .then(result => {
        if(result.success) {
          setOpen(false)
          setCreate(false)
        } else {
          throw result
        }
      })

  return <>
    <Button onClick={handleClick}>
      Log {authenticated ? 'out' : 'in'}
    </Button>

    <Dialog open={open} onClose={() => setOpen(false)}>
      <Form onSubmit={login}>
        <DialogContent>
          <h1>{create ? 'Create Account' : 'Log In'}</h1>
          <Text name="username" label="Username" required />
          <Text name="password" label="Password" type="password" required />
          <Typography variant="caption">
            {create
              ? <>Already have an account? <span className="link" onClick={() => setCreate(false)}>Log in.</span></>
              : <>No account? <span className="link" onClick={() => setCreate(true)}>Create one.</span></>
            }
          </Typography>
        </DialogContent>
        <DialogActions>
          <Submit>{create ? 'Create Account' : 'Log In'}</Submit>
          <Button onClick={() => setOpen(false)}>Cancel</Button>
        </DialogActions>
      </Form>
    </Dialog>
  </>
})