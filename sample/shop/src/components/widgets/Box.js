import { Box as MuiBox } from '@material-ui/core'

export const Box = ({ basis, direction, grow, shrink, wrap, fullWidth, alignItems, justifyContent, gap, ...props }) => <MuiBox
  display="flex"
  flexBasis={basis}
  flexDirection={direction}
  flexGrow={grow}
  flexShrink={shrink}
  flexWrap={wrap}
  alignItems={alignItems}
  justifyContent={justifyContent}
  {...(fullWidth && { width: '100%' })}
  {...(gap && { style: { gap } })}
  {...props}
/>

export const RowBox = ({ basis, grow, shrink, wrap, fullWidth, alignItems, justifyContent, gap, ...props }) => <Box
  direction="row"
  {...{ basis, grow, shrink, wrap, fullWidth, alignItems, justifyContent, gap, ...props }}
/>

export const ColumnBox = ({ basis, grow, shrink, wrap, fullWidth, alignItems, justifyContent, gap, ...props }) => <Box
  direction="column"
  {...{ basis, grow, shrink, wrap, fullWidth, alignItems, justifyContent, gap, ...props }}
/>
