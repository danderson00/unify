import { publisher } from '@x/unify.react'
import { Form, Text, Submit } from '@danderson00/react-forms-material'
import { Card } from '@material-ui/core'
import { useCart } from '../../context'

export const Payment = publisher(({ publish }) => {
  const { newCart } = useCart()
  const pay = details => publish('paymentDetails', { details }).then(newCart)

  return (
    <Card>
      <Form onSubmit={pay}>
        <Text name="name" label="Name on card" value="Joe Bloggs" required />
        <Text name="number" label="Card number" value="4239 0000 0000 0000" required />
        <Text name="expiry" label="Expiry" value="01/32" required />
        <Text name="cvv" label="CVV" value="999" required />

        <Submit variant="contained">Next</Submit>
      </Form>
    </Card>
  )
})