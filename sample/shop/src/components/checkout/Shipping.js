import { publisher } from '@x/unify.react'
import { Form, Text, Submit } from '@danderson00/react-forms-material'
import { Card } from '@material-ui/core'

export const Shipping = publisher(({ publish }) => (
  <Card>
    <Form onSubmit={details => publish('shippingDetails', { details })}>
      <Text name="name" label="Name" value="Joe Bloggs" required />
      <Text name="address" label="Address" value="123 Lime Peel Drive" required />
      <Text name="city" label="City" value="Peel" required />
      <Text name="state" label="State" value="NSW" required />
      <Text name="postcode" label="Postcode" value="2795" required />

      <Submit variant="contained">Next</Submit>
    </Form>
  </Card>
))