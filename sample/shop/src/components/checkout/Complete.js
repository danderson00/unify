import { publisher } from '@x/unify.react'
import { Card, Button } from '@material-ui/core'

export const Complete = publisher(({ publish }) => (
  <Card>
    <p>Your order has been submitted!</p>
    <Button variant="contained" onClick={() => publish.local('home')}>
      Home
    </Button>
  </Card>
))