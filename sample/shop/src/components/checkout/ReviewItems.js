import { publisher, useEvaluate } from '@x/unify.react'
import { Button, Table, TableBody as Body, TableHead as Head, TableRow as Row, TableCell as Cell } from '@material-ui/core'
import { useCart } from '../../context'

export const ReviewItems = publisher(({ publish }) => {
  const { cartId, newCart } = useCart()
  const [order] = useEvaluate('order', { cartId })

  const confirm = () => publish('itemsConfirmed')
  const empty = () => {
    newCart()
    publish.local('home')
  }

  return (
    <div>
      <Table>
        <Head>
          <Row>
            <Cell>Product</Cell>
            <Cell>Qty</Cell>
            <Cell>@</Cell>
            <Cell>=</Cell>
          </Row>
        </Head>

        <Body>
          {order.items.map(item =>
            <Row key={item.productId}>
              <Cell>{item.name}</Cell>
              <Cell>{item.quantity}</Cell>
              <Cell>{item.price && item.price.toFixed(2)}</Cell>
              <Cell>{item.total && item.total.toFixed(2)}</Cell>
            </Row>
          )}
          <Row>
            <Cell><b>TOTAL:</b></Cell>
            <Cell></Cell>
            <Cell></Cell>
            <Cell><b>${order.total && order.total.toFixed(2)}</b></Cell>
          </Row>
        </Body>
      </Table>

      <Button variant="contained" onClick={confirm}>
        Confirm items
      </Button>

      <Button variant="contained" onClick={empty}>
        Empty cart
      </Button>
    </div>
  )
})
