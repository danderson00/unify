import { Navigator } from '@x/unify.react'
import { ColumnBox } from './widgets'
import navigation from '../navigation'
import * as components from '.'
import './App.css'

export const App = () => (
  <ColumnBox p={2} fullWidth>
    <components.Header />
    <Navigator
      machine={navigation}
      resolveComponent={paths => (
        // importing all components above bundles everything into a single bundle
        // you can split bundles here by dynamically calling import(path) instead
        paths.join('/').split('/').reduce((tree, segment) => tree[segment], components)
      )}
    />
  </ColumnBox>
)
