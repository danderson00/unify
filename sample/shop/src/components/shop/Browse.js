import { useEvaluate } from '@x/unify.react'
import { Form, Text, Select, Submit } from '@danderson00/react-forms-material'
import { ProductCard } from '.'

export const Browse = () => {
  const [products, executeSearch] = useEvaluate('search', {}, [])
  const search = ({ keyword, category }) => executeSearch(keyword, category)

  return (
    <>
      <Form onSubmit={search} row gap={4}>
        <Text name="keyword" label="Keyword" value="" shrinkLabel />
        <Select name="category" label="Category" values={['', 'Components', 'Widgets']} />
        <Submit variant="contained">Search</Submit>
      </Form>

      {products.map(product => (
        <ProductCard key={product.productId} product={product}/>
      ))}
    </>
  )
}
