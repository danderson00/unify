import { publisher } from '@x/unify.react'
import { Form, Submit, Input } from '@danderson00/react-forms-material'
import { useCart } from '../../context'

export const AddToOrder = publisher(({ product, publish }) => {
  const { cartId } = useCart()

  return (
    <Form
      row gap={8}
      resetOnSubmit
      className="right"
      onSubmit={({ quantity }) => {
        publish('productAdded', { productId: product.productId, cartId, quantity })
      }}
    >
      <Input name="quantity" type="number" className="short" required />
      <Submit>Add to order</Submit>
    </Form>
  )
})
