import { publisher } from '@x/unify.react'
import { Card } from '@material-ui/core'
import { AddToOrder } from '.'
import { Rating } from '../reviews'
import { ColumnBox, Image, RowBox } from '../widgets'

export const ProductCard = publisher(({ product, publish }) => (
  <Card>
    <AddToOrder product={product} />

    <RowBox gap={8}>
      <Image
        image={product.images[0]}
        className="clickable"
        onClick={() => publish('productSelected', { productId: product.productId })}
      />
      <ColumnBox>
        <h4
          className="clickable"
          onClick={() => publish('productSelected', { productId: product.productId })}
        >
          {product.name} - {product.category} - ${product.price}
        </h4>

        <Rating rating={product.rating} decimals={1} />
        <p>{product.description}</p>
      </ColumnBox>
    </RowBox>
  </Card>
))