import { connect, publisher } from '@x/unify.react'
import { Box, Card, Fab } from '@material-ui/core'
import { AddToOrder } from './AddToOrder'
import { Rating, Reviews } from '../reviews'
import { Images, RowBox } from '../widgets'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'

export const Product = publisher(connect('product')(({ product, publish }) => {
  const { productId, name, price, category, description, images } = product

  return <>
    <Card key={productId} className="positioning-container">
      <RowBox gap={8}>
        <Box>
          <Images images={images} />
        </Box>

        <Box>
          <h2>{name} <Rating rating={product.rating} decimals={1} /></h2>
          <h3>${price}</h3>
          <h4>{category}</h4>

          <p>{description}</p>
          <AddToOrder product={product} />
        </Box>
      </RowBox>

      <Fab onClick={() => publish.local('home')} className="back">
        <ArrowBackIcon />
      </Fab>
    </Card>

    <Reviews />
  </>
}))
