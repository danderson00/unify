import { useState } from 'react'
import { connect, publisher } from '@x/unify.react'
import { Button, Card, Table, TableBody, TableCell, TableHead, TableRow } from '@material-ui/core'
import { AddProduct } from '.'
import { Box } from '../widgets'

export const Products = publisher(connect('products')(({ products, publish }) => {
  const [create, setCreate] = useState(false)

  return <Card>
    <Table>
      <TableHead>
        <TableRow>
          <TableCell>Name</TableCell>
          <TableCell>Price</TableCell>
          <TableCell>Category</TableCell>
          <TableCell>Description</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {products.map(({ productId, name, price, category, description }) =>
          <TableRow
            key={productId}
            onClick={() => publish('productSelected', { productId })}
            className="clickable"
          >
            <TableCell>{name}</TableCell>
            <TableCell>${price}</TableCell>
            <TableCell>{category}</TableCell>
            <TableCell>{description}</TableCell>
          </TableRow>
        )}
      </TableBody>
    </Table>

    <Box mt={2}>
      <Button onClick={() => setCreate(true)} variant="contained">
        Add New Product
      </Button>
    </Box>

    <AddProduct
      open={create}
      onClose={() => setCreate(false)}
    />
  </Card>
}))
