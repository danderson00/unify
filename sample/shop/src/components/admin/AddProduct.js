import { consumer, publisher } from '@x/unify.react'
import { Button, Dialog, DialogActions, DialogContent } from '@material-ui/core'
import { Form, Text, Submit, Select } from '@danderson00/react-forms-material'
import { v4 as uuid } from 'uuid'

export const AddProduct = publisher(consumer(({ publish, open, onClose }) => (
  <Dialog open={open} onClose={onClose}>
    <Form
      resetOnSubmit
      onSubmit={details =>
        publish('product', { productId: details.productId || uuid(), ...details }).then(onClose)
      }
    >
      <DialogContent>
        <h1>Add Product</h1>
        <Text name="name" label="Name" required />
        <Text name="description" label="Description" multiline rows={3} />
        <Text name="price" label="Price" type="number" required />
        <Select name="category" label="Category" values={['Components', 'Widgets']} required />
      </DialogContent>
      <DialogActions>
        <Submit variant="contained">Save</Submit>
        <Button onClick={onClose}>Cancel</Button>
      </DialogActions>
    </Form>
  </Dialog>
)))
