import { connect, publisher } from '@x/unify.react'
import { Box, Button, Card, Fab } from '@material-ui/core'
import { Images, RowBox } from '../widgets'
import ArrowBackIcon from '@material-ui/icons/ArrowBack'

export const Product = publisher(connect('product')(({ product, publish, onClick }) => {
  const { productId, name, price, category, description, images } = product

  return (
    <Card key={productId} className="positioning-container">
      <RowBox gap={8}>
        <Images images={images} publishProps={{ topic: 'productImage', productId }} />

        <Box>
          <h2 onClick={() => onClick(product)}>{name} - ${price}</h2>
          <h4>{category}</h4>

          <p>{description}</p>
          <Button onClick={() => publish('productDeleted', { productId })}>
            Delete
          </Button>
        </Box>
      </RowBox>

      <Fab onClick={() => publish.local('admin')} className="back">
        <ArrowBackIcon />
      </Fab>
    </Card>
  )
}))
