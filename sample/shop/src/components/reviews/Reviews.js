import { useState } from 'react'
import { connect } from '@x/unify.react'
import { Button, Card } from '@material-ui/core'
import { AddReview, Rating } from '.'
import { RowBox } from '../widgets'

export const Reviews = connect('reviews')(({ reviews }) => {
  const [create, setCreate] = useState(false)

  return <>
    <Card>
      <h2>Reviews</h2>

      {reviews.length === 0 &&
        <h3>No reviews have been added.</h3>
      }

      {reviews.map(({ reviewId, rating, text }) => (
        <RowBox key={reviewId} gap={8}>
          <Rating rating={rating} />
          {text}
        </RowBox>
      ))}

      <Button onClick={() => setCreate(true)}>
        Add Review
      </Button>
    </Card>
    <AddReview open={create} onClose={() => setCreate(false)} />
  </>
})
