import { publisher } from '@x/unify.react'
import { Form, Text, Radio, Submit } from '@danderson00/react-forms-material'
import { Button, Dialog, DialogActions, DialogContent } from '@material-ui/core'
import { v4 as uuid } from 'uuid'

export const AddReview = publisher(({ publish, open, onClose }) => (
  <Dialog open={open} onClose={onClose}>
    <Form
      resetOnSubmit
      onSubmit={values => {
        publish('review', { reviewId: uuid(), ...values })
        onClose()
      }}
    >
      <DialogContent>
        <h1>Add Review</h1>
        <Text name="text" label="Review" multiline rows={3} required />
        <Radio name="rating" label="Rating" numeric required row values={[1, 2, 3, 4, 5]} />
      </DialogContent>
      <DialogActions>
        <Submit variant="contained">Submit</Submit>
        <Button onClick={onClose}>Cancel</Button>
      </DialogActions>
    </Form>
  </Dialog>
))
