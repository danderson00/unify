import { Box } from '../widgets'

export const Rating = ({ rating, decimals }) => (
  <Box display="inline-flex">
    <Star disabled={!rating} />
    {rating ? rating.toFixed(decimals) : 0}
  </Box>
)

const Star = ({ disabled }) => (
  <svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" viewBox="0 0 260 260">
    <g>
      <path d="m55,237 74-228 74,228L9,96h240" fill={disabled ? '#DDDDDD' : '#FFC108'} />
    </g>
  </svg>
)

