import { createContext, useContext, useState } from 'react'
import { v4 as uuid } from 'uuid'

const context = createContext({ cartId: null })

export const ContextProvider = ({ children }) => {
  const [cartId, setCartId] = useState(
    window.localStorage.cartId = window.localStorage.cartId || uuid()
  )

  return <context.Provider value={{
    cartId,
    newCart: () => {
      setCartId(window.localStorage.cartId = uuid())
    }
  }}>
    {children}
  </context.Provider>
}

export const useCart = () => {
  const { cartId, newCart } = useContext(context)
  return { cartId, newCart }
}
