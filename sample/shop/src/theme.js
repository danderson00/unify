import { createTheme } from '@material-ui/core/styles'

export default createTheme({
  spacing: 4,
  palette: {
    background: {
      default: '#f9f9ff'
    }
  }
})
