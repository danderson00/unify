import React from 'react'
import { publisher } from '@x/unify.react'
import { Form, Text, Submit, Button } from '@danderson00/react-forms-material'

export default publisher(
  ({ publish, username }) => (
    <Form display="inline" resetOnSubmit onSubmit={
      ({ text }) => publish({ topic: 'message', text, username, publishedAt: Date.now() })}
    >
      <Text name="text" label="Message" required />

      <Submit>Send</Submit>

      <Button onClick={
        () => publish({ topic: 'leave', username })
      }>
        Leave Room
      </Button>
    </Form>
  )
)
