module.exports = {
  message: o => o.topic('message'),
  messages: o => o.message().takeLast(5)
}