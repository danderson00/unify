module.exports = [
  {
    expression: o => o
      .topic('message')
      .takeLast(4)
      .where(x => x.publishedAt > Date.now() - 5000)
      .count()
      .map(x => x <= 3),
    scopeProps: ['room', 'username'],
    topics: ['message'],
    failureMessage: `You cannot publish more than 3 messages per 5 seconds`
  }
]
