import React, { useState } from 'react'
import { Form, Text, Submit } from '@danderson00/react-forms-material'
import Room from './Room'

export default () => {
  const [room, setRoom] = useState()

  return <div>
    <Form onSubmit={setRoom} display="inline">
      <Text name="user" label="User Name" />
      <Text name="name" label="Room" required />
      <Submit>Join</Submit>
    </Form>

    {room && <Room
      scope={{ room: room.name }}
      username={room.user || 'Anonymous'}
    />}
  </div>
}
