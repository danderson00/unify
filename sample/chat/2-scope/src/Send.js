import React from 'react'
import { publisher } from '@x/unify.react'
import { Form, Text, Submit } from '@danderson00/react-forms-material'

export default publisher(
  ({ publish, username }) => (
    <Form display="inline" resetOnSubmit onSubmit={
      ({ text }) => publish({ topic: 'message', text, username })}
    >
      <Text name="text" label="Message" />
      <Submit>Send</Submit>
    </Form>
  )
)
