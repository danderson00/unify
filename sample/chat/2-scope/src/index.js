import React from 'react'
import ReactDOM from 'react-dom'
import consumer from '@x/unify'
import { Provider } from '@x/unify.react'
import App from './App'

consumer().connect()
  .then(host => ReactDOM.render(
    <Provider host={host}>
      <App />
    </Provider>,
    document.getElementById('root')
  ))
