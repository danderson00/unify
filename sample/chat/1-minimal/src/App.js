import React from 'react'
import { connect, publisher } from '@x/unify.react'
import { Form, Text, Submit } from '@danderson00/react-forms-material'

export default publisher(connect({
  messages: o => o.all()
})(
  ({ messages, publish }) => (
    <div>
      <ul>
        {messages.map((message, i) => (
          <li key={i}>{message.text}</li>
        ))}
      </ul>

      <Form onSubmit={publish} resetOnSubmit display="inline">
        <Text name="text" />
        <Submit>Send</Submit>
      </Form>
    </div>
  )
))
