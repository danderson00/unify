import React from 'react'
import { connect } from '@x/unify.react'
import Send from './Send'

export default connect('messages')(
  ({ messages, username, room }) => (
    <div>
      <h3>Hello, {username}, welcome to the {room} chat room!</h3>
      <ul>
        {messages.map((message, i) => (
          <li key={i}>
            <b>{message.username}</b> says <b>{message.text}</b>
          </li>
        ))}
      </ul>
      <Send username={username} />
    </div>
  )
)
