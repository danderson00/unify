import { Machine } from '@x/unify.react'

const machine = Machine({
  initial: 'Lobby',
  states: {
    'Lobby': {
      on: { 'join': 'Room' },
    },
    'Room': {
      meta: {
        scope: ['room'],
        props: ['username']
      },
      on: { 'leave': 'Lobby' }
    }
  }
})

export default machine
