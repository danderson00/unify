import React from 'react'
import { publisher } from '@x/unify.react'
import { Form, Text, Submit } from '@danderson00/react-forms-material'

export default publisher(({ publish }) =>
  <Form display="inline" onSubmit={({ username, room }) =>
    publish({ topic: 'join', room, username: username || 'Anonymous' })}
  >
    <Text name="username" label="User Name" />
    <Text name="room" label="Room" required />
    <Submit>Join</Submit>
  </Form>
)
