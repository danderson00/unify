import React from 'react'
import ReactDOM from 'react-dom'
import consumer from '@x/unify'
import { Provider, Navigator } from '@x/unify.react'
import navigation from './navigation'

consumer().connect()
  .then(host => ReactDOM.render(
    <Provider host={host}>
      <Navigator
        machine={navigation}
        resolveComponent={path => import(`./components/${path}`).then(m => m.default)}
      />
    </Provider>,
    document.getElementById('root')
  ))
