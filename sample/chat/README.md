# unify.sample.chat

The samples in the following folders incrementally demonstrate basic features of Unify:

1) minimal
2) scope
3) navigation
4) vocabulary
5) constraints
